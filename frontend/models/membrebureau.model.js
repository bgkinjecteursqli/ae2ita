export default {
  id: null,
  nom: null,
  prenoms: null,
  fonction: null,
  section: null,
  email: null,
  photo: null,
  date_debut: null,
  date_fin: null,
  date_creation: null,
  slug: null,
  date_modification: null,
}