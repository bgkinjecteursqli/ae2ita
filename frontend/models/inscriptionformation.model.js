export default {
  id: null,
  formation: null,
  nom: null,
  prenoms: null,
  photo: null,
  email: null,
  code: null,
  titre: null,
  date_inscription: null,
  present: null,
  formation_slug: null,
}