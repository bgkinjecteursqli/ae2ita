import { decryptTimestamp } from '~/utils/timeUtils'

export default  function ({ app, store, redirect, $cookies, from }, next){

   const isLog = store.getters['auth/isLoggedIn']
    if(isLog){
      next()
    }

    const nextRoute = from?.fullPath
    let loginPath =`/authentifications/connexion/`
    if (nextRoute){
           loginPath = `${loginPath}?next=${nextRoute}`
    }

    const storedEncryptedTimestamp =  $cookies.get('_app_session');
      if (storedEncryptedTimestamp) {
        let storedExpirationTimestamp = 0
        try{
            storedExpirationTimestamp =  decryptTimestamp(storedEncryptedTimestamp);
        }catch (e) {
          redirect(loginPath)
        }

        const now = new Date().getTime();
        if (now > storedExpirationTimestamp) {

          // La session a expiré,
          redirect(loginPath)

        } else {
          // La session est toujours active
           store.commit('auth/SET_LOGGEDIN', true)
          next()
        }
      }
      else{
        redirect(loginPath)
  }
}
