import SHA256 from 'crypto-js/sha256'
import { decryptUniqueHash } from '~/utils/timeUtils'

export default  function({$cookies, store, redirect, from,route, $axios}, next){

      if(process.client){
        const cookieAgent = $cookies.get('_ua')
        if (cookieAgent){
           const userAgent = SHA256(navigator.userAgent).toString();
           const agentSha = decryptUniqueHash(cookieAgent)
          if (userAgent === agentSha){
            next()
          }else{
            // console.log('session ijack')
            $cookies.remove('_app_session')
            $cookies.remove('_uuid_session')
            $cookies.remove('_ua')
            store.commit('auth/SET_PROFIl', null)
            store.commit('auth/SET_AUTENTICATED', false)
            store.commit('auth/SET_LOGGEDIN', false)
            $axios.setToken(false)
            // location.reload(true)
            window.location.reload(true)
          }
        }
      next()
    }
      next()
}
