// middleware/csrfToken.js

export default async function ({$axios,$cookies, app,  },req, next) {

    try {
      const response = await $axios.$get('/csrf-token');
      const csrfToken = response.csrfToken;
      req.session.csrfToken = csrfToken;
      $cookies.set('csrftoken', csrfToken, {
        path: '/',
        // domain: '.ae2ita.com',
        secure: true,
      });
    } catch (error) {
      console.error('Erreur lors de la récupération du CSRF Token:', error);
      next()
    }
    next()

}
