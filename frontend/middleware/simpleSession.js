import { decryptTimestamp } from '~/utils/timeUtils'

export default  function ({ app, store, redirect, $cookies, from }, next){

    console.log("simple session: ")
   const isLog = store.getters['auth/isLoggedIn']
    if(isLog){
      next()
    }
    const storedEncryptedTimestamp =  $cookies.get('_app_session');
      if (storedEncryptedTimestamp) {
        const storedExpirationTimestamp =  decryptTimestamp(storedEncryptedTimestamp);
        const now = new Date().getTime();
        if (now > storedExpirationTimestamp) {
          $cookies.remove('_app_session')
          $cookies.remove('_uuid_session')
          $cookies.remove('_ua')
          store.commit('auth/SET_LOGGEDIN', false)
          store.commit('auth/SET_AUTENTICATED', false)
          next()
        } else {
          // La session est toujours active
           // store.commit('auth/SET_LOGGEDIN', true)
          next()
        }
      }
      else{
        next()
  }
      next()
}
