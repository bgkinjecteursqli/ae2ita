// middleware/auth.js


import { decryptAuth } from '~/utils/timeUtils'

export default async function (context, next) {
    const {store, $axios, $services, $cookies } = context
       // console.log("simple auth: ")
      const isAuth = store.getters['auth/isAuthenticated']
      if(isAuth){
        if(!store.getters['auth/getUser']){
           store.dispatch('auth/fetchMyProfil')
        }
        next()
      }

      let token = null
      const authCypher = $cookies.get('_uuid_session')
      if(authCypher){
        try{
            const auth = decryptAuth(authCypher)
             token = auth.token
        }catch (e) {
          // en cas de donne incorrect
          next()
        }
      }
     if (isAuth){
       try {
        await $services.token.verifyToken($axios, { token });
        $axios.setToken(token, "Bearer")
        store.commit('auth/SET_AUTENTICATED', true)
        store.dispatch('auth/fetchMyProfil')
        next()
      } catch (error) {
         next()
      }
     }
      next()
}
