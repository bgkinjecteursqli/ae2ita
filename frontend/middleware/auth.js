// middleware/auth.js


import { decryptAuth } from '~/utils/timeUtils'
export default async function (context, next) {
    const {store, $axios, $services, redirect,from, $cookies } = context

      const isAuth = store.getters['auth/isAuthenticated']
      if(isAuth){
        next()
      }

      const nextRoute = from?.fullPath
      let loginPath =`/authentifications/connexion/`
      if (nextRoute){
         loginPath = `${loginPath}?next=${nextRoute}`
      }

      let token = null
      const authCypher = $cookies.get('_uuid_session')
     try{
         const auth = decryptAuth(authCypher)
         token = auth.token
       }catch (e) {
         token = null
       }


      if (!token) {
            redirect(loginPath);
        }

     try {
        await $services.token.verifyToken($axios, { token });
        $axios.setToken(token, "Bearer")
       store.commit('auth/SET_AUTENTICATED', true)
       store.dispatch('auth/fetchMyProfil')
       next()
      } catch (error) {
          redirect(loginPath);
      }
}
