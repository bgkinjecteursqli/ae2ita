// services/anniversaires.js
export default {
    list(axios, search=null) {
      if (search !== null){
        return axios.$get(`membres/anniversaires/?search=${search}`)
      }
      return axios.$get('membres/anniversaires/')
    }

  }
