export default {
  list(axios) {
    return axios.$get('/blogs/evenements/');
  },
  async read(axios, slug) {
    return await axios.$get(`/blogs/evenements/${slug}/`);
  },
};
