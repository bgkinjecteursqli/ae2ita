// services/documents.js
export default {
    list(axios, dossier, search=null, limit=100, offset=0) {
      const params = {
        dossier,
        search,
        limit,
        offset
      }
      return axios.$get('/documents/', { params })
    },
    create(axios, data) {
      return axios.$post('/documents/', data)
    },
    read(axios, slug) {
      return axios.$get(`/documents/${slug}/`)
    },
    update(axios, slug, data) {
      return axios.$put(`/documents/${slug}/`, data)
    },
    partialUpdate(axios, slug, data) {
      return axios.$patch(`/documents/${slug}/`, data)
    },
    delete(axios, slug) {
      return axios.$delete(`/documents/${slug}/`)
    }
  }
