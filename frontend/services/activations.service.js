
export default {
  read(axios, token, uid) {

    return axios.$get(`/activations/?token=${token}&uid=${uid}`)
  },
}
