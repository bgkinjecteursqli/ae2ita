
export default {
  list(axios,search, limit, offset) {
    const params = {
      search,
      limit,
      offset
    }
    return axios.$get('/albums/', { params })
  },
  create(axios, data) {
    return axios.$post('/albums/', data)
  },
  read(axios, slug) {
    return axios.$get(`/albums/${slug}/`)
  },
  update(axios, slug, data) {
    return axios.$put(`/albums/${slug}/`, data)
  },
  partialUpdate(axios, slug, data) {
    return axios.$patch(`/albums/${slug}/`, data)
  },
  delete(axios, slug) {
    return axios.$delete(`/albums/${slug}/`)
  },
  addImages(axios, slug, data) {
    return axios.$post(`/albums/${slug}/add_images/`, data)
  },
  addVideos(axios, slug, data) {
    return axios.$post(`/albums/${slug}/add_videos/`, data)
  },
  getImages(axios, slug) {
    return axios.$get(`/albums/${slug}/images/`)
  },
  getVideos(axios, slug) {
    return axios.$get(`/albums/${slug}/videos/`)
  }
}
