export default {
  async list(axios, search, limit, offset) {

    return await axios.$get('/blogs/experts/');
  },

  async create(axios, data) {
    return await axios.$post('/blogs/experts/', data);
  },

  async get(axios, uuid) {
    return await axios.$get(`/blogs/experts/${uuid}/`);
  },

  async update(axios, uuid, data) {
    return await axios.$put(`/blogs/experts/${uuid}/`, data);
  },

};
