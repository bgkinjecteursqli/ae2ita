// services/membres/documents.js
export default {
    list(axios, search, limit, offset) {
      const params = {
        search,
        limit,
        offset,
      };
      return this.$axios.$get('/membres/documents/', { params });
    },
    create(axios, data) {
      return axios.$post('/membres/documents/', data);
    },
    read(axios, slug) {
      return axios.$get(`/membres/documents/${slug}/`);
    },
    update(axios, slug, data) {
      return axios.$put(`/membres/documents/${slug}/`, data);
    },
    partialUpdate(axios, slug, data) {
      return axios.$patch(`/membres/documents/${slug}/`, data);
    },
    delete(axios, slug) {
      return axios.$delete(`/membres/documents/${slug}/`);
    },
  };
  