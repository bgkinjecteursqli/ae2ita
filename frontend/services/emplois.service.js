export default {
    list(axios) {
      return axios.$get('/emplois/');
    },
    async getBySlug(axios, slug) {
    return await axios.$get(`/emplois/${slug}/`);
  },
    create(axios, data) {
      return axios.$post('/emplois/', data);
    },
    read(axios, slug) {
      return axios.$get(`/emplois/${slug}/`);
    },
    update(axios, slug, data) {
      return axios.$put(`/emplois/${slug}/`, data);
    },
    partialUpdate(axios, slug, data) {
      return axios.$patch(`/emplois/${slug}/`, data);
    },
    delete(axios, slug) {
      return axios.$delete(`/emplois/${slug}/`);
    },
    postuler(axios, offreSlug, data) {
      return axios.$post(`/emplois/${offreSlug}/postuler/`, data);
    },
  };
