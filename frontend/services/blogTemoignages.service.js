export default {
  async list(axios, search, limit, offset) {
    const params = {
      search,
      limit,
      offset,
    };
    return await axios.$get('/blogs/temoignages/', { params });
  },

  async create(axios, data) {
    return await axios.$post('/blogs/temoignages/', data);
  },

  async get(axios, id) {
    return await axios.$get(`/blogs/temoignages/${id}/`);
  },

  async update(axios, id, data) {
    return await axios.$put(`/blogs/temoignages/${id}/`, data);
  },

  async partialUpdate(axios, id, data) {
    return await axios.$patch(`/blogs/temoignages/${id}/`, data);
  },

  async delete(axios, id) {
    return await axios.$delete(`/blogs/temoignages/${id}/`);
  },
};
