// services/membres/parcours.js
export default {
    list(axios, search, limit, offset) {
      const params = {
        search,
        limit,
        offset,
      };
      return  axios.$get('/membres/parcours/', { params });
    },
    create(axios, data) {
      return axios.$post('/membres/parcours/', data);
    },
    read(axios, slug) {
      return  axios.$get(`/membres/parcours/${slug}/`);
    },
    update(axios, slug, data) {
      return  axios.$put(`/membres/parcours/${slug}/`, data);
    },
    partialUpdate(axios, slug, data) {
      return  axios.$patch(`/membres/parcours/${slug}/`, data);
    },
    delete(axios, slug) {
      return  axios.$delete(`/membres/parcours/${slug}/`);
    },
  };
  