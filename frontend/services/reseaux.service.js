export default {
    listEntreprises(axios, search, limit, offset) {
        const params = {
            search,
            limit,
            offset,
        };
        return axios.$get('/reseaux/entreprises/', { params });
    },
    createEntreprise(axios, data) {
        return axios.$post('/reseaux/entreprises/', data);
    },
    readEntreprise(axios, slug) {
        return axios.$get(`/reseaux/entreprises/${slug}/`);
    },
    updateEntreprise(axios, slug, data) {
        return axios.$put(`/reseaux/entreprises/${slug}/`, data);
    },
    partialUpdateEntreprise(axios, slug, data) {
        return axios.$patch(`/reseaux/entreprises/${slug}/`, data);
    },
    deleteEntreprise(axios, slug) {
        return axios.$delete(`/reseaux/entreprises/${slug}/`);
    },

    listEtudiants(axios, search, limit, offset) {
        const params = {
            search,
            limit,
            offset,
        };
        return axios.$get('/reseaux/etudiants/', { params });
    },

    listProfessionnels(axios, search, limit, offset) {
        const params = {
            search,
            limit,
            offset,
        };
        return axios.$get('/reseaux/professionnels/', { params });
    },
    listEntrepreneurs(axios, search, limit, offset) {
        const params = {
            search,
            limit,
            offset,
        };
        return axios.$get('/reseaux/entrepreneurs/', { params });
    },
};
