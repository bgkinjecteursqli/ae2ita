export default {
    createToken(axios, data) {
      return axios.$post('/token/', data);
    },

    refreshToken(axios, data) {
      return axios.$post('/token/refresh/', data);
    },

    async verifyToken(axios, data) {
      return await axios.$post('/token/verify/', data);
    },
  };
