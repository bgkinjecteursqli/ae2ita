// services/membres/promotions.js
export default {
    list(axios, search, limit, offset) {
      const params = {
        search,
        limit,
        offset,
      };
      return axios.$get('/membres/promotions/', { params });
    },
    read(axios, slug) {
      return axios.$get(`/membres/promotions/${slug}/`);
    },
  };
  