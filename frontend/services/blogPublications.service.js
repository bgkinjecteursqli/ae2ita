export default {
  async list(axios, search, limit, offset) {
    const params = {
      search,
      limit,
      offset,
    };
    return await axios.$get('/blogs/publications/', { params });
  },

  async create(axios, data) {
    return await axios.$post('/blogs/publications/', data);
  },

  async get(axios, slug) {
    return await axios.$get(`/blogs/publications/${slug}/`);
  },

  async update(axios, slug, data) {
    return await axios.$put(`/blogs/publications/${slug}/`, data);
  },

  async partialUpdate(axios, slug, data) {
    return await axios.$patch(`/blogs/publications/${slug}/`, data);
  },

  async delete(axios, slug) {
    return await axios.$delete(`/blogs/publications/${slug}/`);
  },
};
