// services/membres/publications.js
export default {
    list(axios, search, limit, offset) {
      const params = {
        search,
        limit,
        offset,
      };
      return axios.$get('/membres/publications/', { params });
    },
    create(axios, data) {
      return axios.$post('/membres/publications/', data);
    },
    read(axios, slug) {
      return axios.$get(`/membres/publications/${slug}/`);
    },
    update(axios, slug, data) {
      return axios.$put(`/membres/publications/${slug}/`, data);
    },
    partialUpdate(axios, slug, data) {
      return axios.$patch(`/membres/publications/${slug}/`, data);
    },
    delete(axios, slug) {
      return axios.$delete(`/membres/publications/${slug}/`);
    },
  };
  