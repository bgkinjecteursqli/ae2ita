export default {
  list(axios, search, limit, offset) {
    const params = {
      search,
      limit,
      offset,
    };
    return axios.$get('/formations/', { params });
  },
  create(axios, data) {
    return axios.$post('/formations/', data);
  },
  read(axios, slug) {
    return axios.$get(`/formations/${slug}/`);
  },
  update(axios, slug, data) {
    return axios.$put(`/formations/${slug}/`, data);
  },
  partialUpdate(axios, slug, data) {
    return axios.$patch(`/formations/${slug}/`, data);
  },
  delete(axios, slug) {
    return axios.$delete(`/formations/${slug}/`);
  },
  inscription(axios, slug, data) {
    return axios.$post(`/formations/${slug}/inscription/`, data);
  },
  inscrits(axios, slug) {
    return axios.$get(`/formations/${slug}/inscrits/`);
  },
  liens(axios, slug) {
    return axios.$get(`/formations/${slug}/liens/`);
  },
  supports(axios, slug) {
    return axios.$get(`/formations/${slug}/supports/`);
  },
  getvideos(axios, slug) {
    return axios.$get(`/formations/${slug}/videos/`);
  },
  createFormateur(axios, data) {
    return axios.$post('/formations/formateurs/', data);
  },
  deleteFormateur(axios, id) {
    return axios.$delete(`/formations/formateurs/${id}/`);
  },
  createLien(axios, data) {
    return axios.$post('/formations/liens/', data);
  },
  deleteLien(axios, id) {
    return axios.$delete(`/formations/liens/${id}/`);
  },
  createSupport(axios, data) {
    return axios.$post('/formations/supports/', data);
  },
  deleteSupport(axios, id) {
    return axios.$delete(`/formations/supports/${id}/`);
  },
  createVideo(axios, data) {
    return axios.$post('/formations/videos/', data);
  },
  deleteVideo(axios, id) {
    return axios.$delete(`/formations/videos/${id}/`);
  },
};
