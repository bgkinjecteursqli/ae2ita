export default {
  async list(axios, search, limit, offset) {
    const params = {
      search,
      limit,
      offset,
    };
    return await axios.$get('/blogs/articles/', { params });
  },

  async create(axios, data) {
    return await axios.$post('/blogs/articles/', data);
  },

  async get(axios, slug) {
    return await axios.$get(`/blogs/articles/${slug}/`);
  },

  async update(axios, slug, data) {
    return await axios.$put(`/blogs/articles/${slug}/`, data);
  },

  async partialUpdate(axios, slug, data) {
    return await axios.$patch(`/blogs/articles/${slug}/`, data);
  },

  async delete(axios, slug) {
    return await axios.$delete(`/blogs/articles/${slug}/`);
  },
};
