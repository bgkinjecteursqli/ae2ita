// services/membres/formations.js
export default {
    list(axios, slug, search, limit, offset) {
      const params = {
        search,
        limit,
        offset,
      };
      return axios.$get(`membres/profils/${slug}/formations/`, { params });
    },

  };
