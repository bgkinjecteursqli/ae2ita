// services/membres/profils.js
export default {
    list(axios, search, limit, offset) {
      const params = {
        search,
        limit,
        offset,
      };
      return this.$axios.$get('/membres/profils/', { params });
    },
    create(axios, data) {
      return axios.$post('/membres/profils/', data);
    },
    read(axios, slug) {
      return axios.$get(`/membres/profils/${slug}/`);
    },
     readProfil(axios) {
      return  axios.$get(`/membres/profils/mon_profil/`);
    },
    update(axios, slug, data) {
      return axios.$put(`/membres/profils/${slug}/`, data);
    },
    partialUpdate(axios, slug, data) {
      return axios.$patch(`/membres/profils/${slug}/`, data);
    },
    delete(axios, slug) {
      return axios.$delete(`/membres/profils/${slug}/`);
    },
    getFormations(axios, slug) {
      return axios.$get(`/membres/profils/${slug}/formations/`);
    },
  };
