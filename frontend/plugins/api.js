export default function ({ $axios }, inject) {
    // Create a custom axios instance
    const api = $axios.create({
      headers: {
        common: {
          Accept: 'application/json, text/plain, */*'
        }
      }
    })
      // Interceptors (facultatif)
      api.interceptors.request.use((config) => {
        // Vous pouvez effectuer des modifications sur la configuration de la requête avant l'envoi
        return config
      }, (error) => {
        // Gérer les erreurs liées à la requête
        return Promise.reject(error)
      })

      api.interceptors.response.use((response) => {
        // Vous pouvez effectuer des modifications sur la réponse avant qu'elle ne soit renvoyée aux composants
        return response
      }, (error) => {
        // Gérer les erreurs de réponse
        return Promise.reject(error)
      })
    // Set baseURL to something different
    api.setBaseURL('http://127.0.0.1:8000')
  
    // Inject to context as $api
    inject('api', api)
  }
