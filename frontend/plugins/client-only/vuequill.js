import Vue from 'vue';
import '@vueup/vue-quill/dist/vue-quill.snow.css';
import { QuillEditor } from '@vueup/vue-quill'

Vue.use(QuillEditor)

export default function ({app}, inject){
  // console.log('QuillEditor', QuillEditor)
  app.components.QuillEditor = QuillEditor;

}
