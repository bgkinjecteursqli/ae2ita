// plugins/vee-validate.js

import Vue from 'vue';
import VeeValidate, { ValidationProvider, ValidationObserver, extend } from 'vee-validate';
import { required, email } from 'vee-validate/dist/rules';

// Règles de validation personnalisées
extend('required', required);
extend('email', email);

Vue.use(VeeValidate);
Vue.component('ValidationProvider', ValidationProvider);
Vue.component('ValidationObserver', ValidationObserver);
