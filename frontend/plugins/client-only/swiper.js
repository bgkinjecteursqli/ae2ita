  // import Swiper bundle with all modules installed
  // import Vue from 'vue'
   import Swiper from 'swiper/swiper-bundle.js'
  // import { Navigation, Pagination, Scrollbar, A11y } from 'swiper/modules';


  import 'swiper/swiper-bundle.css';

  const swiperOptions = {
      option1: {
        speed: 600,
        loop: true,
        effect: 'cards',
        spaceBetween: 10,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false
        },
        slidesPerView: 1,
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        }
      },
      option2: {
        speed: 600,
        loop: true,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false
        },
        slidesPerView:3,
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true
        },
        scrollbar: {
          el: '.swiper-scrollbar',
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },

      },
      option3: {
      effect: 'coverflow',
        speed:500,
        autoplay: {
          delay: 5000,
          disableOnInteraction: false
        },
      slidesPerView: 1,
      centeredSlides: true,
      coverflowEffect: {
        rotate: 50,
        stretch: 0,
        depth: 100,
        modifier: 1,
        slideShadows: true,
      },
      pagination: {
        el: '.swiper-pagination',
        type: 'bullets',
        clickable: true,
      },
      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev',
        },
      },
      option4: {
          effect: 'fade',
          autoplay: {
            delay: 3000,
            disableOnInteraction: false,
          },
          pagination: {
            el: '.swiper-pagination',
            type: 'bullets',
            clickable: true,
          },
          navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
          },
      },
      option5: {
        effect: 'cube',
        speed:700,
        cubeEffect: {
          slideShadows: true,
          shadow: true,
          shadowOffset: 20,
          shadowScale: 0.94,
        },
        autoplay: {
          delay: 3000,
          disableOnInteraction: false,
        },
        pagination: {
          el: '.swiper-pagination',
          type: 'bullets',
          clickable: true,
        },
        navigation: {
          nextEl: '.swiper-button-next',
          prevEl: '.swiper-button-prev',
        },
      },
      option6: {
  effect: 'flip',
  flipEffect: {
    slideShadows: true,
    limitRotation: true,
  },
  autoplay: {
    delay: 3000,
    disableOnInteraction: false,
  },
  pagination: {
    el: '.swiper-pagination',
    type: 'bullets',
    clickable: true,
  },
  navigation: {
    nextEl: '.swiper-button-next',
    prevEl: '.swiper-button-prev',
  },
},


    }


  export default ({ app }, inject) => {
    if (process.client) {
      inject('swiper', Swiper)
      inject('swiperOptions', swiperOptions)
    }

  }

  // import { Swiper , SwiperSlide} from 'swiper/vue'
  // Vue.use(Swiper)
  // Vue.use(SwiperSlide)
