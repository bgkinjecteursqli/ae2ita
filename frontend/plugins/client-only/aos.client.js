import AOS from 'aos';
// import Vue from 'vue'
import "aos/dist/aos.css"

export default ({ app }) => {
    app.AOS = AOS.init({
        once: false,
        duration: 1000,
        // easing: 'slide',
        mirror: true,
        easing: 'ease-out-back',
				
    });

};


function aosInit() {
    AOS.init({
      duration: 800,
      easing: 'slide',
      once: false,
      mirror: true
    });
  }
  window.addEventListener('load', () => {
    aosInit();
  });