import Counter from '@/assets/js/counter';

// const counter1 = new Counter(".counter1");
// const counter2 = new Counter(".counter2");



// let counters = [];

export default ({ app }, inject) => {
//   app.router.beforeEach((to, from, next) => {
//     // Arrêter et détruire toutes les instances de compteur
//     counters.forEach(counter => {
//         counter.die()
//         counter.destroy()
//     });
//     counters = [];

//     next();
//   });

  if (process.client) {
    const selectors = [
      '.counter-1',
    //   '.counter-2',
    //   '.counter-3',
    //   '.counter-4',
    ];

    selectors.forEach((selector, index) => {
      
        const counter = new Counter(selector, /* options */);
        // counters.push(counter);
        inject(`${selectors[index]}`, counter)
    });
  }
//   console.log(app)
};