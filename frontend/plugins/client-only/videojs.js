import videojs from 'video.js';
import 'video.js/dist/video-js.css'

export default ({ app }, inject) => {
    app.videojs=videojs;
    inject('videojs', videojs)
  }