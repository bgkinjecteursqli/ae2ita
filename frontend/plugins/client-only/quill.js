import Quill from 'quill';
import 'quill/dist/quill.bubble.css';
import 'quill/dist/quill.core.css';
import 'quill/dist/quill.snow.css';

export default function({app}, inject){

     if(process.client){
       inject("quill", Quill)
     }
}
