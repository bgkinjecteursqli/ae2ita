import { Fancybox } from "@fancyapps/ui";
import '@fancyapps/ui/dist/fancybox/fancybox.css';


export default ({ app }, inject) => {
    app.Fancybox=Fancybox;
    inject('Fancybox', Fancybox)
    // console.log(app)
  }