
import albumService from '@/services/albums.service';
import activitiesService from '@/services/activations.service';
import anniversairesService from '~/services/anniversaires.service';
import documentsService from '~/services/documents.service';
import emploisService from '~/services/emplois.service';
import membreDocumentsService from '~/services/membreDocuments.service';
import membreParcoursService from '~/services/membreParcours.service';
import membreProfilsService from '~/services/membreProfils.service';
import membrePromotionsService from '~/services/membrePromotions.service';
import membrePublicationsService from '~/services/membrePublications.service';
import membresFormationsService from '~/services/membresFormations.service.';
import reseauxService from '~/services/reseaux.service';
import formationsService from '~/services/formations.service';
import tokenService from '~/services/token.service';
import blogArticleService from '~/services/blogArticle.service';
import blogPublicationsService from '~/services/blogPublications.service';
import blogTemoignagesService from '~/services/blogTemoignages.service';
import evenementsService from '~/services/evenements.service'
import expertsService from '~/services/blogExperts.service'

const services = {
  activations: activitiesService,
  albums: albumService,
  anniversaires:anniversairesService,
  documents:documentsService,
  emplois:emploisService,
  formations: formationsService,
  membreDocuments:membreDocumentsService,
  membreParcours:membreParcoursService,
  membreProfils:membreProfilsService,
  membrePromotions:membrePromotionsService,
  membrePublications:membrePublicationsService,
  membresFormations:membresFormationsService,
  reseaux: reseauxService,
  token:tokenService,
  blogArticle: blogArticleService,
  blogPublications:blogPublicationsService,
  blogTemoignages:blogTemoignagesService,
  evenements:evenementsService,
  experts:expertsService
}

export default function ({ app }, inject) {
  inject('services', services)
}

