// plugins/axios.js

import { decryptAuth } from '~/utils/timeUtils'

export default  function ({ $axios, redirect, $toast, $cookies }) {
  $axios.onRequest(config => {
    const csrfToken = $cookies.get('csrftoken');
    const authCypher = $cookies.get('_uuid_session')
    let token = null

    try {
    const auth = decryptAuth(authCypher)
     token = auth.token
    }catch (e) {
    token = null
    }


    if(token){
      $axios.setToken(token, 'Bearer')
    }
    if (csrfToken) {
      config.headers['X-CSRFToken'] = csrfToken;
      $axios.setHeader('X-CSRFToken', csrfToken);
    }
    if(config.method === 'post'){
     // console.log('Making post data :', config.data)

       // $axios.setHeader('Content-Type', 'multipart/form-data')
     // config.headers['Content-Type']= 'multipart/form-data'
    }

    console.log('Making request to url :' + config.url)
  })

  $axios.onError( error => {
    console.error('error api:', error.response.data)
    // const login = '/authentifications/connexion/'
    let code = 0
    try{
      code = error.response.status
    }catch (error){
      code = 1
    }

    if(code === 1){
       $toast.error('serveur distant indisponible')
    }

    if (code === 404) {
      $toast.error('Objet demandé introuvable !')
    }
    if(code === 500){
      $toast.error('erreur 501: quelque a mal tourné en interne !')
    }
    if(code ===401){
       $toast.error(`donnée authentification incorrectes`)
    }
     return Promise.reject(error.response.data)
  })

  $axios.onResponse(response => {
    return Promise.resolve(response)
  })
  // $axios.setHeader('Content-Type', 'multipart/form-data', ['post', 'put'])
   $axios.setHeader('Content-Type', 'application/x-www-form-urlencoded', ['post','put','patch',])
}

