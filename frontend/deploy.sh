#!/bin/bash

# Build frontend code
echo "install packages"
yarn
echo "Building frontend code..."
yarn build
chmod +x start.sh

pm2 restart ita --update-env
