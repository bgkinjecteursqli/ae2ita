// store/docs.js
// import axios from 'axios';

export const state = () => ({
  docs: [
    {
        id: 1,
        name: 'cours : Résumé du cours de Probabilté 1',
        type: 'pdf',
        size: 2048,
        date: '2023-06-18',
        link: '/documents/document.pdf',
        folder: 'ita_1'
    },
    {
        id: 2,
        name: 'Presentation.pptx',
        type: 'pptx',
        size: 4096,
        date: '2023-06-19',
        link: '/documents/Presentation.pptx',
        folder: 'ita_2'
    },
    {
        id: 3,
        name: 'Spreadsheet.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/Spreadsheet.xlsx',
        folder: 'ita_3'
    },
    {
        id: 4,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'bureau'
    },
    {
        id: 5,
        name: 'cours : Résumé du cours de Probabilté 5',
        type: 'pdf',
        size: 2048,
        date: '2023-06-18',
        link: '/documents/document.pdf',
        folder: 'ita_2'
    },
    {
        id: 6,
        name: 'Presentation.pptx',
        type: 'pptx',
        size: 4096,
        date: '2023-06-19',
        link: '/documents/Presentation.pptx',
        folder: 'ita_2'
    },
    {
        id: 7,
        name: 'Spreadsheet.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/Spreadsheet.xlsx',
        folder: 'autres'
    },
    {
        id: 8,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'ita_2'
    },
    {
        id: 9,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'bureau'
    },
    {
        id: 1,
        name: 'cours : Résumé du cours de Probabilté 1',
        type: 'pdf',
        size: 2048,
        date: '2023-06-18',
        link: '/documents/document.pdf',
        folder: 'ita_1'
    },
    {
        id: 2,
        name: 'Presentation.pptx',
        type: 'pptx',
        size: 4096,
        date: '2023-06-19',
        link: '/documents/Presentation.pptx',
        folder: 'ita_2'
    },
    {
        id: 3,
        name: 'Spreadsheet.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/Spreadsheet.xlsx',
        folder: 'ita_3'
    },
    {
        id: 4,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'bureau'
    },
    {
        id: 5,
        name: 'cours : Résumé du cours de Probabilté 5',
        type: 'pdf',
        size: 2048,
        date: '2023-06-18',
        link: '/documents/document.pdf',
        folder: 'ita_2'
    },
    {
        id: 6,
        name: 'Presentation.pptx',
        type: 'pptx',
        size: 4096,
        date: '2023-06-19',
        link: '/documents/Presentation.pptx',
        folder: 'ita_2'
    },
    {
        id: 7,
        name: 'Spreadsheet.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/Spreadsheet.xlsx',
        folder: 'autres'
    },
    {
        id: 8,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'ita_2'
    },
    {
        id: 9,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'bureau'
    },
    {
        id: 5,
        name: 'cours : Résumé du cours de Probabilté 5',
        type: 'pdf',
        size: 2048,
        date: '2023-06-18',
        link: '/documents/document.pdf',
        folder: 'ita_2'
    },
    {
        id: 6,
        name: 'Presentation.pptx',
        type: 'pptx',
        size: 4096,
        date: '2023-06-19',
        link: '/documents/Presentation.pptx',
        folder: 'ita_2'
    },
    {
        id: 7,
        name: 'Spreadsheet.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/Spreadsheet.xlsx',
        folder: 'autres'
    },
    {
        id: 8,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'ita_2'
    },
    {
        id: 9,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'bureau'
    },
    {
        id: 1,
        name: 'cours : Résumé du cours de Probabilté 1',
        type: 'pdf',
        size: 2048,
        date: '2023-06-18',
        link: '/documents/document.pdf',
        folder: 'ita_1'
    },
    {
        id: 2,
        name: 'Presentation.pptx',
        type: 'pptx',
        size: 4096,
        date: '2023-06-19',
        link: '/documents/Presentation.pptx',
        folder: 'ita_2'
    },
    {
        id: 3,
        name: 'Spreadsheet.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/Spreadsheet.xlsx',
        folder: 'ita_3'
    },
    {
        id: 4,
        name: 'word.docx',
        type: 'docx',
        size: 3072000,
        date: '2023-06-20',
        link: '/documents/word.docx',
        folder: 'autres'
    },
    {
        id: 5,
        name: 'cours : Résumé du cours de Probabilté 5',
        type: 'pdf',
        size: 2048,
        date: '2023-06-18',
        link: '/documents/document.pdf',
        folder: 'ita_1'
    },

],
folderDoc:[],
  currentPage: 1,
  totalPages: 0,
});

export const mutations = {
  setDocs(state, { docs, totalPages }) {
    state.docs = state.docs.concat(docs);
    state.totalPages = totalPages;
  },
  setFolderDocs(state, { docs, totalPages }) {
    state.folderDoc = docs;
    state.totalPages = totalPages;
  },
  setCurrentPage(state, page) {
    state.currentPage = page;
  },
};

export const actions = {
   fetchDocs({ commit, state }, page = 1) {
    try {
    //   const response = await axios.get(`https://example.com/api/docs?page=${page}`); // Remplacez l'URL par l'API réelle avec la pagination
    //   const fetchedDocs = response.data.docs; // Assurez-vous que la structure des données correspond à celle attendue (un tableau d'objets)
    //   const totalPages = response.data.totalPages; // Assurez-vous que le nombre total de pages est renvoyé par l'API
    const fetchedDocs = [{
        id: 27,
        name: 'tableau statistiques.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/Spreadsheet.xlsx',
        folder: 'bureau',
    },{
        id: 37,
        name: 'cours proba.xlsx',
        type: 'xlsx',
        size: 3072,
        date: '2023-06-20',
        link: '/documents/docx.docx',
        folder: 'ita_3'
    },]    
    const totalPages = 1
      commit('setDocs', { docs: fetchedDocs, totalPages });
      commit('setCurrentPage', page);
    } catch (error) {
      console.error('Une erreur s\'est produite lors de la récupération des documents :', error);
    }
  },

  fetchDocsByFolder({ commit, state }, folderSlug) {
    // Filtrer les documents en fonction du slug du dossier
    const filteredDocs = state.docs.filter((doc) => doc.folder === folderSlug);
    commit('setFolderDocs', { docs: filteredDocs, totalPages:1 });
  },
};
