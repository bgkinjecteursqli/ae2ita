// store/reseauxStore.js

export const state = () => ({
  nosReseaux: [
      {
        title: 'Pôle des élèves ingénieurs',
        description: "Rassemble les élèves Ingénieurs des Techniques Agricoles de l'École Supérieur d'Agronomie (ESA) de l' INP-HB, offrant un espace d'échange et de collaboration.",
        lien: 'reseaux/etudiants',
        image: '/images/etudiants.png'
      },
      {
        title: 'Pôle des ingénieurs (Alumni)',
        description2: "Le pôle des ingénieurs des Techniques Agricoles est une communauté dynamique d'anciens élèves ingénieurs des techiques agricoles.",
        description: "Faites partie du réseau exclusif d'anciens élèves ingénieurs. Connectez-vous avec d'autres professionnels de l'agriculture.",
        lien: 'reseaux/alumnis',
        image: '/images/diplome.jpg'
      },
      {
        title: 'Réseaux des entrepreneurs ITA',
        description: "Ensemble, nous bâtissons l'avenir de l'agriculture et générons des opportunités d'emploi pour un impact durable.",
        lien: 'reseaux/entreprises',
        image: '/images/entrepreneur.jpg'
      }
    ],
  entreprises: [],
  etudiants: [],
  professionnels: [],
  entrepreneurs: [],
  loading: false,
  error: null,
});

export const mutations = {
  SET_ENTREPRISES(state, entreprises) {
    state.entreprises = [...state.entreprises, ...entreprises];
  },
  SET_ETUDIANTS(state, etudiants) {
    state.etudiants = [...state.etudiants, ...etudiants];
  },
  SET_PROFESSIONNELS(state, professionnels) {
    state.professionnels = [...state.professionnels, ...professionnels];
  },
  SET_ENTREPRENEURS(state, entrepreneurs) {
    state.entrepreneurs = [...state.entrepreneurs, ...entrepreneurs];
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchEntreprises({ commit }, { search, limit, offset }) {
    commit('SET_LOADING', true);
    try {
      const response = await this.$services.reseaux.listEntreprises(
        this.$axios,
        search,
        limit,
        offset
      );
      const entreprises = response.results;
      commit('SET_ENTREPRISES', entreprises);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async createEntreprise({ commit }, data) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.reseaux.createEntreprise(
        this.$axios,
        data
      );
      commit('SET_ENTREPRISES', result);
      commit('SET_LOADING', false);
      this.$toast.success('Entreprise créée avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async readEntreprise({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      const entreprise = await this.$services.reseaux.readEntreprise(
        this.$axios,
        slug
      );
      commit('SET_ACTIVE_ENTREPRISE', entreprise);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
   async updateEntreprise({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.reseaux.updateEntreprise(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_ENTREPRISE', result);
      commit('SET_LOADING', false);
      this.$toast.success('Entreprise mise à jour avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
   async partialUpdateEntreprise({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.reseaux.partialUpdateEntreprise(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_ENTREPRISE', result);
      commit('SET_LOADING', false);
      this.$toast.success('Mise à jour partielle de l\'entreprise réussie');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deleteEntreprise({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      await this.$services.reseaux.deleteEntreprise(this.$axios, slug);
      commit('SET_ACTIVE_ENTREPRISE', {});
      commit('SET_LOADING', false);
      this.$toast.success('Entreprise supprimée avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async fetchEtudiants({ commit }, { search, limit, offset }) {
    commit('SET_LOADING', true);
    try {
      const response = await this.$services.reseaux.listEtudiants(
        this.$axios,
        search,
        limit,
        offset
      );
      const etudiants = response.results;
      commit('SET_ETUDIANTS', etudiants);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async fetchProfessionnels({ commit }, { search, limit, offset }) {
    commit('SET_LOADING', true);
    try {
      const response = await this.$services.reseaux.listProfessionnels(
        this.$axios,
        search,
        limit,
        offset
      );
      const professionnels = response.results;
      commit('SET_PROFESSIONNELS', professionnels);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async fetchEntrepreneurs({ commit }, { search, limit, offset }) {
    commit('SET_LOADING', true);
    try {
      const response = await this.$services.reseaux.listEntrepreneurs(
        this.$axios,
        search,
        limit,
        offset
      );
      const entrepreneurs = response.results;
      commit('SET_ENTREPRENEURS', entrepreneurs);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
};

export const getters = {
  getEntreprises(state) {
    return state.entreprises;
  },
  getEtudiants(state) {
    return state.etudiants;
  },
  getProfessionnels(state) {
    return state.professionnels;
  },
  getEntrepreneurs(state) {
    return state.entrepreneurs;
  },
  isLoading(state) {
    return state.loading;
  },
   getNosReseaux: state => {
      return state.nosReseaux
   },
  getError(state) {
    return state.error;
  },
};
