
export const state = () => ({
    nosSpecialites: [
        {
            title: 'Agriculture Générale',
            sigle: 'A.G',
            description: 'Maîtrisez les bases de l\'agriculture et découvrez les différentes techniques pour cultiver et produire des récoltes de qualité.',
            image: '/images/vegetables.jpg'
        },
        {
            title: 'Agro-Industrie',
            sigle: 'A.I',
            description: 'Explorez le monde passionnant de l\'agro-industrie et apprenez à transformer les produits agricoles en produits finis à valeur ajoutée.',
            image: '/images/ae2ita_IAI.jpeg'
        },
         {
            title: 'Entrepreneuriat Agricole',
            sigle: 'E.G.A',
            description: 'Développez vos compétences en entrepreneuriat et apprenez à créer et gérer votre propre entreprise agricole, en exploitant les opportunités du marché.',
            image: '/images/entrepreneuriat-agricole.jpg'
        },
        {
            title: 'Génie Rural',
            sigle: 'G.R',
            description: 'Devenez un expert dans la planification et la gestion des infrastructures rurales, en mettant l\'accent sur l\'amélioration des conditions de vie des communautés agricoles.',
            image: '/images/genie-rural.jpg'
        },
        {
            title: 'Zootechnologie',
            sigle: 'Z.T',
            description: 'Plongez dans le domaine de la zootechnologie et apprenez à élever et à gérer différents types d\'animaux, en veillant à leur bien-être et à leur productivité.',
            image: '/images/zootechnologie.jpg'
        },
        {
            title: 'Foresterie (aux et forêt)',
            sigle: 'I.T.F',
            description: 'Explorez la gestion durable des ressources forestières et découvrez comment préserver les écosystèmes forestiers tout en tirant parti de leurs nombreux avantages.',
            image: '/images/foresterie.jpeg'
        }
    ]



});

export const mutations = {};

export const actions = { };

export const getters = {
    getNosSpecialites: state => {
      return state.nosSpecialites
    }
  }
