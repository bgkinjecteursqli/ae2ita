export const state = () => ({
  bureauPresidents:[
    {
    ordre: 1,
    fonction: "Président",
    nomEtPrenoms: "Kouame N'Da Valery",
    promotion: "19",
    statut: "Ingénieur",
    contact: "0707028134/ 0102022323",
      photo:"/images/3.jpg"
  },
  {
    ordre: 2,
    fonction: "1er Vice-Président",
    nomEtPrenoms: "Yapi Money Hervé",
    promotion: "20",
    statut: "Ingénieur",
    contact: "0707364091/ 0102196768",
    photo:"/images/3.jpg"
  },
  {
    ordre: 3,
    fonction: "2ème Vice-Président",
    nomEtPrenoms: "N'Goran Yao Fulgence",
    promotion: "46",
    statut: "Elève-Ingénieur",
    contact: "0748785308",
    photo:"/images/3.jpg"
  },
  ],
  bureauExecutif:  [
  {
    ordre: 1,
    fonction: "Secrétaire Général",
    nomEtPrenoms: "Patricia N'Guessan (Epse HIEN)",
    promotion: "37",
    statut: "Ingénieur",
    contact: "0708642091",
    photo:"/images/3.jpg"
  },
  {
    ordre: 2,
    fonction: "Secrétaire Général Adjoint",
    nomEtPrenoms: "Kouassi Koffi Kan Meric Sylveane",
    promotion: "46",
    statut: "Elève-Ingénieur",
    contact: "0787004987",
    photo:"/images/3.jpg"
  },
  {
    ordre: 3,
    fonction: "Trésorière Générale",
    nomEtPrenoms: "Amani Amoi Clarisse",
    promotion: "36",
    statut: "Ingénieur",
    contact: "0758458662",
    photo:"/images/3.jpg"
  },
  {
    ordre: 4,
    fonction: "Trésorière Générale Adjointe",
    nomEtPrenoms: "Bamba Madoussou Noura",
    promotion: "46",
    statut: "Elève-Ingénieur",
    contact: "0141682548",
    photo:"/images/3.jpg"
  }
],
  bureauOrganes:[
  {
    ordre: 1,
    titre:"FORMATION",
    commission: "FORMATION, RENFORCEMENT DE CAPACITE, STAGES ET INSERTION PROFESSIONNELLE",
    membres: [
      { nomEtPrenoms: "DJINA DJOLO JEAN MARC", promotion: "35", contact: "0709152949", photo: "/images/3.jpg" },
      { nomEtPrenoms: "JUNIOR ADJE FELIX", promotion: "22", contact: "0747812017", photo: "/images/3.jpg" },
      { nomEtPrenoms: "KOFFI KONAN ANGE", promotion: "18", contact: "0709419342/0103010270", photo: "/images/3.jpg" },
      { nomEtPrenoms: "ADOUKOUA KACOU ETIENNE", promotion: "46", contact: "0554237151", photo: "/images/3.jpg" }
    ]
  },
  {
    ordre: 2,
    titre:"PROJETS",
    commission: "PROJETS, PLANNIFICATION ET RECHERCHE DE FINANCEMENT",
    membres: [
      { nomEtPrenoms: "AMEDE SARE", promotion: "18", contact: "0101221320", photo: "/images/3.jpg" },
      { nomEtPrenoms: "MEITE INZA", promotion: "20", contact: "0545006060", photo: "/images/3.jpg" },
      { nomEtPrenoms: "JULIEN KOUAKOU", promotion: "40", contact: "0758186897", photo: "/images/3.jpg" }
    ]
  },
  {
    ordre: 3,
    titre: "SUIVI",
    commission: "SUIVI EVALUATION INTERNE",
    membres: [
      { nomEtPrenoms: "MME KOUMAN", promotion: "30", contact: "0707371170", photo: "/images/3.jpg" }
    ]
  },
  {
    ordre: 4,
    titre: "COMMUNICATION",
    commission: "COMMUNICATION ET PROMOTION DE L’ITA",
    membres: [
      { nomEtPrenoms: "MME TOURE KOULAKO", promotion: "31", contact: "0708437389", photo: "/images/3.jpg" },
      { nomEtPrenoms: "CAMARA SOUNAN THEOPHILE", promotion: "36", contact: "0708362195", photo: "/images/3.jpg" },
      { nomEtPrenoms: "KOUAKOU GHISLAIN BORIS", promotion: "47", contact: "0759188395", photo: "/images/3.jpg" }
    ]
  },
  {
    ordre: 5,
    titre:"MOBILISATION",
    commission: "MOBILISATION, COHESION ET CONSOLIDATION DES LIENS DE CORPORATION",
    membres: [
      { nomEtPrenoms: "KOUADIO PAUL GHISLAIN", promotion: "38", contact: "0708748456", photo: "/images/3.jpg" },
      { nomEtPrenoms: "MME MBANGA FATOU", promotion: "40", photo: "/images/3.jpg" }
    ]
  },
  {
    ordre: 6,
    titre:"ORGANISATION",
    commission: "COMMISSION ORGANISATION",
    membres: [
      { nomEtPrenoms: "SEI BI BOYE RUCHNO", promotion: "46", contact: "0767413235", photo: "/images/3.jpg" },
      { nomEtPrenoms: "KOUADIO N’DRI JEAN BORIS", promotion: "34", contact: "0707585061", photo: "/images/3.jpg" },
      { nomEtPrenoms: "OUATTARA ALHASSANE", promotion: "47", contact: "0769255896", photo: "/images/3.jpg" },
      { nomEtPrenoms: "ABO YABA MARIE-CHRISTIANE", promotion: "47", contact: "0748985382", photo: "/images/3.jpg" },
      { nomEtPrenoms: "FANNY KOLOTCHOLOMAN JEAN MARC", promotion: "46", contact: "0708153554", photo: "/images/3.jpg" }
    ]
  }
]
})

export const getters = {
  getBureauExecutif: (state) => state.bureauExecutif,
  getOrganes:(state)=>state.bureauOrganes,
  getPresidents:(state)=>state.bureauPresidents
}

