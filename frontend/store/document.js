// store/documents.js
export const state = () => ({
    document: [],
    loading: false,
    error: null,
  })

export const mutations = {
    SET_DOCUMENT(state, documents) {
      state.document = [...state.document, documents];
    },
    ADD_DOCUMENT(state, documents) {
        state.document.push(...documents);
      },
     SET_LOADING(state, loading) {
      state.loading = loading
    },
    SET_ERROR(state, error) {
      state.error = error
    },
  }
  export const actions = {
    async fetchDocuments({ commit }, { dossier, search, limit, offset }) {
      commit('SET_LOADING', true)
      commit("SET_ERROR", null);
      try {
        const documents = await this.$services.documents.list(this.$axios, dossier, search, limit, offset)
        commit('SET_DOCUMENT', documents.results)
        console.log(documents)
        commit('SET_LOADING', true)
      } catch (error) {
        console.error("doc",error)
      commit("SET_ERROR", error);

      }
    },
    async createDocument({ commit }, data) {
      try {
        const newDocument = await this.$services.documents.create(this.$axios, data)
        console.log(newDocument)
      } catch (error) {
        console.error(error)
      }
    },
    async readDocument({ commit }, slug) {
      try {
        const document = await this.$services.documents.read(this.$axios, slug)
        console.log(document)
      } catch (error) {
        console.error(error)
      }
    },
  }

  export const getters = {
    publicDocument(state) {
      return state.document;
    },
    isLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    },

  };
