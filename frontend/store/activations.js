export const state = () => ({
    activations: []
  })
  
  export const mutations = {
    SET_ACTIVATIONS(state, activations) {
      state.activations = activations
    }
  }
  
  export const actions = {
    async fetchActivations({ commit }, { token, uid }) {
      try {
        const activations = await this.$services.activations.read(token, uid)
        commit('SET_ACTIVATIONS', activations)
      } catch (error) {
        // Gérer l'erreur ici
        console.error(error)
      }
    }
  }
  