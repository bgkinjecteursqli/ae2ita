// store/auth.js

/**
 * Cette fonction additionne deux nombres.
 * @param {number} a - Le premier nombre.
 * @param {number} b - Le deuxième nombre.
 * @returns {number} - La somme des deux nombres.
 */

/**
 * Module pour la gestion des authentification.
 * @ module auth.js
 *
 * Auth Vuex module
 * Handles state management for authentication in the app.
 *
 * State:
 * - auth: Object containing auth tokens
 * - isAuthenticated: Boolean indicating if user is authenticated
 * - isLoggedIn: Boolean indicating if user is logged in
 * - isAdmin: Boolean indicating if user is admin
 * - userProfil: User profile object
 * - loading: Boolean indicating background action in progress
 * - error: Error object if any error occurs
 *
 * Mutations:
 * - SET_AUTH: Sets auth state with auth object
 * - SET_PROFIL: Sets userProfil state with user object
 * - SET_ADMIN: Sets isAdmin state with boolean
 * - SET_AUTHENTICATED: Sets isAuthenticated state with boolean
 * - SET_LOGGEDIN: Sets isLoggedIn state with boolean
 * - SET_LOADING: Sets loading state with boolean
 * - SET_ERROR: Sets error state with error object
 *
 * Actions:
 * - login: Logs user in, sets auth and user state on success
 * - fetchMyProfil: Fetches user profile data
 * - logout: Clears auth state and logs user out
 *
 * Getters:
 * - isAuthenticated: Gets authentication status
 * - isLoggedIn: Gets login status
 * - isAdmin: Gets admin status
 * - getUser: Gets user profile
 * - isLoading: Gets background task status
 * - getError: Gets error object
*/


import { encryptAuth, encryptTimestamp, generateExpirationTimestamp } from '~/utils/timeUtils'

export default {
    state: () => ({
        auth: {},
        isAuthenticated: false,
        isLoggedIn:false,
        isAdmin:false,
        userProfil:null,
        loading: false,
        error: null,
    }),

    mutations: {
        SET_AUTH(state, auth) {
            state.auth = auth;
        },
       SET_PROFIl(state, user) {
            state.userProfil = user;
        },
      SET_ADMIN(state, admin) {
            state.isAdmin = admin;
        },
       SET_AUTENTICATED(state, admin) {
            state.isAuthenticated = admin;
        },
      SET_LOGGEDIN(state, value) {
            state.isLoggedIn = value;
        },
        SET_LOADING(state, loading) {
            state.loading = loading;
        },
        SET_ERROR(state, error) {
            state.error = error;
        },
    },

    actions: {
        async login({ commit, dispatch }, { username, password }) {
            commit('SET_LOADING', true);
            commit('SET_ERROR', null);
            const form = new FormData()
             form.append('username', username)
              form.append('password', password)
            try {
                const response = await this.$services.token.createToken(this.$axios, form);
                commit('SET_LOGGEDIN', true)
                commit('SET_AUTH', response)
                 this.$axios.setToken(response.access, 'Bearer')
                await dispatch('fetchMyProfil')
               const auth={
                  refresh: response.refresh,
                  token: response.access
               }
                 const  enryptAuth = encryptAuth(auth)
                 const timeStamp = generateExpirationTimestamp()
                 const encrypt = encryptTimestamp(timeStamp.toString())

                this.$cookies.set('_app_session', encrypt, {path: '/'})
                this.$cookies.set('_uuid_session', enryptAuth, {path: '/'} )
            } catch (error) {
                commit('SET_ERROR', error);
            }
            commit('SET_LOADING', false);
        },
        async fetchMyProfil({state, commit}){
          if(state.userProfil !== null ) {
            return
          }
           if(state.isAuthenticated === false ) {
            return
          }
            try{
              const profil = await this.$services.membreProfils.readProfil(this.$axios)
              commit('SET_PROFIl', profil)
            }catch (error){
              commit('SET_ERROR', error);
            }
        },
         logout({ commit }) {
          console.log('loout')
          commit('SET_LOADING', true);
          this.$cookies.remove('_app_session')
          this.$cookies.remove('_uuid_session')
          this.$cookies.remove('_ua')
          this.$axios.setToken(false)
            commit('SET_AUTH', null);
            commit('SET_LOGGEDIN', false)
            commit('SET_AUTENTICATED', false)
            commit('SET_PROFIl', null)
                setTimeout(()=>{
              commit('SET_LOADING', false);
           }, 2000)

        },

    },

    getters: {
        isAuthenticated: (state) => state.isAuthenticated,
        isLoggedIn: (state) => state.isLoggedIn,
        isAdmin: (state) => state.isAdmin,
        getUser:(state) => state.userProfil,
        isLoading:(state)=> state.loading,
        getError:(state)=> state.error,
    },
};
