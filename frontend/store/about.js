
 export const state = ()=>({
      label :`<strong>N</strong>ous formons une famille, unie par une passion commune pour l'excellence académique,
              l'épanouissement personnel et la solidarité entre étudiants.`,
      title:'',
    short_descripton: `AE2ITA (Amicale des Élèves Ingénieurs et Ingénieurs des Techniques Agricoles) crée le 09 juin 2002,
     est une association à caractère apolitique regroupant en son sein les élèves Ingénieurs des Techniques Agricoles régulièrement inscrits à l'École Supérieurs d'Agronomie (ESA) de l'INP-HB de Yamoussoukro et des Ingénieurs des Techniques agricoles,
      anciens élèves de l'ex-IAB (Institut Agricole de Bouaké) ou de l'ESA.`,
   long_description: [
  `L'amicale <strong>AE2ITA</strong> est bien plus qu'une simple association étudiante.
             Nous sommes une communauté dynamique et engagée,
             rassemblant les étudiants de l'école d'ingénieurs AE2ITA autour d'une passion commune : l'excellence académique, l'épanouissement personnel et le renforcement des liens entre les étudiants.`,
  `Depuis notre création, nous nous sommes fixés pour mission d'offrir à nos membres une expérience étudiante inoubliable, en favorisant leur épanouissement sur les plans académique, professionnel
            et social. Nous croyons fermement que l'apprentissage ne se limite pas aux salles de classe, mais se nourrit également de rencontres,
            d'échanges et d'expériences enrichissantes.`,
  `Au sein de l'amicale AE2ITA, nous organisons une multitude d'activités stimulantes et variées. Des conférences inspirantes animées par des experts de l'industrie, des ateliers pratiques pour développer les compétences techniques,
             des sorties conviviales pour renforcer les liens entre les membres, et bien plus encore. Nous nous efforçons de créer un environnement où chacun peut s'épanouir, exprimer sa créativité et trouver sa place au sein de notre communauté.`,
  `Nous sommes également fiers de notre engagement envers la communauté qui nous entoure. Nous organisons régulièrement des projets de bénévolat, des actions sociales et des collaborations avec d'autres associations pour apporter notre contribution positive et faire une différence dans la société.`,
  `Rejoindre l'amicale AE2ITA, c'est bien plus que devenir membre d'une association. C'est intégrer une famille où l'entraide, le partage de connaissances et les amitiés durables sont au cœur de notre dynamique. Que vous soyez un nouvel étudiant à la recherche de nouvelles opportunités ou un membre
            chevronné souhaitant transmettre votre expérience, nous vous accueillons les bras ouverts.`,
  `Explorez notre site pour en savoir plus sur nos activités, nos réalisations et nos événements à venir.
             Rejoignez-nous et faites partie de cette aventure passionnante au sein de l'amicale AE2ITA.`,
  `Ensemble, nous construisons un avenir brillant et développons les leaders de demain.`
]
})

 export const getters = {
  getTitle(state){
  return state.title
  },
  getLabel(state){
    return state.label
  },
  getShort(state){
    return state.short_descripton
  },
  getLong(state){
    return state.long_description
  }
}


