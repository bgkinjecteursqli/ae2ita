// store/documents.js
export const state = () => ({
  entreprises: [],
  loading: false,
  error: null,
})

export const mutations = {
  SET_ENTREPRISES(state, documents) {
    state.entreprises = [];
    state.entreprises = [...documents];
  },

  SET_LOADING(state, loading) {
    state.loading = loading
  },
  SET_ERROR(state, error) {
    state.error = error
  },
}
export const actions = {
  async fetchEntreprise({ commit }) {
    commit('SET_LOADING', true)
    commit("SET_ERROR", null);
    try {
      const data = await this.$axios.$get('/reseaux/entreprises/')
      commit('SET_ENTREPRISES', data.results)
      commit('SET_LOADING', false)
    } catch (error) {
      commit('SET_LOADING', false)
      commit("SET_ERROR", error);
    }
  }
}

export const getters = {
  getEntreprises(state) {
    return state.entreprises;
  },
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },

};
