// store/temoignagesStore.js

export const state = () => ({
  temoignages: [],
  activeTemoignage: {},
  nextPage:null,
  loading: false,
  error: null,
});

export const mutations = {
  SET_TEMOIGNAGES(state, temoignages) {
    state.temoignages = [...temoignages];
  },
  SET_ACTIVE_TEMOIGNAGE(state, temoignage) {
    state.activeTemoignage = temoignage;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
 SET_NEXT_PAGE(state, nextPage) {
    state.nextPage = nextPage;
  },
};

export const actions = {
  async fetchTemoignages({state, commit }) {
    if(state.temoignages?.length>0){
      return
    }
    
    commit('SET_LOADING', true);
    try {
      const response = await this.$services.blogTemoignages.list(this.$axios);
      const temoignages = response.results;
      commit('SET_TEMOIGNAGES', temoignages);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async fetchTemoignageById({ commit }, id) {
    commit('SET_LOADING', true);
    try {
      const temoignage = await this.$services.blogTemoignages.get(this.$axios, id);
      commit('SET_ACTIVE_TEMOIGNAGE', temoignage);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async createTemoignage({ commit }, data) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogTemoignages.create(this.$axios, data);
      commit('SET_TEMOIGNAGES', result);
      commit('SET_LOADING', false);
      this.$toast.success('Témoignage créé avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async updateTemoignage({ commit }, { id, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogTemoignages.update(
        this.$axios,
        id,
        data
      );
      commit('SET_ACTIVE_TEMOIGNAGE', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async partialUpdateTemoignage({ commit }, { id, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogTemoignages.partialUpdate(
        this.$axios,
        id,
        data
      );
      commit('SET_ACTIVE_TEMOIGNAGE', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deleteTemoignage({ commit }, id) {
    commit('SET_LOADING', true);
    try {
      await this.$services.blogTemoignages.delete(this.$axios, id);
      commit('SET_ACTIVE_TEMOIGNAGE', {});
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async loadNextPage({state, commit }) {
    const nextPage = state.nextPage
    try {
      if (nextPage !== null) {
        const response = await this.$axios.$get(nextPage);
        const temoignages = response.results;
        commit('SET_TEMOIGNAGES', temoignages);
        commit('SET_NEXT_PAGE',response.next)
      }else{
        this.$toast.info('il y a plus de données !')
      }

    } catch (error) {
      // Traiter l'erreur si nécessaire
    }
  },
};

export const getters = {
  getTemoignages(state) {
    return state.temoignages;
  },
  getActiveTemoignage(state) {
    return state.activeTemoignage;
  },
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
  getTemoignageById: (state) => (id) => {
    return state.temoignages.find((temoignage) => temoignage.id === id);
  },
};
