// store/articlesStore.js

export const state = () => ({
  articles: [],
  activeArticle: {},
  nextPage:null,
  loading: false,
  adding:false,
  visibleArticles: [],
  remainingArticles: [],
  itemsPerPage: 3,
  error: null,
});

export const mutations = {
  SET_ARTICLES(state, articles) {
    state.articles = [...articles];
  },
   ADD_ARTICLES(state, articles) {
    state.articles = [...state.articles, ...articles];
  },
  SET_ACTIVE_ARTICLE(state, article) {
    state.activeArticle = article;
  },
   SET_NEXT_PAGE(state, nextPage) {
    state.nextPage = nextPage;
  },
 SET_VISIBLE_ARTICLES(state, visibleArticles) {
    state.visibleArticles = state.visibleArticles.concat(visibleArticles);
  },
  SET_REMAINING_ARTICLES(state, remainingArticles) {
    state.remainingArticles = [...remainingArticles];
  },

  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ADDING(state, adding) {
    state.adding = adding;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchArticles({state, commit, dispatch }) {
    if (state.articles.length>0) return

    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const response = await this.$services.blogArticle.list(
        this.$axios);
      const articles = response.results;
      commit('SET_ARTICLES', articles);
      dispatch('paginateArticles')
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async fetchSuggestions({state, dispatch}){
    if (state.articles.length===0){
      await dispatch('fetchArticles')
    }
  },
  async fetchArticleBySlug({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      const article = await this.$services.blogArticle.get(this.$axios, slug);
      commit('SET_ACTIVE_ARTICLE', article);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async createArticle({ commit }, data) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogArticle.create(this.$axios, data);
      commit('ADD_ARTICLES', [result])
      commit('SET_LOADING', false);
      this.$toast.success('Article créé avec succès')
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async readArticle({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      const article = await this.$services.blogArticle.get(this.$axios, slug);
      commit('SET_ACTIVE_ARTICLE', article);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async updateArticle({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogArticle.update(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_ARTICLE', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async partialUpdateArticle({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogArticle.partialUpdate(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_ARTICLE', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async deleteArticle({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      await this.$services.blogArticle.delete(this.$axios, slug);
      commit('SET_ACTIVE_ARTICLE', {});
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async fetchNextPage({state, commit }) {
    const nextPage = state.nextPage
    try {
      if (nextPage !== null) {
        const response = await this.$axios.$get(nextPage);
        const articles = response.results;
        commit('SET_REMAINING_ARTICLES', articles);
        commit('ADD_ARTICLES', articles);
        commit('SET_NEXT_PAGE',response.next)
      }else{
        this.$toast.info('il y a plus de données !')
      }

    } catch (error) {
      // Traiter l'erreur si nécessaire

    }
  },
  paginateArticles({ state, commit }) {
    commit('SET_VISIBLE_ARTICLES', state.articles.slice(0, state.itemsPerPage));
    commit('SET_REMAINING_ARTICLES', state.articles.slice(state.itemsPerPage));
  },
  loadMoreArticles({state,commit}) {
    console.log('request load datat more')
    state.adding = true
      setTimeout(() => {
        const nextDocs = state.remainingArticles.splice(0, state.itemsPerPage);
        commit('SET_VISIBLE_ARTICLES', nextDocs)
        state.adding=false;
      }, 1000);
    },
};

export const getters = {
  getArticles(state) {
    return state.articles;
  },
  getVisibleArticles(state) {
    return state.visibleArticles;
  },
   getRemainingArticles(state) {
    return state.remainingArticles;
  },
  getActiveArticle(state) {
    return state.activeArticle;
  },
  isLoading(state) {
    return state.loading;
  }, isAdding(state) {
    return state.adding;
  },
  getError(state) {
    return state.error;
  },
   getArticleBySlug: (state) => (slug) => {
    return state.articles.find((article) => article.slug === slug);
  },
   getSuggestions(state){
    return state.articles.filter((article) => article.slug !== state.activeArticle.slug);
  },
};
