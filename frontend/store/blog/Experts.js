// store/expert.js
export const state = () => ({
  experts: [],
  expert: {},
  loading: false,
  error: null,
});

export const mutations = {
  SET_EXPERTS(state, experts) {
    state.experts = experts;
  },
  SET_EXPERT(state, expert) {
    state.expert = expert;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchExperts({ commit }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const experts = await this.$services.experts.list(this.$axios);
      commit('SET_EXPERTS', experts.results);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_ERROR', error);
      commit('SET_LOADING', false);
    }
  },
  async createExpert({state, commit }, data) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const expert = await this.$services.experts.create(this.$axios, data);
      commit('SET_EXPERT', expert);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_ERROR', error);
      commit('SET_LOADING', false);
    }
  },
  async fetchExpertByUuid({ commit }, uuid) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    if(state.experts) {
      const expert = state.experts.find(expert => expert.uuid === uuid);
      commit('SET_EXPERT', expert);
      commit('SET_LOADING', false);
      return;
    }
    try {
      const expert = await this.$services.experts.get(this.$axios, uuid);
      commit('SET_EXPERT', expert);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_ERROR', error);
      commit('SET_LOADING', false);
    }
  },
  async updateExpert({ commit }, { uuid, data }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const expert = await this.$services.experts.update(this.$axios, uuid, data);
      commit('SET_EXPERT', expert);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_ERROR', error);
      commit('SET_LOADING', false);
    }
  },
};

export const getters = {
  getExperts(state) {
    return state.experts;
  },
  getSuggestions(state) {
    return state.experts.filter(expert => state.expert.uuid !== expert.uuid).slice(0, 2);
  },

  getExpert(state) {
    return state.expert;
  },
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
};
