// store/publicationStore.js

export const state = () => ({
  publications: [],
  suggestions: [],
  activePublication: {},
  nextPage:null,
  loading: false,
  error: null,
});

export const mutations = {
  SET_PUBLICATIONS(state, publications) {
    state.publications = [...publications];
  },
    SET_SUGGESTIONS(state, suggestions) {
    state.suggestions = [...suggestions];
  },
  SET_ACTIVE_PUBLICATION(state, publication) {
    state.activePublication = publication;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
  SET_NEXT_PAGE(state, nextPage) {
    state.nextPage = nextPage;
  },
};

export const actions = {
  async fetchPublications({ commit }, { limit=100, offset=0 }) {
    // const params ='titre=&commisson=&ordre='
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const response = await this.$services.blogPublications.list(
        this.$axios,
        {
          limit,
          offset
        }
      );
      const publications = response.results;
      commit('SET_PUBLICATIONS', publications);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async fetchSuggestions({state, dispatch}){
    if (state.publications.length===0){
      await dispatch('fetchPublications', 100,0)
    }
  },

  async fetchPublicationBySlug({ commit }, slug) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const publication = await this.$services.blogPublications.get(this.$axios, slug);
      commit('SET_ACTIVE_PUBLICATION', publication);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_ERROR', error.message);
      commit('SET_LOADING', false);
    }
  },

  async createPublication({ commit }, data) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogPublications.create(this.$axios, data);
      commit('SET_PUBLICATIONS', result)
      commit('SET_LOADING', false);
      this.$toast.success('Publication créée avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async updatePublication({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogPublications.update(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_PUBLICATION', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async partialUpdatePublication({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.blogPublications.partialUpdate(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_PUBLICATION', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deletePublication({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      await this.$services.blogPublications.delete(this.$axios, slug);
      commit('SET_ACTIVE_PUBLICATION', {});
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async loadNextPage({state, commit }) {
    const nextPage = state.nextPage
    try {
      if (nextPage !== null){
        const response = await this.$axios.$get(nextPage);
        const publications = response.results;
        commit('SET_PUBLICATIONS', publications);
        commit('SET_NEXT_PAGE', response.next)
      }else{
        this.$toast.info("il y a plus de donnés !")
      }

    } catch (error) {
      // Traiter l'erreur si nécessaire
    }
  },
};

export const getters = {
  getPublications(state) {
    return state.publications;
  },
  getActivePublication(state) {
    return state.activePublication;
  },
  getError(state) {
    return state.error;
  },
  getPublicationBySlug: (state) => (slug) => {
    return state.publications.find((publication) => publication.slug === slug);
  },
  getNextPage(state){
    return state.nextPage
  },
   isLoading(state) {
    return state.loading;
  },
   getSuggestions(state){
    return state.publications.filter((publication) => publication.slug !== state.activePublication.slug);
  },
};
