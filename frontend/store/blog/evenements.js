export const state = () => ({
  evenements: [],
  evenement: {},
  loading: false,
  error: null,
});

export const mutations = {
  SET_EVENEMENTS(state, evenements) {
    state.evenements = evenements;
  },
  SET_EVENEMENT(state, evenement) {
    state.evenement = evenement;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchEvenements({ commit, state }) {
    if(state.evenements.length>0) return
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const evenements = await this.$services.evenements.list(this.$axios);
      commit('SET_EVENEMENTS', evenements.results);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_ERROR', error);
      commit('SET_LOADING', false);
    }
  },
  async fetchEvenementBySlug({ commit }, slug) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const evenement = await this.$services.evenements.read(this.$axios, slug);
      commit('SET_EVENEMENT', evenement);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_ERROR', error);
      commit('SET_LOADING', false);
    }
  },
};

export const getters = {
  getEvenements(state) {
    return state.evenements;
  },
  getEvenement(state) {
    return state.evenement;
  },
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
};
