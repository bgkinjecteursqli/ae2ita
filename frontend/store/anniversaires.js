// store/anniversaires.js
import moment from 'moment';

export const state = () => ({
    anniversaires: [],
    anniversairesDuJour: [],
    anniversairesCetteSemaine: [],
    loading: false,
    error: null,
})

export const mutations = {
    SET_ANNIVERSAIRES(state, anniversaires) {
        state.anniversaires = anniversaires
    },
    SET_ANNIVERSAIRES_DU_JOUR(state, anniversaires) {
        state.anniversairesDuJour = [...anniversaires];
    },
    SET_ANNIVERSAIRES_CETTE_SEMAINE(state, anniversaires) {
        state.anniversairesCetteSemaine = [...anniversaires];
    },
    SET_LOADING(state, loading) {
      state.loading = loading;
        },
    SET_ERROR(state, error) {
      state.error = error;
      },
}

export const actions = {
    async fetchAnniversaires({ dispatch, commit, state }, search ) {
      if (state.anniversaires.length >0) {return }
      commit('SET_LOADING', true)
        try {
            const anniversaires = await this.$services.anniversaires.list(this.$axios, search)
             commit('SET_ANNIVERSAIRES', anniversaires.results)
             await dispatch('anniversairesDuJour')
             await dispatch('anniversairesCetteSemaine')
       commit('SET_LOADING', false)
        } catch (error) {
           commit('SET_ERROR', error)
           commit('SET_LOADING', false)
        }
    },
    anniversairesDuJour({ commit , state}) {
        const anniversairesDuJour = state.anniversaires.filter((anniv) => {
            moment.locale('fr')
            const annivDate = moment(anniv.date_naissance, 'YYYY-MM-DD');
            const today = moment();
            // Vérifier si le jour et le mois de la date de naissance correspondent au jour et au mois actuels
            return (
                annivDate.format('DD-MM') === today.format('DD-MM')
            );
        });
        commit('SET_ANNIVERSAIRES_DU_JOUR', anniversairesDuJour);
    },

    anniversairesCetteSemaine({ commit, state }) {
        try {
            moment.locale('fr')
            const dateFormat ='DD-MM-YYYY'
            const debutSemaine = moment().startOf('isoWeek').format(dateFormat);
            const finSemaine = moment().endOf('isoWeek').format(dateFormat);
            const anneeActive = moment().year();

            const anniversairesCetteSemaine = state.anniversaires.filter((anniv) => {
                const annivDate = moment(anniv.date_naissance, 'YYYY-MM-DD')
                const newStr = `${annivDate.format('DD-MM')}-${anneeActive}`
                const newDate = moment(newStr, dateFormat)
                // Vérifier si le jour et le mois de la date de naissance sont entre le début et la fin de la semaine en cours
                const estDansLaSemaine = newDate.isBetween(moment(debutSemaine, dateFormat), moment(finSemaine, dateFormat), null, '[]');
                return estDansLaSemaine;
            });

            commit('SET_ANNIVERSAIRES_CETTE_SEMAINE', anniversairesCetteSemaine);
        } catch (error) {
           commit('SET_ERROR', error)
        }
    }
}

  export const getters = {
    getAnniversaireMois(state){
      return state.anniversaires
    },
    getAnniversairesSemaine(state){
      return state.anniversairesCetteSemaine
    },
    getAnniversairesJour(state){
      return state.anniversairesDuJour
    },
    isLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    },
  };
