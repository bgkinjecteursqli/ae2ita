export const state = () => ({
    emplois: [],
    activeEmploi:{},
    loading: false,
    error: null,
  })

  export const mutations = {
    SET_EMPLOIS(state, emplois) {
      state.emplois = [...emplois]
    },
    SET_ACTIVE_EMPLOI(state, emplois) {
        state.activeEmploi = emplois
      },
    SET_LOADING(state, loading) {
      state.loading = loading
    },
    SET_ERROR(state, error) {
      state.error = error
    },
  }

  export const actions = {
    async fetchEmplois({ commit, state }) {
      if(state.emplois.length>0) return
      commit('SET_LOADING', true)
      commit('SET_ERROR', null);
      try {
        const emplois = await this.$services.emplois.list(this.$axios)
        commit('SET_EMPLOIS', emplois.results)
        commit('SET_LOADING', false)
      } catch (error) {
        commit('SET_LOADING', false)
        commit('SET_ERROR', error)
      }
    },
    async fetchEmploisBySlug({ commit }, slug) {
      commit('SET_LOADING', true);
      commit('SET_ERROR', null);
      try {
        const offre = await this.$services.emplois.getBySlug(this.$axios, slug);
        commit('SET_ACTIVE_EMPLOI', offre);
        commit('SET_LOADING', false);
      } catch (error) {
        commit('SET_LOADING', false);
        commit('SET_ERROR', error);
      }
  },
    async postuler({ commit }, { slug, data }) {
      commit('SET_LOADING', true)
      try {
        const result = await this.$services.emplois.postuler(this.$axios, slug, data)
        console.log('postuler resultat', result)
        commit('SET_LOADING', false)
      } catch (error) {
        commit('SET_LOADING', false)
        commit('SET_ERROR', error)
      }
    },

    async readEmploi({ commit }, slug) {
      commit('SET_LOADING', true)
      try {
        const emploi = await this.$services.emplois.read(this.$axios, slug)
        console.log(emploi)
        commit('SET_ACTIVE_EMPLOI', emploi)
        commit('SET_LOADING', false)
      } catch (error) {
        commit('SET_LOADING', false)
        commit('SET_ERROR', error)
      }
    },

    async updateEmploi({ commit }, { slug, data }) {
      commit('SET_LOADING', true)
      commit('SET_ERROR', null);
      try {
        const result = await this.$services.emplois.update(this.$axios, slug, data)
        commit('SET_ACTIVE_EMPLOI', result.results)
        commit('SET_LOADING', false)
      } catch (error) {
        commit('SET_LOADING', false)
        commit('SET_ERROR', error)
      }
    },

    async partialUpdateEmploi({ commit, dispatch }, { slug, data }) {
      commit('SET_LOADING', true)
      commit('SET_ERROR', null);
      try {
        const result = await this.$services.emplois.partialUpdate(this.$axios, slug, data)

        commit('SET_ACTIVE_EMPLOI', result.results)
        commit('SET_LOADING', false)
      } catch (error) {
        commit('SET_LOADING', false)
        commit('SET_ERROR', error)
      }
    },

    async deleteEmploi({ commit, dispatch }, slug) {
      commit('SET_LOADING', true)
      commit('SET_ERROR', null);
      try {
        await this.$services.emplois.delete(this.$axios, slug)
        dispatch('fetchEmplois',{})
        commit('SET_ACTIVE_EMPLOI', {})
        commit('SET_LOADING', false)
      } catch (error) {
        commit('SET_LOADING', false)
        commit('SET_ERROR', error)
      }
    },

  }

  export const getters = {
    getEmplois(state) {
      return state.emplois;
    },
    getStages(state){
      return state.emplois.filter((offre) => offre.type_emplois === 'stage')
    },
     getEmbauches(state){
      return state.emplois.filter((offre) => offre.type_emplois === 'embauche')
    },
    getActiveEmploi(state) {
        return state.activeEmploi;
      },
    isLoading(state) {
      return state.loading;
    },
    getError(state) {
      return state.error;
    },
    getSuggestions(state){
    return state.emplois.filter((offre) => offre.slug !== state.activeEmploi.slug);
    },
  };
