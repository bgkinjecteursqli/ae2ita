export const state = () => ({
  formations: [],
  activeFormation: {},
  loading: false,
  error: null,
});

export const mutations = {
  SET_FORMATIONS(state, formations) {
    state.formations = formations;
  },
  SET_ACTIVE_FORMATION(state, formation) {
    state.activeFormation = formation;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchFormations({ commit, state }) {
    if (state.formations.length > 0) return;

    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const formations = await this.$services.formations.list(
        this.$axios,
        null,
        null,
        null
      );
      commit('SET_FORMATIONS', formations.results);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async createFormation({ commit }, data) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const newFormation = await this.$services.formations.create(
        this.$axios,
        data
      );

      console.log('Nouvelle formation créée', newFormation);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

   async fetchFormationBySlug({ commit }, slug) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const formation = await this.$services.formations.read(this.$axios, slug);

      commit('SET_ACTIVE_FORMATION', formation);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      console.log("error")
      commit('SET_ERROR', true);
    }
  },
    async updateFormation({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const updatedFormation = await this.$services.formations.update(this.$axios, slug, data);

      commit('SET_ACTIVE_FORMATION', updatedFormation.results);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async partialUpdateFormation({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const updatedFormation = await this.$services.formations.partialUpdate(this.$axios, slug, data);

      commit('SET_ACTIVE_FORMATION', updatedFormation.results);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deleteFormation({ commit, dispatch }, slug) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      await this.$services.formations.delete(this.$axios, slug);
      dispatch('fetchFormations', {}); // Assuming you have a fetchFormations action
      commit('SET_ACTIVE_FORMATION', {});
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  //  s'inscrire à une formation
  async inscriptionFormation({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.formations.inscription(
        this.$axios,
        slug,
        data
      );
      console.log('Inscription réussie', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

   async fetchLiens({ commit, state }, slug) {
    if (state.activeFormation && state.activeFormation.slug === slug && state.activeFormation.liens) {
      return; // Liens already fetched for the activeFormation
    }

    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const liens = await this.$services.formations.liens(this.$axios, slug);

      commit('SET_ACTIVE_FORMATION_LIENS', liens);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async createLien({ commit }, { data }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const createdLien = await this.$services.formations.createLien(this.$axios,  data);

      commit('ADD_ACTIVE_FORMATION_LIEN', createdLien);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deleteLien({ commit }, { lienId }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      await this.$services.formations.deleteLien(this.$axios, lienId);

      commit('REMOVE_ACTIVE_FORMATION_LIEN', lienId); // Assuming you have a mutation to remove a lien from the activeFormation
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

   async fetchVideos({ commit, state }, formationSlug) {
    if (state.activeFormation && state.activeFormation.slug === formationSlug && state.activeFormation.videos) {
      return;
    }
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const videos = await this.$services.formations.getvideos(this.$axios, formationSlug);

      commit('SET_ACTIVE_FORMATION_VIDEOS', videos);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deleteVideo({ commit }, { id }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      await this.$services.formations.deleteVideo(this.$axios, id);

      commit('REMOVE_ACTIVE_FORMATION_VIDEO', id); // Assuming you have a mutation to remove a video from the activeFormation
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async createVideo({ commit }, { data }) {
    try {
      await this.$services.formations.createVideo(this.$axios, data);
    } catch (error) {
      console.error(error);
    }
  },
  fetchFormateurs() {
    try {
       // await this.$services.formations.formateurs(this.$axios);
      console.error('not implemented')
    } catch (error) {
      console.error(error);
    }
  },

  async createFormateur({ commit }, data) {
    try {
       await this.$services.formations.createFormateur(this.$axios, data);

    } catch (error) {
      console.error(error);
    }
  },

  async deleteFormateur({ commit }, id) {
    try {
      await this.$services.formations.deleteFormateur(this.$axios, id);
    } catch (error) {
      console.error(error);
    }
  },
};

export const getters = {
  getFormations(state) {
    return state.formations;
  },
  getActiveFormation(state) {
    return state.activeFormation;
  },
  getSuggestions(state){
    return state.formations.filter((formation) => formation.slug !== state.activeFormation.slug);
    },
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
};
