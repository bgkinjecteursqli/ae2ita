// store/albumStore.js

export const state = () => ({
  albums: [],
  activeAlbum: {},
  albumImages:[],
  albumVideos:[],
  loading: false,
  error: null,
});

export const mutations = {
  SET_ALBUMS(state, albums) {
    state.albums = [...state.albums, ...albums];
  },
  SET_ACTIVE_ALBUM(state, album) {
    state.activeAlbum = album;
  },
   CLEAR_ACTIVE_ALBUM(state) {
    state.activeAlbum = { };
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ALBUM_IMAGES(state, images) {
    state.albumImages = []
    state.albumImages = [...images];
  },
  SET_ALBUM_VIDEOS(state, videos) {
    state.albumVideos = []
    state.albumVideos = [...videos];
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchAlbums({ commit , state}, search=null, limit=100, offset=0 ) {
    if(state.albums.length>0) {
      return
    }
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const response = await this.$services.albums.list(
        this.$axios,
        search,
        limit,
        offset
      );
      const albums = response.results;
      commit('SET_ALBUMS', albums);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async createAlbum({ commit }, data) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.albums.create(
        this.$axios,
        data
      );
      commit('SET_ACTIVE_ALBUM', result);
      commit('SET_LOADING', false);
      this.$toast.success('Album créé avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async readAlbum({ commit , state}, slug) {
    if(state.activeAlbum.slug === slug){
      return
    }
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    commit('SET_ALBUM_VIDEOS', []);
    commit('SET_ALBUM_VIDEOS', []);
    try {
      const album = await this.$services.albums.read(this.$axios, slug);
      commit('SET_ACTIVE_ALBUM', album);
      commit('SET_ALBUM_IMAGES', album.album_images);
      commit('SET_ALBUM_VIDEOS', album.album_videos);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async updateAlbum({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.albums.update(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_ALBUM', result);
      commit('SET_LOADING', false);
      this.$toast.success('Album mis à jour avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async partialUpdateAlbum({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.albums.partialUpdate(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_ALBUM', result);
      commit('SET_LOADING', false);
      this.$toast.success('Mise à jour partielle de l\'album réussie');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deleteAlbum({ commit }, { slug }) {
    commit('SET_LOADING', true);
    try {
      await this.$services.albums.delete(this.$axios, slug);
      commit('SET_ACTIVE_ALBUM', {});
      commit('SET_LOADING', false);
      this.$toast.success('Album supprimé avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async addImagesToAlbum({ commit, dispatch }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      await this.$services.albums.addImages(
        this.$axios,
        slug,
        data
      );

      dispatch('fetchImagesForAlbum', slug);
      commit('SET_LOADING', false);
      this.$toast.success('Images ajoutées à l\'album avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async addVideosToAlbum({ commit, dispatch }, { slug, data }) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
       await this.$services.albums.addVideos(
        this.$axios,
        slug,
        data
      );

      dispatch('fetchVideosForAlbum', slug);
      commit('SET_LOADING', false);
      this.$toast.success('Vidéos ajoutées à l\'album avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async fetchImagesForAlbum({ commit }, { slug }) {
    commit('SET_LOADING', true);
    commit('SET_ALBUM_IMAGES', []);
    try {
       const response = await this.$services.albums.getImages(
        this.$axios,
        slug
      );
      commit('SET_ALBUM_IMAGES', response)
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async fetchVideosForAlbum({ commit }, slug  ) {
    commit('SET_LOADING', true);
    commit('SET_ALBUM_VIDEOS', []);
    try {
      const response = await this.$services.albums.getVideos(
        this.$axios,
        slug
      );
      commit('SET_ALBUM_VIDEOS', response)
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  clearActiveAlbum({commit}){
    commit('CLEAR_ACTIVE_ALBUM',{});
  }
};

export const getters = {
  getAlbums(state) {
    return state.albums;
  },
  getActiveAlbum(state) {
    return state.activeAlbum;
  },
   getAlbumVideos(state) {
    return state.albumVideos;
  },
  getAlbumImages(state) {
    return state.albumImages;
  },
  getAlbumBySlug:(state)=>(slug)=>state.albums.filter((album)=>album.slug ===slug),
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
};
