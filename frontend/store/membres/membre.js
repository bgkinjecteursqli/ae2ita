export const state = () => ({
  membre: {},
  membres: [],
  membreForm: {},
  registrationStep:1,
  loading: false,
  error: false,
});

export const mutations = {
  SET_MEMBRE(state, membre) {
    state.membre = membre
  },
  SET_MEMBRES(state, payload) {
    state.membres = payload;
  },
  ADD_MEMBRE_FORM(state, payload={}) {
    payload.forEach(function (key, val) {
      state.membreForm[key]= val
    })
  },
  SET_MEMBRE_FORM(state, payload={}) {
    payload.forEach(function(key, val) {
      state.membreForm[key]= val
    })
  },
  CLEAR_MEMBRE_FORM(state) {
    state.membreForm = {}
  },
  SET_REGISTRATION_STEP(state, payload) {
    state.registrationStep = payload;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchMembres({ commit }, { limit=50, offset=0}) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const response = await this.$services.membreProfils.list(this.$axios, '', limit, offset);
      const datas = response.results;
      commit('SET_MEMBRES', datas);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
  async getMembre({ commit }, { slug=""}) {
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
      const response = await this.$services.membreProfils.read(this.$axios, slug);
      const datas = response.results;
      commit('SET_MEMBRE', datas);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

   async createMembre({commit, dispatch}, data){
    commit('SET_LOADING', true);
    commit('SET_ERROR', null);
    try {
       const response =  await this.$services.membreProfils.create(this.$axios, data)
      commit('SET_MEMBRE', response);
      setTimeout(  function (){
      commit('SET_LOADING', false);
      }, 2000)
      this.$toast.success("profil crée avec succès")
    }catch (e) {
      commit('SET_LOADING', false)
      console.error("create:", e)

      commit('SET_ERROR', true)
      return Promise.reject(e)
    }
  },

  async updateMembre({commit}, {slug, data}){
   commit('SET_LOADING', true);
    commit('SET_ERROR', null);

    try {
      const response = await this.$services.membreProfils.update(this.$axios, slug,  data)
      console.log('update user response:', response)
      this.$toast.succes('Profil mise à jours avec succès')
      const _this = this
      commit('SET_LOADING', false)
      setTimeout(function () {
        _this.$router.push({name:'authentifications-connexion'})
      }, 2000)
      console.log("response update:", response)
    }catch (e) {
      const msg = {}
      for (const key in e) {
        msg[key]= e[key]
      }
      console.log("msg", e)
      commit('SET_ERROR', msg)
      commit('SET_LOADING', false)
      return Promise.reject(e)

    }
    commit('SET_LOADING', false)
  },
  switchForm(context, formIndex){
    context.commit('SET_LOADING', true)
    setTimeout(function (){
      context.commit('SET_REGISTRATION_STEP', formIndex)
      context.commit('SET_LOADING', false)
    }, 2000)
  }
};


export const getters = {
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
  getRegistrationStep(state) {
    return state.registrationStep
  },
  getMembre(state){
    return state.membre
  }
};
