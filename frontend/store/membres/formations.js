
export default {
    state: () => ({
        formations: [],
        active_formation: {},
        documents: [],
        active_document: {},
        loading: false,
        error: null,
    }),

    mutations: {
        SET_FORMATIONS(state, formations) {
            state.formations = formations;
        },
        SET_ACTIVE_FORMATION(state, formation) {
            state.active_formation = formation;
        },
        SET_LOADING(state, loading) {
            state.loading = loading;
        },
        SET_ERROR(state, error) {
            state.error = error;
        },
    },

    actions: {
        async fetchFormations({ commit }, {slug,  limit=100, offset }) {
            commit('SET_LOADING', true);
            try {
                const formations = await this.$services.membresFormations.list(this.$axios, slug, limit, offset);
                commit('SET_FORMATIONS', formations.results);
                commit('SET_LOADING', false);
            } catch (error) {
                commit('SET_LOADING', false);
                commit('SET_ERROR', error);
            }
        },

        async readFormation({ commit }, slug) {
            commit('SET_LOADING', true);
            try {
                const formation = await this.$services.membresFormations.read(this.$axios, slug);
                commit('SET_ACTIVE_FORMATION', formation);
                commit('SET_LOADING', false);
            } catch (error) {
                commit('SET_LOADING', false);
                commit('SET_ERROR', error);
            }
        },

    },

    getters: {
        getFormations(state) {

            return state.formations;
        },
        getActiveDocument(state) {
            return state.active_formation;
        },
        isLoading(state) {
            return state.loading;
        },
        getError(state) {
            return state.error;
        },
    }
}
