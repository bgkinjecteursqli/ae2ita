

export default {
    state: () => ({
        documents: [],
        active_document: {},
        loading: false,
        error: null,
    }),

    mutations: {
        SET_DOCUMENTS(state, documents) {
            state.documents = [...state.documents, documents];
        },
        REMOVE_DOCUMENT(state, slug) {
            state.documents = state.documents.filter(doc => doc.slug !== slug);
        },
        SET_ACTIVE_DOCUMENT(state, document) {
            state.active_document = document;
        },
        SET_LOADING(state, loading) {
            state.loading = loading;
        },
        SET_ERROR(state, error) {
            state.error = error;
        },
    },

    actions: {

        async fetchDocuments({ commit }, { search, limit, offset }) {
            commit('SET_LOADING', true);
            try {
                const documents = await this.$services.membreDocuments.list(this.$axios, search, limit, offset);
                commit('SET_DOCUMENTS', documents.results);
                commit('SET_LOADING', false);
            } catch (error) {
                commit('SET_LOADING', false);
                commit('SET_ERROR', error);
            }
        },
        async readDocument({ commit }, slug) {
            commit('SET_LOADING', true);
            try {
                const document = await this.$services.membreDocuments.read(this.$axios, slug);
                commit('SET_ACTIVE_DOCUMENT', document);
                commit('SET_LOADING', false);
            } catch (error) {
                commit('SET_LOADING', false);
                commit('SET_ERROR', error);
            }
        },
        async createDocument({ commit }, data) {
            commit('SET_LOADING', true);
            try {
                const document = await this.$services.membreDocuments.create(this.$axios, data);
                commit('SET_DOCUMENTS', document.results);
                commit('SET_LOADING', false);
            } catch (error) {
                commit('SET_LOADING', false);
                commit('SET_ERROR', error);
            }
        },
        async deleteDocument({ commit }, slug) {
            commit('SET_LOADING', true);
            try {
                const document = await this.$services.membreDocuments.delete(this.$axios, slug);
                console.log(document);
                commit('REMOVE_DOCUMENT', slug)
                commit('SET_LOADING', false);
            } catch (error) {
                commit('SET_LOADING', false);
                commit('SET_ERROR', error);
            }
        },
    },

    getters: {
        getDocuments(state) {
            return state.documents;
        },
        getActiveDocument(state) {
            return state.active_document;
        },
        isLoading(state) {
            return state.loading;
        },
        getError(state) {
            return state.error;
        },
    }
}