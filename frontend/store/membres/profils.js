// store/promotionsStore.js

export const state = () => ({
  promotions: [],
  activePromotion: {},
  loading: false,
  error: null,
});

export const mutations = {
  SET_PROMOTIONS(state, promotions) {
    state.promotions = [...state.promotions, ...promotions];
  },
  SET_ACTIVE_PROMOTION(state, promotion) {
    state.activePromotion = promotion;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchPromotions({ commit }, { search, limit, offset }) {
    commit('SET_LOADING', true);
    try {
      const response = await this.$services.promotions.list(
        this.$axios,
        search,
        limit,
        offset
      );
      const promotions = response.results;
      commit('SET_PROMOTIONS', promotions);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async fetchPromotionBySlug({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      const promotion = await this.$services.promotions.read(this.$axios, slug);
      commit('SET_ACTIVE_PROMOTION', promotion);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
};

export const getters = {
  getPromotions(state) {
    return state.promotions;
  },
  getActivePromotion(state) {
    return state.activePromotion;
  },
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
  getPromotionBySlug: (state) => (slug) => {
    return state.promotions.find((promotion) => promotion.slug === slug);
  },
};
