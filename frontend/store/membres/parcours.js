// store/parcoursStore.js

export const state = () => ({
  parcours: [],
  activeParcours: {},
  loading: false,
  error: null,
});

export const mutations = {
  SET_PARCOURS(state, parcours) {
    state.parcours = [...state.parcours, ...parcours];
  },
  SET_ACTIVE_PARCOURS(state, parcours) {
    state.activeParcours = parcours;
  },
  SET_LOADING(state, loading) {
    state.loading = loading;
  },
  SET_ERROR(state, error) {
    state.error = error;
  },
};

export const actions = {
  async fetchParcours({ commit }, { search, limit, offset }) {
    commit('SET_LOADING', true);
    try {
      const response = await this.$services.membreParcours.list(
        this.$axios,
        search,
        limit,
        offset
      );
      const parcours = response.results;
      commit('SET_PARCOURS', parcours);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async fetchParcoursBySlug({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      const parcours = await this.$services.membreParcours.read(this.$axios, slug);
      commit('SET_ACTIVE_PARCOURS', parcours);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async createParcours({ commit }, data) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.membreParcours.create(this.$axios, data);
      commit('SET_PARCOURS', result);
      commit('SET_LOADING', false);
      this.$toast.success('Parcours créé avec succès');
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async updateParcours({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.membreParcours.update(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_PARCOURS', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async partialUpdateParcours({ commit }, { slug, data }) {
    commit('SET_LOADING', true);
    try {
      const result = await this.$services.membreParcours.partialUpdate(
        this.$axios,
        slug,
        data
      );
      commit('SET_ACTIVE_PARCOURS', result);
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },

  async deleteParcours({ commit }, slug) {
    commit('SET_LOADING', true);
    try {
      await this.$services.membreParcours.delete(this.$axios, slug);
      commit('SET_ACTIVE_PARCOURS', {});
      commit('SET_LOADING', false);
    } catch (error) {
      commit('SET_LOADING', false);
      commit('SET_ERROR', error);
    }
  },
};

export const getters = {
  getParcours(state) {
    return state.parcours;
  },
  getActiveParcours(state) {
    return state.activeParcours;
  },
  isLoading(state) {
    return state.loading;
  },
  getError(state) {
    return state.error;
  },
  getParcoursBySlug: (state) => (slug) => {
    return state.parcours.find((parcours) => parcours.slug === slug);
  },
};
