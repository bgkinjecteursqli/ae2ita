export const state = () => ({
    missions: [
      {
        title: "Favoriser un cadre d'échange permanent",
        description: "Création d'opportunités d'échange entre les Elèves-Ingénieurs et Ingénieurs de Techniques Agricoles.",
        icon: "fa-comments",
      },
      {
        title: "Insertion professionnelle",
        description: "Faciliter l'insertion des diplômés dans la vie active.",
        icon: "fa-briefcase",
      },
      {
        title: "Consolider les liens de corporation",
        description: "Création d'un fort sentiment d'appartenance et de solidarité entre les membres.",
        icon: "fa-users",
      },
      {
        title: "Renforcement des Capacités",
        description: "Contribuer à l'amélioration et à l'exécution des programmes de formation initiale.",
        icon: "fa-cogs",
      },
     
    ],
  });
  
  export const getters = {
    getMissions: (state) => {
      return state.missions;
    },
  };
  