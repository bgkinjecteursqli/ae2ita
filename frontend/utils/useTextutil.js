import { ref, computed } from 'vue'

export function useTextTruncat(text=""){
  const textRef = ref(text)
  const limitRef =ref(250)
  const textTruncated =  computed(()=> {
    if (textRef.value.length > limitRef.value) {
      const truncatedContent = text.slice(0, limitRef.value);
      return truncatedContent +'...'
    }else {
      return text;
    }
  })
  const setLimit = (limit) =>{
    limitRef.value = limit
  }
  const setText = (value) =>{
    textRef.value = value
  }

  return{
    setLimit,
    setText,
    textTruncated
  }
}
