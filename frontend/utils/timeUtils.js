
// Fonction pour générer le timestamp d'expiration de la session (une semaine plus tard)



export function generateExpirationTimestamp(day=7) {
  const now = new Date();
  const oneWeekLater = new Date(now.getTime() + day * 24 * 60 * 60 * 1000);
  return oneWeekLater.getTime();
}

// Fonction pour chiffrer le timestamp d'expiration
export function encryptTimestamp(timestamp) {
  const CryptoJS = require('crypto-js')
  const secretKey = process.env.SECRET_KEY;
  return CryptoJS.AES.encrypt(timestamp, secretKey).toString();
}

export function encryptAuth(auth) {
  const CryptoJS = require('crypto-js')
  const secretKey = process.env.SECRET_KEY;
  return CryptoJS.AES.encrypt(JSON.stringify(auth), secretKey).toString();
}

export function decryptAuth(authCypher) {
  const CryptoJS = require('crypto-js')
  const secretKey = process.env.SECRET_KEY;
  const bytes  = CryptoJS.AES.decrypt(authCypher, secretKey);
  return JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
}

// Fonction pour déchiffrer le timestamp d'expiration
export function decryptTimestamp(encryptedTimestamp) {
  const CryptoJS = require('crypto-js')
  const secretKey = process.env.SECRET_KEY;
  const decrypted = CryptoJS.AES.decrypt(encryptedTimestamp, secretKey)
  const originalText = decrypted.toString(CryptoJS.enc.Utf8)
  return parseInt(originalText, 10);
}


export  function generateSecret(bits=32){
  const crypto = require('crypto');
 return  crypto.randomBytes(bits).toString('hex');
}

// Générer une clé de hachage unique pour le navigateur
export function encryptUniqueHash(string) {
  const SHA256 = require('crypto-js/sha256')
  const CryptoJS = require('crypto-js')
  const secretKey = process.env.SECRET_KEY;
  const bytes = SHA256(string).toString()
  return CryptoJS.AES.encrypt(bytes, secretKey).toString();
}

export function decryptUniqueHash(encryptedHash) {
  const CryptoJS = require('crypto-js');
  const secretKey = process.env.SECRET_KEY;
  try {
    const bytes = CryptoJS.AES.decrypt(encryptedHash, secretKey);
    const originalText = bytes.toString(CryptoJS.enc.Utf8);
    return originalText;
  } catch (error) {
    // En cas d'erreur de décryptage (hash invalide ou clé incorrecte), renvoyer null ou une valeur par défaut
    return null;
  }
}
