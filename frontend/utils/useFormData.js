// useFormData.js
import { ref } from 'vue';

export default function useFormData() {
  const createFormData = (userForm) => {
    const formData = new FormData();

    // Convert the reactive object to a regular object
    const plainObject = ref(userForm).value;

    for (const key in plainObject) {
      formData.append(key, plainObject[key]);
    }

    return formData;
  };

  return {
    createFormData,
  };
}
