// utils/auth.js

// Fonction pour définir le token dans le local storage ou les cookies
export function setToken(token) {
    // Utilisez ici le stockage de votre choix, par exemple, les cookies ou le local storage
    // Si vous utilisez les cookies, vous pouvez installer le package 'cookie-universal-nuxt'
    // et remplacer app.$cookies par this.$cookies
    if (process.client) {
      localStorage.setItem('token', token); // Stockez le token dans le local storage du navigateur
      // Si vous utilisez les cookies, vous pouvez utiliser cette ligne au lieu du local storage :
      // this.$cookies.set('token', token, { path: '/', maxAge: 604800 }); // Stockez le token dans les cookies avec une expiration d'une semaine (en secondes)
    }
  }

  // Fonction pour supprimer le token du local storage ou des cookies
  export function removeToken() {
    // Utilisez ici le stockage de votre choix, par exemple, les cookies ou le local storage
    // Si vous utilisez les cookies, vous pouvez installer le package 'cookie-universal-nuxt'
    // et remplacer app.$cookies par this.$cookies
    if (process.client) {
      localStorage.removeItem('token'); // Supprimez le token du local storage du navigateur
      // Si vous utilisez les cookies, vous pouvez utiliser cette ligne au lieu du local storage :
      // this.$cookies.remove('token'); // Supprimez le token des cookies
    }
  }
