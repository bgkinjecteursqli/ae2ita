import { defineComponent, reactive, toRefs } from 'vue';

export default defineComponent({
  name: 'UseLoading',
  setup() {
    const state = reactive({
      loading: false,
    });

    const loading = () => {
      state.loading = true;
    };

    const finish = () => {
      state.loading = false;
    };

    return {
      ...toRefs(state),
      loading,
      finish,
    };
  },
});
