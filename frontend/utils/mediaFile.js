
export function telechargerMedia(url) {

  const link = document.createElement('a');
  link.href = url;
  link.target = '_blank';
  link.download = url.split('/').pop();
  link.click();
}
