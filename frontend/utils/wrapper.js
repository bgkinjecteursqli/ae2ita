export default function asyncWrapper(fn) {
    return async function() {
      try {
        return await fn();
      } catch (e) {
        console.error(e);
        throw e;
      }
    };
  }