// composables/useLoading.js
import {  onMounted, onUnmounted, ref} from 'vue';

export default function useAppLoading() {
  const onLoading = ref(true);

  const handleLoadComplete = () => {
    onLoading.value = false;
  };

  onMounted(() => {
    window.addEventListener('load', handleLoadComplete);
  });

  onUnmounted(() => {
    window.removeEventListener('load', handleLoadComplete);
  });

  return {
    onLoading,
  };
}
