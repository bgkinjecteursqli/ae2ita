// customComposition.js
import { ref, onMounted, onBeforeUnmount, computed } from 'vue';

export function useDeviceType() {
  const deviceType = ref(''); // 'mobile', 'tablet', 'desktop'
  const screenWidth = ref(0);
  try {
   screenWidth.value = window.innerWidth
  }catch (e) {
    screenWidth.value = 0
  }
  const isMobile = computed(() => screenWidth.value < 768);
  const isTablet = computed(() => screenWidth.value >= 768 && screenWidth.value < 992);
  const isDesktop = computed(() => screenWidth.value >= 992);
  const checkDeviceType = () => {
    screenWidth.value = window.screen.width;
    if (screenWidth.value < 768) {
      deviceType.value = 'mobile';
    } else if (screenWidth.value < 1024) {
      deviceType.value = 'tablet';
    } else {
      deviceType.value = 'desktop';
    }
  };

  onMounted(() => {
     screenWidth.value = window.innerWidth
    checkDeviceType();
    window.addEventListener('resize', checkDeviceType);
  });

  onBeforeUnmount(() => {
    window.removeEventListener('resize', checkDeviceType);
  });

  return {
    deviceType,
    isMobile,
    isTablet,
    isDesktop,
  };
}
