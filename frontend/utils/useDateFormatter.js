import { ref, computed, onMounted, onBeforeUnmount } from 'vue';
import moment from 'moment';

moment.locale('fr');

export function useEventDateFormatter(startDate, endDate) {
  moment.locale('fr');
  const startDateRef = ref(startDate);
  const endDateRef = ref(endDate);

  const formatFrenchMonth = (date) => {
    const monthIndex = moment(date).month();
    return moment.monthsShort()[monthIndex].toUpperCase();
  };

  const formattedStartDate = computed(() => {
    const day = moment(startDateRef.value).format('DD');
    const month = formatFrenchMonth(startDateRef.value);
    return `${day} ${month}`;
  });

  const formattedEndDate = computed(() => {
    const day = moment(endDateRef.value).format('DD');
    const month = formatFrenchMonth(endDateRef.value);
    return `${day} ${month}`;
  });

  const formattedYear = computed(() => {
    return `${moment(startDateRef.value).format('YYYY')}`;
  });

  return {
    formattedStartDate,
    formattedEndDate,
    formattedYear,
  };
}

export function useFromDataFormatter(date){
   const dateRef = ref(date)
  const timer = ref(0)
  const currentDate = ref(moment());

  const dateFormatted = computed(()=> {
      const targetDate = moment(dateRef.value);
      const diffInSeconds = currentDate.value.diff(targetDate, 'seconds');
      const diffInMinutes = currentDate.value.diff(targetDate, 'minutes');
      const diffInHours = currentDate.value.diff(targetDate, 'hours');
      const diffInDays = currentDate.value.diff(targetDate, 'days');
      const diffInWeeks = currentDate.value.diff(targetDate, 'weeks');
      const diffInMonths = currentDate.value.diff(targetDate, 'months');
      const diffInYears = currentDate.value.diff(targetDate, 'years');

      if (diffInSeconds < 60) {
        return `il y a ${diffInSeconds} seconde${diffInSeconds > 1 ? 's' : ''}`;
      } else if (diffInMinutes < 60) {
        return `il y a ${diffInMinutes} minute${diffInMinutes > 1 ? 's' : ''}`;
      } else if (diffInHours < 24) {
        return `il y a ${diffInHours} heure${diffInHours > 1 ? 's' : ''}`;
      } else if (diffInDays < 7) {
        return `il y a ${diffInDays} jour${diffInDays > 1 ? 's' : ''}`;
      } else if (diffInWeeks < 4) {
        return `il y a ${diffInWeeks} semaine${diffInWeeks > 1 ? 's' : ''}`;
      } else if (diffInMonths < 12) {
        return `il y a ${diffInMonths} mois`;
      } else {
        return `il y a ${diffInYears} an${diffInYears > 1 ? 's' : ''}`;
      }
    })

  onMounted(()=>{
     timer.value = setInterval(()=>{
       currentDate.value = moment()
    },5000)
  })
  onBeforeUnmount(()=>{
    clearInterval(timer.value)
  })

  return {
    dateFormatted
  }
}
