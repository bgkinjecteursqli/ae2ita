import moment from 'moment'

export  function  dateCreation(dateString){
        moment.locale('fr')
        return moment(dateString, 'YYYY-MM-DD').format('DD MMMM YYYY')
}
export   function  dateCloture(dateString){
        moment.locale('fr')
        const now = moment()
      const d = moment(dateString, 'YYYY-MM-DD')
      const  isValidte = now.isSameOrBefore(d)
      if(isValidte){
        return d.format('DD MMMM YYYY')
      }
      return 'Expiré'
}
export   function tempsRestant(dateString){
     moment.locale('fr')
      const now = moment()
      const d = moment(dateString, 'YYYY-MM-DD')
     const  isValidte = now.isSameOrBefore(d)
      if(isValidte){
        return d.fromNow()
      }
      return 'Expiré'
    }
