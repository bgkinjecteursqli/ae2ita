export default {
  ssr:false,
//   watch:true,
//   watchOptions:{
// files:['components/**/*.vue','pages/**/*.vue', 'store/**/*.js'],
//   },
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Plateforme',
    titleTemplate: 'AE2ITA - %s',
    htmlAttrs: {
      lang: 'fr',
      amp:true
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: "AE2ITA crée le 09 juin 2002, est une association à caractère apolitique regroupant en son sein les élèves Ingénieurs des Techniques Agricoles régulièrement inscrits à l'École Supérieurs d'Agronomie (ESA) de l'INP-HB de Yamoussoukro et des Ingénieurs des Techniques agricoles, anciens élèves de l'ex-IAB (Institut Agricole de Bouaké) ou de l'ESA." },
      { name: 'format-detection', content: 'telephone=no' },
      { title: "AE2ITA: PlateForme Digitale" },
      {
          hid: 'twitter:title',
          name: 'twitter:title',
          content: "AE2ITA / INP-HB, ESA: Amicales Des Elèves des ingénieurs  et Ingénieurs des techniques agricoles "
        },
        {
          hid: 'twitter:description',
          name: 'twitter:description',
          content: "AE2ITA / INP-HB, ESA:: Amicales Des Elèves des ingénieurs  et Ingénieurs des techniques agricoles"
        },
        {
          hid: 'twitter:image',
          name: 'twitter:image',
          content: 'https://ae2ita.com/apple-touch-icon.png'
        },
        {
          hid: 'twitter:image:alt',
          name: 'twitter:image:alt',
          content: "AE2ITA / INP-HB, ESA: Amicales Des Elèves des ingénieurs  et Ingénieurs des techniques agricoles"
        },
      {
        hid: 'og:title',
        property: 'og:title',
        content: "AE2ITA / INP-HB, ESA:Amicales Des Elèves des ingénieurs  et Ingénieurs des techniques agricoles (AE2ITA)"
      },
      {
        hid: 'og:description',
        property: 'og:description',
        content: "AE2ITA crée le 09 juin 2002, est une association à caractère apolitique regroupant en son sein les élèves Ingénieurs des Techniques Agricoles régulièrement inscrits à l'École Supérieurs d'Agronomie (ESA) de l'INP-HB de Yamoussoukro et des Ingénieurs des Techniques agricoles, anciens élèves de l'ex-IAB (Institut Agricole de Bouaké) ou de l'ESA."
      },
      {
        hid: 'og:image',
        property: 'og:image',
        content: "https://ae2ita.com/apple-touch-icon.png"
      },
      {
        hid: 'og:image:secure_url',
        property: 'og:image:secure_url',
        content: "https://ae2ita.com/apple-touch-icon.png"
      },
      {
        hid: 'og:image:alt',
        property: 'og:image:alt',
        content: "Amicales Des Elèves des ingénieurs  et Ingénieurs des techniques agricoles (AE2ITA)"
      },
      { name: "google-site-verification", content:"WnNXOlPGwmRUhtcdxgAgSYEThMhZbWG2ef7qR9lJh_U"}
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'stylesheet', href: 'https://use.fontawesome.com/releases/v5.11.2/css/all.css' },
      { rel: 'stylesheet', href: 'https://unpkg.com/swiper/swiper-bundle.min.css' }
    ],
    script: [
      {
        src: 'https://code.jquery.com/jquery-3.4.1.min.js' ,type:"text/javascript", body: true, ssr:false
      },
      { src: '/js/jquery-3.4.1.min.js',  body:true, mode:'client' },
      { src: '/js/bootstrap.min.js', body: true, mode:'client' },
      { src: '/js/popper.min.js',  body:true, mode:'client' },
      { src: '/js/mdb.min.js', body:true, mode:'client' },
      { src: '/js/script.js',body:true, mode:'client' }
    ]
  },
  alias: {
  },
  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
  "~/assets/css/style.css",
    "~/assets/css/mdb.min.css",
    "~/assets/css/bootstrap.css",
    "~/assets/css/transitions.css",
    "swiper/swiper.css",
    '@fancyapps/ui/dist/fancybox/fancybox.css',
    'video.js/dist/video-js.css',
    'swiper/swiper.css',
    'quill/dist/quill.snow.css'
    ],
  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [

    { src: '~/plugins/axios.js'},
    { src: '~/plugins/services.js'},
    { src: '~/plugins/client-only/fancybox.js', mode: 'client' },
    { src: '~/plugins/client-only/videojs.js', mode: 'client' },
    { src: '~/plugins/client-only/jquery.plugin.js',  mode: 'client'},
    { src: '~/plugins/client-only/aos.client.js', mode: 'client', body:true  },
    { src: '~/plugins/client-only/swiper.client.js', mode: 'client' },
    { src: '~/plugins/client-only/swiper.js', mode: 'client' },
    {src: '~/plugins/metaHeader.js'},
    {src: '~/plugins/client-only/quill.js', mode: 'client', sss:false},
    {src: '~/plugins/client-only/vuequill.js', mode: 'client', sss:false}

    // { src: '~/plugins/client-only/counter.js', mode: 'client' },
  ],
  middleware: ['auth', 'csrfToken', 'session', 'simpleAuth','simpleSession', 'userAgent', "closed"],
  routes:{
    middleware: ['closed']
  },
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,
  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    // 'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
    'cookie-universal-nuxt',
    'vue-toastification/nuxt',
    '@nuxt/devtools',
    '@nuxtjs/sitemap'

  ],
   sitemap: {
    // options
     hostname: process.env.HOSTNAME,
    gzip: true,
    exclude: [
      '/api/**',
      '/admin/**'
    ]
  },

  toast:{
    transition: "Vue-Toastification__bounce",
    position: "top-right",
    timeout: 5000,
    closeOnClick: true,
    pauseOnFocusLoss: true,
    pauseOnHover: true,
    draggable: true,
    draggablePercent: 0.6,
    showCloseButtonOnHover: false,
    hideProgressBar: false,
    closeButton: "button",
    icon: "fas fa-rocket",
    rtl: false
  },
  device: {
    refreshOnResize: true
  },
  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    // Workaround to avoid enforcing hard-coded localhost:3000: https://github.com/nuxt-community/axios-module/issues/308
    baseURL: process.env.API_BASE_URL,
    credentials:false,
    proxyHearders:false,
    axios: {
      proxy: true
    }
  },
proxy: {
  '/api/': { target: process.env.HOSTNAME, pathRewrite: {'^/api/': ''}, changeOrigin: true }
  },
  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    buildDir: 'dist'
  },
  loading: {
    color: 'green',
    height: '3px'
  },
  // loading: '~/components/TheLoading.vue',
  generate: {
    fallback: 'error.html', // ou toute autre valeur par défaut que vous avez définie
    error: 'pages/error.vue',// Chemin vers votre page d'erreur personnalisée
  },
  pageTransition: {
    name: 'fade',
    mode: 'out-in'
    },
  env: {
    API_BASE_URL: process.env.API_BASE_URL,
    SECRET_KEY: process.env.SECRET_KEY,
    CDN_BASE_URL:process.env.CDN_BASE_URL
  },
  server: {
    port: process.env.PORT || 3000
  }
}
