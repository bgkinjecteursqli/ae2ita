from rest_framework import serializers


class AuthSerializer(serializers.Serializer):
    # membre_email = serializers.EmailField()
    email = serializers.EmailField(allow_blank=False)
    #username = serializers.CharField(allow_blank=False, max_length=50)
    password = serializers.CharField(allow_blank=False, max_length=15)