# views.py
from django.contrib.auth import get_user_model
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.permissions import AllowAny
from rest_framework_simplejwt.views import TokenObtainPairView
from auths.serializers import AuthSerializer
from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,

)

MyUser: User = get_user_model()


class CustomTokenObtainPairView(APIView):
    permission_classes = [AllowAny]

    def post(self, request):
        serializer = AuthSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        password = serializer.validated_data.get('password')
        email = serializer.validated_data.get('email')

        user = get_object_or_404(MyUser, email=email)
        if user.check_password(raw_password=password):
            # If authentication is successful, generate tokens
            refresh = RefreshToken.for_user(user)
            access_token = str(refresh.access_token)
            return Response({'access_token': access_token, 'refresh_token': refresh}, status=status.HTTP_201_CREATED)
        return Response({'message': "données de connexion invalide"}, status=status.HTTP_400_BAD_REQUEST)


class MyTokenObtainPairView(TokenObtainPairView):
    username_field = 'email'


class MyTokenRefreshView(TokenRefreshView):
    pass


class CustomTokenRefreshView(APIView):
    def post(self, request):
        refresh_token = request.data.get('refresh_token')

        # Vérifier si le jeton de rafraîchissement est valide
        try:
            refresh = RefreshToken(refresh_token)
            refresh.verify()
        except:
            # Gérer l'erreur si le jeton de rafraîchissement est invalide
            return Response({'error': 'Invalid refresh token'}, status=400)

        # Générer un nouveau jeton d'accès
        access_token = str(refresh.access_token)

        return Response({'access_token': access_token})
