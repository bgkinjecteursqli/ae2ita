# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

project = 'ae2ita backend'
copyright = '2023, BorisBob91'
author = 'BorisBob91'
release = '1.0.1'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = []
source_suffix = ['.rst', '.md', '.py']
import os
import sys
sys.path.insert(0, r'C:\Users\BorisBob\Documents\GitHub\aktaion\ae2ita\backend')
source_dirs = [r'C:\Users\BorisBob\Documents\GitHub\aktaion\ae2ita\backend']
templates_path = ['_templates']
exclude_patterns = []

language = 'fr'

# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'alabaster'
html_static_path = ['_static']


