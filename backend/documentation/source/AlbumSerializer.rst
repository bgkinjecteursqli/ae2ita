"""
serializers.py
===========

This module defines Django REST Framework serializers for the Albums app models.

Classes
-------

.. autoclass:: AlbumImageSerializer
   :members:
   :undoc-members:

.. autoclass:: AlbumVideoSerializer
   :members:
   :undoc-members:

.. autoclass:: AlbumListSerializer
   :members:
   :undoc-members:

.. autoclass:: AlbumDetailSerializer
   :members:
   :undoc-members:

The ``AlbumImageSerializer`` and ``AlbumVideoSerializer`` serialize the
``AlbumImage`` and ``AlbumVideo`` models respectively.

The ``to_representation()`` methods prepend the CDN base URL to the
``fichier`` and ``thumbnail`` fields if needed.

The ``AlbumListSerializer`` is used for list views of albums, adding
``images_count`` and ``videos_count`` fields.

The ``AlbumDetailSerializer`` includes the reverse relationships for
full album details.

The ``create()`` method of ``AlbumListSerializer`` handles saving images
and videos along with the album being created.


"""