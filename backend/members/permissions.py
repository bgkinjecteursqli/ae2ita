from rest_framework import permissions


class IsAdminOrOwner(permissions.BasePermission):
    def has_object_permission(self, request, view, obj):
        # Autoriser les administrateurs à effectuer toutes les actions
        if request.user.is_superuser:
            return True

        # Autoriser le propriétaire de l'objet à effectuer des actions de mise à jour et de suppression
        if request.user == obj.membre:
            return True

        # Refuser les autres actions
        return False

    def has_permission(self, request, view):
        return True
