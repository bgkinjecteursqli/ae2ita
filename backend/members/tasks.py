# members/tasks.py

from celery import shared_task
from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags
from datetime import datetime, timedelta
from django.contrib.auth.tokens import default_token_generator
from django.utils.encoding import force_bytes
from django.utils.http import urlsafe_base64_encode
from backend import settings
from .exceptions import TokenExpiredException, TokenInvalidException
from .models import ConfirmationEmail, Membre
import base64


@shared_task
def send_confirmation_email_task(membre_id, token):
    membre = Membre.objects.get(pk=membre_id)

    uid = urlsafe_base64_encode(force_bytes(membre.pk))
    activation_link = f'{settings.DOMAIN_LiNK}/api/activations/?token={token}&uid={uid}'

    subject = 'Confirmation de compte'
    html_message = render_to_string('confirmation_email.html', {
        'user': membre.get_nom_complet(),
        'genre': 'Monsieur ' if membre.sexe.startswith('M') else ' Madame/Mademoiselle ',
        'token': token,
        'activation_link': activation_link
    })
    plain_message = strip_tags(html_message)
    from_email = 'noreply@ae2ita.com'
    to_email = [membre.email1]

    try:
        send_mail(subject=subject, message=plain_message, from_email=from_email, recipient_list=to_email,
                  html_message=html_message)
    except Exception as e:
        pass


@shared_task
def send_confirmation_success_email_task(membre_id):
    membre = Membre.objects.get(pk=membre_id)
    login_link = '/authentifications/connexions/'

    html_message = render_to_string('confirmation_succes_email.html', {
        'user': membre.get_nom_complet(),
        'genre': 'Monsieur ' if membre.sexe.startswith('M') else ' Madame/Mademoiselle ',
        'email': membre.email1,
        'login_link': login_link
    })
    plain_message = strip_tags(html_message)

    send_mail(
        subject="Compte AE2ITA activé avec succès",
        message=plain_message,
        from_email="noreply@ae2ita.com",
        recipient_list=[membre.email1],
        fail_silently=False,
        html_message=html_message
    )


@shared_task
def verify_confirmation_token_task(token):
    from django.utils import timezone
    try:
        # Recherche du courrier électronique de confirmation correspondant au token
        confirmation_email = ConfirmationEmail.objects.get(token=token)

        # Vérification de la date d'expiration du token
        expiration_date = confirmation_email.created_at + timedelta(days=2)
        now = timezone.now()
        if now > expiration_date:
            confirmation_email.token = ''
            confirmation_email.save()
            raise TokenExpiredException()

        # Vérification de la validité du token
        user = confirmation_email.membre.user
        if not default_token_generator.check_token(user, token):
            raise TokenInvalidException()

        # Le token et la date d'expiration sont valides
        return True
    except ConfirmationEmail.DoesNotExist:
        raise TokenInvalidException()


# create celery tast thad send message tout all memebers

@shared_task
def notification_publication_email_task(titre, date, lien, description):
    membres = Membre.objects.filter(is_email_confirmed=True)
    email_list = [membre.email1 for membre in membres]

    subject = titre
    html_message = render_to_string('notification_publication.html', {
        'titre': titre,
        'date': date,
        'lien': lien,
        'description': description
    })
    plain_message = strip_tags(html_message)
    from_email = 'noreply@ae2ita.com'
    to_email = email_list

    try:
        send_mail(subject=subject, message=plain_message, from_email=from_email, recipient_list=to_email,
                  html_message=html_message)
    except Exception as e:
        pass

