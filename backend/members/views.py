import sqlite3
from datetime import date

from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.db import transaction
from django.shortcuts import get_object_or_404
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from rest_framework.status import HTTP_400_BAD_REQUEST
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema

from rest_framework import viewsets, permissions, generics, status, mixins
from rest_framework.decorators import action, api_view, permission_classes
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from backend.serialiers import MultipleSerializerMixin

from formations.models import InscriptionFormation
from formations.serializers import FormationListSerializer, FormationDetailSerializer
from members.exceptions import TokenInvalidException, TokenExpiredException
from members.models import Membre, Parcours, DocumentPersonnel, PublicationAine, Promotion, MembreBureau, \
    ConfirmationEmail, Entreprise

from members.serialiers import MembreListSerializer, MembreDetailSerializer, ParcoursSerializer, \
    DocumentPersonnelSerializer, \
    PublicationAineSerializer, MemberAnniversaireSerializer, PromotionListSerializer, PromotionDetailSerializer, \
    MembreBureauSerializer, UserSerializer, EntrepriseSerializer, ProfilSerializer, EntrepreneurSerializer
from members.utils import verify_confirmation_token, send_confirmation_email, send_confirmation_success_email, \
    is_valid_email

UserModel = get_user_model()


class MembreViewSet(MultipleSerializerMixin, viewsets.ModelViewSet):
    queryset = Membre.objects.all()
    serializer_class = MembreListSerializer
    detail_serializer_class = MembreDetailSerializer
    permission_classes = [permissions.AllowAny]
    lookup_field = 'slug'
    filterset_fields = ["nom", "entrepreneur"]

    @transaction.atomic
    def create(self, request, *args, **kwargs):

        # Création de l'utilisateur
        data:dict = request.data
        user_data:dict = {
                    'email': data.get('email1'),
                    'username': data.get('email1'),
                    'first_name': data.get('nom'),
                    'last_name': data.get('prenoms'),
                    'password': data.get('password'),
                    'is_active': False,
                    'is_superuser': False,
                    'is_staff': False
            }
        try:
            is_user = User.objects.get(email=user_data['email'])
            return Response(data={"username": "user with email: {} exist".format(user_data['email'])}, status=status.HTTP_400_BAD_REQUEST)
        except Exception as e:
            user_profil = User(**user_data)
            user_profil.set_password(user_data.get('password'))
            user_profil.save()
            request.data['user'] = user_profil.pk
        # Création du membre associé à l'utilisateur
        membre_serializer = self.get_serializer(data=request.data)
        try:
            membre_serializer.is_valid(raise_exception=True)
        except Exception as e:
            return Response(data=membre_serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        membre_serializer.save()
        return Response(membre_serializer.data, status=status.HTTP_201_CREATED)

    @transaction.atomic
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        for field in Membre._meta.fields:
            if field.name is not "user":
                instance.__dict__[field.name] =request.data.get(field.name, instance.__dict__[field.name])

        serializer = self.get_serializer(instance, data=instance.__dict__,  partial=kwargs.get('partial', False))
        serializer.is_valid(raise_exception=True)
        # Mettez à jour l'utilisateur associé s'il y a des changements dans les champs pertinents
        user_data = {
            'email': request.data.get('email1', instance.email1),
            'username': request.data.get('email1', instance.email1),
            'first_name': request.data.get('nom', instance.nom),
            'last_name': request.data.get('prenoms', instance.prenoms),
        }

        # Mise à jour des champs utilisateur
        for key, value in user_data.items():
            setattr(instance.user, key, value)

        # Si le mot de passe est fourni, mettez à jour le mot de passe
        password = request.data.get('password')
        if password:
            instance.user.set_password(password)

        instance.user.save()

        # Mettez à jour le membre associé
        serializer.save()

        return Response(serializer.data)

    @swagger_auto_schema(
        methods=['get'],
        operation_description="Liste des formations auxquelles le membre est inscrit",
        responses={200: FormationListSerializer},
    )
    @action(detail=True, methods=['get'])
    def formations(self, request, slug=None):
        membre = self.get_object()
        inscriptions = InscriptionFormation.objects.filter(membre=membre)
        formations = [inscription.formation for inscription in inscriptions]
        serializer = FormationListSerializer(formations, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        methods=['get'],
        operation_description="envoyer un code de confirmation du compte",
    )
    @action(detail=True, methods=['get'])
    def send_confirmation(self, request, slug=None):
        membre = self.get_object()
        confirmation = ConfirmationEmail.objects.filter(membre=membre).exists()
        if confirmation:
            ConfirmationEmail.objects.get(membre=membre).delete()
        confirmation = ConfirmationEmail(membre=membre)
        confirmation.save()
        send_confirmation_email(membre, confirmation.token)
        return Response({'detail': "email de confirmation envoyé"}, status=201)

    @action(detail=False, methods=['get'], permission_classes=[permissions.IsAuthenticated])
    def mon_profil(self, request):
        user = self.request.user
        membre = get_object_or_404(Membre, user=user)
        serializer = MembreListSerializer(membre)
        return Response(serializer.data, status=200)


class MemberAnniversaireView(generics.ListAPIView):
    serializer_class = MemberAnniversaireSerializer
    search_fields = ['date_naissance', 'date_naissance__year', 'date_naissance__month']
    filterset_fields = ['date_naissance']

    def get_queryset(self):
        # Récupérer le mois en cours
        current_month = date.today().month
        # Filtrer les membres dont le mois de naissance correspond au mois en cours
        queryset = Membre.objects.filter(date_naissance__month=current_month)
        return queryset


class ParcoursViewSet(viewsets.ModelViewSet):
    queryset = Parcours.objects.all()
    serializer_class = ParcoursSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'slug'
    filterset_fields = ['membre']


class DocumentPersonnelViewSet(viewsets.ModelViewSet):
    queryset = DocumentPersonnel.objects.all()
    serializer_class = DocumentPersonnelSerializer
    permission_classes = [permissions.IsAuthenticated]
    lookup_field = 'slug'

    def get_queryset(self):
        user = self.request.user
        membre = get_object_or_404(Membre, user=user)
        return DocumentPersonnel.objects.filter(membre=membre)


class PublicationAineViewSet(viewsets.ModelViewSet):
    queryset = PublicationAine.objects.all()
    serializer_class = PublicationAineSerializer
    lookup_field = 'slug'


class PromotionAPIView(MultipleSerializerMixin, viewsets.GenericViewSet, ListModelMixin, RetrieveModelMixin):
    queryset = Promotion.objects.all()
    serializer_class = PromotionListSerializer
    detail_serializer_class = PromotionDetailSerializer
    lookup_field = 'slug'


class MembreBureauViewSet(viewsets.ModelViewSet):
    queryset = MembreBureau.objects.all()
    serializer_class = MembreBureauSerializer
    lookup_field = 'slug'

    def create(self, request, *args, **kwargs):
        email = request.data.get('email')
        fonction = request.data.get('fonction')
        date_debut = request.data.get('date_debut')
        date_fin = request.data.get('date_fin')

        try:
            # user = User.objects.get(email=email)
            member: Membre = Membre.objects.get(email1=email)
            # Créer un membre du bureau avec les informations du membre correspondant
            membre_bureau_dict = {
                'nom': member.nom,
                'prenoms': member.prenoms,
                'email': email,
                'fonction': fonction,
                'photo': member.photo,
                'date_debut': date_debut,
                'date_fin': date_fin,
            }
            serializer = MembreBureauSerializer(data=membre_bureau_dict)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)

            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
        except User.DoesNotExist:
            return Response({'error': 'Aucun membre correspondant trouvé pour cet e-mail.'},
                            status=status.HTTP_400_BAD_REQUEST)


@swagger_auto_schema(
    method='get',
    operation_description='Activate user account',
    manual_parameters=[
        openapi.Parameter('token', openapi.IN_QUERY, description='Activation token', type=openapi.TYPE_STRING),
        openapi.Parameter('uid', openapi.IN_QUERY, description='User ID', type=openapi.TYPE_STRING),
    ],
    responses={
        200: 'Account activated successfully',
        400: 'Invalid activation token or user ID',
    }
)
@api_view(['GET'])
@permission_classes([AllowAny])
def activate_account(request):
    token = request.query_params.get('token', None)
    uid = request.query_params.get('uid', None)

    if not token or not uid:
        return Response({'detail': 'Veuillez fournie le token et uid d\'activation.'},
                 status=status.HTTP_400_BAD_REQUEST)
    membre = None
    try:
        uid = force_text(urlsafe_base64_decode(uid))
        membre = get_object_or_404(Membre, pk=uid)
        confirmation = get_object_or_404(ConfirmationEmail, token=token, membre=membre)
    except (TypeError, ValueError, OverflowError, Membre.DoesNotExist):
        return Response({'detail': 'Le lien de confirmation est invalide ou a expiré.'},
                        status=status.HTTP_400_BAD_REQUEST)
    else:
        if membre is not None and not membre.is_active and not membre.is_email_confirmed:
            try:
                verify_confirmation_token(token)
                membre.is_active = True
                membre.is_email_confirmed = True
                membre.user.is_active = True
                membre.user.save()
                membre.save()
                send_confirmation_success_email(membre)
            except(TokenExpiredException, TokenInvalidException) as error:

                return Response({'detail': 'Le lien de confirmation est invalide ou a expiré.', 'error': error},
                                status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response({'detail': 'il semble que votre compte est déjà activé.'}, status=status.HTTP_200_OK)
        return Response({'detail': 'Votre compte a été activé avec succès.'}, status=status.HTTP_200_OK)

@api_view(['GET'])
def check_email(request):
    email = request.query_params.get('email', None)

    if not email or not is_valid_email(email):
        return Response({'detail': 'Veuillez fournir votre e-mail valide.'}, status=status.HTTP_400_BAD_REQUEST)
    try:
        membre = Membre.objects.get(email1=email)
        return Response(status=status.HTTP_400_BAD_REQUEST, data={'details':'email utilisé'})
    except Membre.DoesNotExist:
        return Response(status=status.HTTP_200_OK, data={'details':'disponible'})


class EntrepriseViewSet(viewsets.ModelViewSet):
    serializer_class = EntrepriseSerializer
    queryset = Entreprise.objects.all()
    permissions = [permissions.AllowAny]
    lookup_field = 'slug'


class EntrepreneurViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = EntrepreneurSerializer
    permissions = [permissions.AllowAny]
    lookup_field = 'slug'

    def get_queryset(self):
        return Membre.objects.filter(entrepreneur=True)


class ProfessionnelViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = ProfilSerializer
    queryset = Entreprise.objects.all()
    permissions = [permissions.AllowAny]
    lookup_field = 'slug'

    def get_queryset(self):
        return Membre.objects.filter(status__iexact='ingenieurs')


class EtudiantViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
    serializer_class = ProfilSerializer
    permissions = [permissions.AllowAny]
    lookup_field = 'slug'

    def get_queryset(self):
        return Membre.objects.filter(status__iexact='eleve ingenieurs')
