from django.core.mail import send_mail
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes
from django.utils.html import strip_tags
from datetime import datetime, timedelta
from django.contrib.auth.tokens import default_token_generator

from backend import settings
from .exceptions import TokenExpiredException, TokenInvalidException
from django.utils.http import urlsafe_base64_encode

from .models import ConfirmationEmail, Membre


def send_confirmation_email(membre, token: str):
    confirmation_url = " {}/authentifications/confirmation?token={}&uid={}"
    import base64
    base64 = base64.urlsafe_b64encode(str(membre.pk).encode('utf-8')).decode('utf-8')
    uid = urlsafe_base64_encode(force_bytes(membre.pk))
    # activation_link = f'{settings.DOMAIN_LiNK}/api/activations/?token={token}&uid={uid}'
    activation_link = confirmation_url.format(settings.FRONT_URL, token, uid)
    subject = 'Confirmation de compte'
    html_message = render_to_string('confirmation_email.html', {
        'user': membre.get_nom_complet(),
        'genre': 'Monsieur ' if membre.sexe.startswith('M') else ' Madame/Mademoiselle ',
        'token': token,
        'activation_link': activation_link
    })
    plain_message = strip_tags(html_message)
    from_email = 'noreply@ae2ita.com'
    to_email = [membre.email1]
    try:

        state = send_mail(subject=subject, message=plain_message, from_email=from_email, recipient_list=to_email,
                          html_message=html_message)
        print('mail send state', state)
    except Exception as e:
        pass


def send_confirmation_success_email(membre):
    login_link = '/authentifications/connexions/'

    html_message = render_to_string('confirmation_succes_email.html', {
        'user': membre.get_nom_complet(),
        'genre': 'Monsieur ' if membre.sexe.startswith('M') else ' Madame/Mademoiselle ',
        'email': membre.email1,
        'login_link': login_link
    })
    plain_message = strip_tags(html_message)
    send_mail(
        subject="Compte AE2ITA activé avec succès",
        message=plain_message,
        from_email="noreply@ae2ita.com",
        recipient_list=[membre.email1],
        fail_silently=False,
        html_message=html_message
    )


def verify_confirmation_token(token):
    from django.utils import timezone
    try:
        # Recherche du courrier électronique de confirmation correspondant au token
        confirmation_email = ConfirmationEmail.objects.get(token=token)

        # Vérification de la date d'expiration du token
        expiration_date = confirmation_email.created_at + timedelta(days=2)
        now = timezone.now()
        if now > expiration_date:
            confirmation_email.token = ''
            confirmation_email.save()
            raise TokenExpiredException()

        # Vérification de la validité du token
        user = confirmation_email.membre.user
        if not default_token_generator.check_token(user, token):
            raise TokenInvalidException()

        # Le token et la date d'expiration sont valides
        return True
    except ConfirmationEmail.DoesNotExist:
        raise TokenInvalidException()


def is_valid_email(email):
    import re
    email_regex = r'^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,5}$'

    if re.match(email_regex, email):
        return True
    else:
        return False


def notification_publication_email(titre, date, lien, description):
    membres = Membre.objects.filter(is_email_confirmed=True)
    email_list = [membre.email1 for membre in membres]
    print("email list", email_list)
    subject = titre
    html_message = render_to_string('notification_publication.html', {
        'titre': titre,
        'date': date,
        'lien': lien,
        'description': description
    })
    plain_message = strip_tags(html_message)
    from_email = 'noreply@ae2ita.com'
    to_email = email_list

    try:
        send_mail(subject=subject, message=plain_message, from_email=from_email, recipient_list=to_email,
                  html_message=html_message)
    except Exception as e:
        pass
