import json

from django.test import TestCase

# Create your tests here.
from faker import Faker
from faker.providers import date_time, internet

# Initialiser le générateur de données Faker
fake = Faker()
fake.add_provider(date_time)
fake.add_provider(internet)

# Fonction pour générer un enregistrement de test
def generate_test_data():
    data = {
        "password": fake.password(),
        "nom": fake.last_name(),
        "prenoms": fake.first_name(),
        "date_naissance": fake.date_of_birth(minimum_age=18, maximum_age=80).strftime('%Y-%m-%d'),
        "lieu_naissance": fake.city(),
        "contact1": fake.phone_number(),
        "contact2": fake.phone_number(),
        "status": fake.random_element(elements=("Étudiant", "Enseignant", "Chercheur")),
        "email1": fake.email(),
        "email2": fake.email(),
        "domicile": fake.address(),
        "nationalite": fake.country(),
        "matricule": fake.random_number(digits=8),
        "niveau": fake.random_element(elements=("Licence", "Master", "Doctorat")),
        "specialite": fake.job(),
        "residence": fake.address(),
        "bibliographie": fake.text(max_nb_chars=200),
        "date_derniere_connexion": fake.date_time_this_month(before_now=True, after_now=False, tzinfo=None).isoformat(),
        "is_email_confirmed": fake.boolean(),
        "is_active": fake.boolean(),
        "sexe": fake.random_element(elements=("M", "F")),
        #"user": fake.random_number(digits=5),
    }
    return data

# Générer 10 enregistrements de test
test_data = [generate_test_data() for _ in range(5)]

# Afficher les données de test
for data in test_data:
    print('--------------')
    print(json.dumps(data))
    print('--------------')
