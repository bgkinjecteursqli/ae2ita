from django.urls import include, path
from rest_framework.routers import DefaultRouter, SimpleRouter
from .views import MembreViewSet, ParcoursViewSet, DocumentPersonnelViewSet, PublicationAineViewSet, \
    MemberAnniversaireView, PromotionAPIView, MembreBureauViewSet, activate_account, EntrepriseViewSet, \
    EntrepreneurViewSet, ProfessionnelViewSet, EtudiantViewSet, check_email

# Créez un routeur par défaut
router = DefaultRouter()
router_2 = SimpleRouter()
router_3 = SimpleRouter()
# Enregistrez les viewsets avec le routeur
router.register(r'profils', MembreViewSet, basename='membres-profils')
router.register(r'parcours', ParcoursViewSet, basename='membres-parcours')
router.register(r'documents', DocumentPersonnelViewSet, basename='membres-documents')
router.register(r'publications', PublicationAineViewSet, basename='membres-publications'),
router.register(r'promotions', PromotionAPIView, basename='membres-promotions'),

router_2.register(r'membres-bureau', MembreBureauViewSet, basename='membres-bureau')

router_3.register(r'entreprises', EntrepriseViewSet, basename='reseaux-entreprises')
router_3.register(r'entrepreneurs', EntrepreneurViewSet, basename='reseaux-entrepreneurs')
router_3.register(r'professionnels', ProfessionnelViewSet, basename='reseaux-professionnels')
router_3.register(r'etudiants', EtudiantViewSet, basename='reseaux-etudiant')

router = [
    # Autres routes de votre application
    path('membres/', include(router.urls)),
    path('reseaux/', include(router_3.urls)),
    path('presentations/', include(router_2.urls)),
    path('membres/anniversaires/', MemberAnniversaireView.as_view(), name='membre-anniversaire'),
    path('activations/', activate_account),
    path('email-verification/', check_email),
    path('promotions/', PromotionAPIView, name='member-promotions'),
]
