from datetime import datetime, timedelta
import os

from autoslug import AutoSlugField
from django.contrib.auth.base_user import BaseUserManager
from django.contrib.auth.models import User, AbstractUser
from django.contrib.auth.tokens import default_token_generator
from django.db import models, transaction
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from utils.FileUtil import validate_image_extension
from utils.model_to_js import convert_models_to_js


class AccountManager(BaseUserManager):
    def create_user(self, email, password=None, **kwargs):
        if not email:
            raise ValueError('Users must have a valid email address.')

        account = self.model(email=self.normalize_email(email))
        account.set_password(password)
        account.save()
        return account

    def create_superuser(self, email, password, **kwargs):
        account = self.create_user(email, password, **kwargs)
        account.is_admin = True
        account.save()


def photo_rename(instance, filename):
    from string import ascii_letters
    import random
    ext = os.path.splitext(filename)[-1]
    str_name = ''.join([random.choice(ascii_letters) for x in range(8)])
    return f'membres/photo_{str_name}{ext}'


class Membre(models.Model):
    class Meta:
        ordering = ['-date_creation']

    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True)
    nom = models.CharField(max_length=100)
    prenoms = models.CharField(max_length=100)
    photo = models.FileField(upload_to=photo_rename, blank=True, null=True, default='avatar.jpg', validators=[validate_image_extension])
    date_naissance = models.DateField(blank=True, null=True)
    lieu_naissance = models.CharField(max_length=100, blank=True, null=True)
    contact1 = models.CharField(max_length=20)
    contact2 = models.CharField(max_length=20, blank=True, null=True)
    contact3 = models.CharField(max_length=20, blank=True, null=True)
    email1 = models.EmailField()
    email2 = models.EmailField(blank=True, null=True)
    profil = models.CharField(max_length=55, blank=True, null=True, default='', help_text="ingénieur, élève ingénieurs")
    status_emplois = models.CharField(max_length=100, blank=True, default='',
                                     help_text='Salarié, entrepreneur et sans emploi')
    fonction_emplois = models.CharField(max_length=255, blank=True, default='', help_text='poste occupé')
    structure_emplois = models.CharField(max_length=255, blank=True, default='', help_text='structure de travail')
    region_activites = models.CharField(max_length=15, blank=True, default='', help_text='localité de travail')
    niveau = models.CharField(max_length=100, blank=True, default='')
    specialite = models.CharField(max_length=100, blank=True, default='')
    bibliographie = models.TextField(blank=True, default='')
    slug = AutoSlugField(populate_from='get_slug', unique_with=['date_naissance__year'])
    is_email_confirmed = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    sexe = models.CharField(max_length=10, default='M', blank=True)
    password = models.CharField(max_length=15, blank=True, null=True)
    entrepreneur = models.BooleanField(default=False, blank=True)
    entreprise = models.CharField(max_length=255, blank=True, null=True, default='')
    pays_residence = models.CharField(max_length=225, blank=True, null=True, default='')
    ville_residence = models.CharField(max_length=255, blank=True, null=True, default='')
    promotion = models.PositiveIntegerField(null=True)
    ecole_origine = models.CharField(max_length=255, blank=True, null=True, default='')
    date_derniere_connexion = models.DateTimeField(blank=True, null=True)
    date_modification = models.DateTimeField(auto_now=True)
    date_creation = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.nom + " " + self.prenoms

    def get_slug(self):
        return f"{self.nom} {self.prenoms} {self.promotion}"

    def get_nom_complet(self):
        return self.nom + ' ' + self.prenoms

    def save(self, *args, **kwargs):
        self.password = None
        super(Membre, self).save(*args, **kwargs)


list_annees = [
    ('1A', '1ère année'),
    ('2A', '2ème année'),
    ('3A', '3ème année'),
]


class Parcours(models.Model):
    class Meta:
        ordering = ['-date_creation']

    membre = models.ForeignKey(Membre, on_delete=models.CASCADE, related_name='parcours')
    intitule = models.CharField(max_length=100)
    niveau = models.CharField(max_length=100, choices=list_annees)
    specialite = models.CharField(max_length=100)
    annee_scolaire = models.CharField(max_length=100)
    ecole = models.CharField(max_length=100, default='ESA')
    date_creation = models.DateTimeField(auto_now_add=True)
    slug = AutoSlugField(populate_from=['get_slug'])

    def __str__(self):
        return self.intitule

    def get_slug(self):
        return f'{self.membre.nom} {self.intitule} {self.annee_scolaire}'


def document_filename(instance, filename):
    # Obtenir le nom du membre
    membre_nom = instance.membre.nom
    # Obtenir le nom du document
    document_nom = instance.nom
    # Obtenir l'extension du fichier
    extension = os.path.splitext(filename)[-1]
    # Construire le nouveau nom de fichier
    nouveau_nom = f"{membre_nom}_{document_nom}{extension}"
    # Retourner le nouveau nom de fichier
    return os.path.join('documents', nouveau_nom)


class DocumentPersonnel(models.Model):
    class Meta:
        ordering = ['-date_creation']

    membre = models.ForeignKey(Membre, on_delete=models.CASCADE, related_name='documents')
    extension = models.CharField(max_length=25)
    titre = models.CharField(max_length=100)
    fichier = models.FileField(upload_to=document_filename)
    date_creation = models.DateTimeField(auto_now_add=True)
    slug = AutoSlugField(populate_from=[''])

    def __str__(self):
        return self.titre

    def get_file_ext(self):
        return os.path.splitext(self.fichier.name)[1]

    @transaction.atomic
    def save(self, *args, **kwargs):
        self.extension = self.get_file_ext()
        super(DocumentPersonnel, self).save(*args, **kwargs)


class PublicationAine(models.Model):
    membre = models.ForeignKey(Membre, on_delete=models.CASCADE)
    titre = models.CharField(max_length=100)
    contenu = models.TextField()
    date_publication = models.DateTimeField(auto_now_add=True)
    slug = AutoSlugField(unique_with=['date_publication__month'], populate_from='get_slug_source')

    class Meta:
        verbose_name_plural = "Publications des aînés"
        ordering = ['-date_publication']

    def __str__(self):
        return self.titre

    def get_slug_source(self):
        return f"{self.titre} {self.membre.nom} {self.date_publication.year}"


class Promotion(models.Model):
    promotion = models.PositiveIntegerField()
    intitule = models.CharField(max_length=100, blank=True, default=None)
    annee = models.PositiveIntegerField()
    date_creation = models.DateTimeField(auto_now_add=True)
    slug = AutoSlugField(unique=True, populate_from='get_slug_source')

    def get_slug_source(self):
        return f"{self.intitule} {self.annee}"

    def __str__(self):
        return self.promotion

    class Meta:
        ordering = ['-date_creation']

    def __str__(self):
        return self.intitule

    def save(self, *args, **kwargs):
        if not self.intitule:
            self.intitule = f'Promotion {self.promotion}'

        super(Promotion, self).save(*args, **kwargs)


class MembreBureau(models.Model):
    nom = models.CharField(max_length=100)
    prenoms = models.CharField(max_length=100)
    fonction = models.CharField(max_length=100)
    section = models.CharField(max_length=100)
    email = models.EmailField()
    photo = models.ImageField(upload_to='membres_bureau/')
    date_debut = models.DateField()
    date_fin = models.DateField()
    date_creation = models.DateTimeField(auto_now_add=True)
    slug = AutoSlugField(populate_from=['nom', 'prenoms'], unique_with=['fonction'])
    date_modification = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.nom} {self.prenoms} - {self.fonction}"


class ConfirmationEmail(models.Model):
    membre = models.OneToOneField(Membre, on_delete=models.CASCADE)
    token = models.CharField(max_length=100, default='')
    created_at = models.DateTimeField(auto_now_add=True)
    expiration_date = models.DateTimeField()
    def __str__(self):
        return self.membre.nom + ' - ' + str(self.expiration_date)
    def set_expire(self):
        current_date = datetime.now().date()
        # Add two days to the current date
        result_date = current_date + timedelta(days=2)
        self.expiration_date = result_date

    def save(self, *args, **kwargs):
        self.token = default_token_generator.make_token(self.membre.user)
        self.set_expire()
        super(ConfirmationEmail, self).save(*args, **kwargs)


def document_filename(instance, filename):
    from utils.StringUtil import StringUtil
    entrep= StringUtil.clean_title(instance.nom.lower())
    extension = os.path.splitext(filename)[-1]
    nouveau_nom = f"{entrep}{extension}"

    return os.path.join('entreprise', nouveau_nom)


class Entreprise(models.Model):
    slug = AutoSlugField(populate_from='nom')
    membre = models.ForeignKey(Membre, on_delete=models.CASCADE, related_name='entreprise_membres', blank=True, null=True)
    nom = models.CharField(max_length=125, unique=True, blank=False)
    auteur = models.CharField(max_length=100, blank=False )
    logo = models.ImageField(upload_to=document_filename, validators=[validate_image_extension])
    descriptions = models.TextField(blank=True)
    lieu = models.CharField(max_length=25, blank=True, default="")
    domaine = models.CharField(max_length=255, blank=True, default='')
    contact = models.CharField(max_length=125, blank=True, default='', null=True)
    email = models.EmailField(blank=True, null=True)
    annne_experience = models.PositiveIntegerField(blank=True, null=True)

    def __str__(self):
        return f"{self.nom} de {self.auteur}"

# ------- signal------------

# @receiver(post_save, sender=Membre)
def create_membre_signal(sender, instance: Membre, created, **kwargs):
    from members.utils import send_confirmation_email

    if created:
        confirmation = ConfirmationEmail(membre=instance)

        confirmation.save()
        send_confirmation_email(instance, confirmation.token)


# conversion model
model_list = [Membre, User, MembreBureau, Promotion, DocumentPersonnel, PublicationAine, Parcours, Entreprise]

convert_models_to_js(model_list)
