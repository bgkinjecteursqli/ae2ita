from rest_framework import serializers, status
from rest_framework.exceptions import APIException


class TokenExpiredException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Le token de confirmation a expiré."
    default_code = 'token_expired'


class TokenInvalidException(APIException):
    status_code = status.HTTP_400_BAD_REQUEST
    default_detail = "Le token de confirmation est invalide."
    default_code = 'token_invalid'


class TokenInvalidExceptionSerializer(serializers.Serializer):
    detail = serializers.CharField(default=TokenInvalidException.default_detail)
    status= serializers.IntegerField(default=TokenInvalidException.status_code)
    code= serializers.CharField(default=TokenInvalidException.default_code)