from django.contrib import admin
from .models import Membre, MembreBureau, DocumentPersonnel, Parcours, PublicationAine, Promotion, ConfirmationEmail, \
    Entreprise

admin.site.register(MembreBureau)
admin.site.register(DocumentPersonnel)
admin.site.register(Parcours)
admin.site.register(PublicationAine)
admin.site.register(Promotion)
admin.site.register(ConfirmationEmail)

@admin.register(Membre)
class MmebreAdmin(admin.ModelAdmin):
    list_display = ["id","nom","prenoms","profil", "sexe","is_active", "is_email_confirmed"]




@admin.register(Entreprise)
class EntrepriseAdmin(admin.ModelAdmin):
    list_display = ['nom', 'auteur', 'lieu']
    list_filter = ['nom']
    search_fields = ['nom', 'auteur']
    ordering = ['nom']

