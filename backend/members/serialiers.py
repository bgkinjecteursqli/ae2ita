from datetime import date

from rest_framework import serializers
from django.contrib.auth import get_user_model

from backend import settings
from .models import Membre, Parcours, DocumentPersonnel, PublicationAine, Promotion, MembreBureau, ConfirmationEmail, \
    Entreprise

User = get_user_model()


class ParcoursSerializer(serializers.ModelSerializer):
    class Meta:
        model = Parcours
        fields = '__all__'


class DocumentPersonnelSerializer(serializers.ModelSerializer):
    class Meta:
        model = DocumentPersonnel
        fields = '__all__'


class PublicationAineSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublicationAine
        fields = '__all__'


class MembreListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Membre
        fields = '__all__'

    def create(self, validated_data):
        from members.utils import send_confirmation_email
        membre = Membre.objects.create(**validated_data)

        confirmation = ConfirmationEmail(membre=membre)
        confirmation.save()

        send_confirmation_email(membre, confirmation.token)
        return membre


class MembreDetailSerializer(serializers.ModelSerializer):
    parcours = serializers.SerializerMethodField()
    documents = serializers.SerializerMethodField()
    publications = serializers.SerializerMethodField()

    class Meta:
        model = Membre
        fields = '__all__'

    def get_parcours(self, instance):
        return []

    def get_documents(self, instance):
        return []

    def get_publications(self, instance):
        return []


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

    def create(self, validated_data):
        password = validated_data.pop('password', None)
        user: User = User(**validated_data)
        if password:
            user.set_password(password)
        user.save()
        return user


class MemberAnniversaireSerializer(serializers.ModelSerializer):
    age = serializers.SerializerMethodField()
    contact = serializers.CharField(source='contact1', read_only=True)
    email = serializers.CharField(source='email1', read_only=True)
    promotion = serializers.CharField(read_only=True)

    class Meta:
        model = Membre
        fields = ['nom', 'prenoms', 'photo', 'date_naissance', 'age', 'sexe', 'email', 'contact', 'promotion']

    def get_age(self, obj):
        # Calculer l'âge à partir de la date de naissance
        if obj.date_naissance:
            today = date.today()
            age = today.year - obj.date_naissance.year - (
                    (today.month, today.day) < (obj.date_naissance.month, obj.date_naissance.day))
            return age
        return None


class PromotionListSerializer(serializers.ModelSerializer):
    effectif = serializers.SerializerMethodField()

    class Meta:
        model = Promotion
        fields = '__all__'

    def get_effectif(self, instance):
        return Membre.objects.filter(promotion=instance).count()


class PromotionDetailSerializer(serializers.ModelSerializer):
    membres = serializers.SerializerMethodField()

    class Meta:
        model = Promotion
        fields = '__all__'

    def get_membres(self, instance: Promotion):
        membres = Membre.objects.filter(promotion=instance.label)
        serializer = MembreListSerializer(membres, many=True)
        return serializer.data


class MembreBureauSerializer(serializers.ModelSerializer):
    class Meta:
        model = MembreBureau
        fields = '__all__'


class EntrepriseSerializer(serializers.ModelSerializer):
    # membre = serializers.CharField(source='membre.get_nom_complet', read_only=True)
    # photo = serializers.CharField(source='membre.photo', read_only=True)

    class Meta:
        model = Entreprise
        fields = '__all__'

    def to_representation(self, instance:Entreprise):
        data = super(EntrepriseSerializer, self).to_representation(instance)
        if settings.CDN_LiNK:
            logo_url = settings.CDN_LiNK + instance.logo.url
            data['logo'] = logo_url
        return data


class ProfilSerializer(serializers.ModelSerializer):
    class Meta:
        model = Membre
        fields = ['id', 'slug', 'nom', 'prenoms', 'photo', 'contact1', 'email1', 'bibliographie']


class EntrepreneurSerializer(serializers.ModelSerializer):
    entreprises = serializers.SerializerMethodField()

    class Meta:
        model = Membre
        fields = ['id', 'slug', 'nom', 'prenoms', 'photo', 'contact1', 'email1', 'bibliographie', 'entrepreneur',
                  'entreprises']

    def get_entreprises(self, instance: Membre):
        entr = Entreprise.objects.filter(membre=instance)
        serializers = EntrepriseSerializer(entr, many=True)
        return serializers.data


class ActivationSerializer(serializers.Serializer):
    token = serializers.CharField(max_length=125)
    uid = serializers.CharField(max_length=125)
