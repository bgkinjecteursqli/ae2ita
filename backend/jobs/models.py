from autoslug import AutoSlugField
from django.db import models

# Create your models here.
from django.db import models
from django.db.transaction import atomic

from members.models import Membre
from utils.model_to_js import convert_models_to_js


class OffreEmploi(models.Model):
    TYPE_CHOICES = [
        ('CDI', 'CDI'),
        ('CDD', 'CDD'),
    ]

    EMPLOIS_CHOICES = [
        ('stage', 'stage'),
        ('embauche', 'embauche'),
    ]

    titre = models.CharField(max_length=100)
    description = models.TextField(blank=True, default='')
    type_offre = models.CharField(max_length=20, default="CDI", choices=TYPE_CHOICES, blank=True, help_text="CDI, CDD")
    type_emplois = models.CharField(max_length=20, default=EMPLOIS_CHOICES[0], choices=EMPLOIS_CHOICES, help_text='stage, embauche')
    lieu_travail = models.CharField(max_length=100)
    nombre_poste = models.PositiveIntegerField(default=0, blank=True)
    experiences_professionnelles = models.CharField(max_length=125, blank=True, null=True)
    niveau = models.CharField(max_length=150)
    date_cloture = models.DateField()
    date_creation = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(upload_to='offres_emploi/images/', blank=True, null=True)
    email_job = models.EmailField()
    entreprise = models.CharField(max_length=100, blank=True, null=True)
    duree = models.CharField(max_length=20, blank=True, null=True, default="indeterminée")
    slug = AutoSlugField(populate_from='get_slug', unique=True)

    class Meta:
        ordering = ('-date_creation',)

    def get_slug(self):
        return f'{self.titre} {self.date_cloture.month} {self.date_cloture.year}'

    def __str__(self):
        return self.titre


class Candidature(models.Model):
    cv = models.FileField(upload_to='candidature')
    lm = models.FileField(upload_to='candidature', )
    titre = models.CharField(max_length=155, blank=True, null=True)
    email = models.EmailField(blank=True, null=True)
    corps = models.TextField(blank=True, default='')
    membre = models.ForeignKey(Membre, on_delete=models.CASCADE, blank=True)
    entreprise = models.CharField(max_length=125, blank=True, default="")
    date_creation = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.titre

    @atomic
    def save(self, *args, **kwargs):
        super(Candidature, self).save(*args, **kwargs)


model_list = [OffreEmploi, Candidature]
convert_models_to_js(model_list)
