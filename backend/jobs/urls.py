from django.urls import path, include
from rest_framework.routers import SimpleRouter

from jobs.views import OffreEmploiViewSet, PostulerOffreView

router = SimpleRouter()
router.register('emplois', OffreEmploiViewSet, basename='emplois')

router = [
    # Autres routes de votre application
    path('', include(router.urls)),
    path('emplois/<str:offre_slug>/candidature/', PostulerOffreView.as_view())
]