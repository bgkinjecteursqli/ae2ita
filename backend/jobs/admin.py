from django.contrib import admin

from .models import OffreEmploi, Candidature

admin.site.register(OffreEmploi)
admin.site.register(Candidature)