from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, status, permissions, mixins, generics

from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.views import APIView

from members.models import Membre
from .models import OffreEmploi, Candidature
from .serializers import OffreEmploiSerializer, PostulerSerializer, CandidatureSerializer
from .utils import envoyer_email_candidature


class OffreEmploiViewSet(viewsets.ModelViewSet):
    queryset = OffreEmploi.objects.all()
    serializer_class = OffreEmploiSerializer
    lookup_field = 'slug'
    permission_classes = [permissions.AllowAny]

    @swagger_auto_schema(
        operation_description="Postuler a l'offre",
        request_body=CandidatureSerializer
    )
    @action(detail=True, methods=['post'], permission_classes=[permissions.IsAuthenticated])
    def postuler(self, request, slug=None):
        user = request.user
        membre = get_object_or_404(Membre, user=user)
        offre = self.get_object()
        try:
            titre = request.data.get('titre', None)
            corps = request.data.get('corps', None)
            email = request.data.get('email', None)
            cv_file = request.FILES.get('cv', None)
            lm_file = request.FILES.get('lm', None)
            candiat = Candidature(titre=titre, email=email,
                                  corps=corps, membre=membre,
                                  entreprise=offre.entreprise,
                                  cv=cv_file, lm=lm_file)
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)
        try:
            candiat.save()
        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)

        try:
            envoyer_email_candidature(offre=offre, membre=membre, corps=corps, titre=titre, email=email,
                                      cv_file=cv_file,
                                      lettre_motivation_file=lm_file, candidature=candiat)
            return Response({'message': 'Candidature soumise avec succès'}, status=status.HTTP_201_CREATED)

        except Exception as e:
            return Response({'detail': str(e)}, status=status.HTTP_400_BAD_REQUEST)


class PostulerOffreView(APIView):
    permission_classes = [permissions.IsAuthenticated]

    @swagger_auto_schema(

        operation_description="Postuler a une nouvelle formations",
        request_body=PostulerSerializer
    )
    def post(self, request, offre_slug):
        offre = get_object_or_404(OffreEmploi, slug=offre_slug)
        serializer = PostulerSerializer(data=request.data)
        if serializer.is_valid():
            user = self.request.user
            membre = get_object_or_404(Membre, user=user)
            # membre = get_object_or_404(Membre, email1=serializer.validated_data.get('membre_email'))
            cv_file = membre.documents.filter(type='cv').last()
            add_lm = serializer.validated_data.get('add_lm')
            lm_file = None
            if add_lm:
                lm_file = membre.documents.filter(type='lm').last()
            envoyer_email_candidature(offre, membre, cv_file, lm_file)
            return Response({'success': True, 'message': 'Postulation réussie'}, status=status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
