from django.core.mail import EmailMessage, send_mail
from django.template.loader import render_to_string
from django.utils.html import strip_tags

from members.models import Membre
from .models import OffreEmploi, Candidature


def envoyer_email_candidature(offre: OffreEmploi, membre: Membre, corps: str, titre: str, email: str, cv_file,
                              lettre_motivation_file=None, send_copy: bool = True, candidature:Candidature=None):
    # Créer l'objet EmailMessage
    html_message: object = render_to_string('candidature_email.html', {
        'titre': titre,
        'email': email,
        "corps": corps
    })
    sujet = titre
    plain_message = strip_tags(html_message)
    to_email = [offre.email_job]  # L'adresse e-mail de l'entreprise
    attachments = [(candidature.cv.name, candidature.cv.read(), cv_file.content_type)]

    if lettre_motivation_file:
        attachments.append((candidature.lm.name,
                            candidature.lm.read(), lettre_motivation_file.content_type))

    email = EmailMessage(subject=sujet, body=corps,
                         from_email=email, to=to_email, reply_to=(email,), attachments=attachments)

    # Envoyer l'e-mail
    state = email.send()
    if send_copy:
        send_email_copy(membre, offre, cv_file, lettre_motivation_file, candidature=candidature)


def send_email_copy(membre, offre, cv, lettre_motivation=None, candidature:Candidature=None):
    # Créer le contenu de l'e-mail
    subject = "Confirmation de candidature à l'offre d'emploi"
    body = f"Bonjour {membre.nom},\n\nNous avons bien envoyé votre candidature pour l'offre d'emploi: '{offre.titre}'."

    # Créer l'objet EmailMessage avec les pièces jointes
    email = EmailMessage(subject, body, to=[membre.email1])
    email.attach(candidature.cv.name, candidature.cv.read(), cv.content_type)
    if lettre_motivation:
        email.attach(candidature.lm.name, candidature.lm.read(), lettre_motivation.content_type)

    # Envoyer l'e-mail
    state = email.send()
