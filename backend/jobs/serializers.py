from rest_framework import serializers
from .models import OffreEmploi, Candidature


class OffreEmploiSerializer(serializers.ModelSerializer):
    class Meta:
        model = OffreEmploi
        fields = '__all__'


class PostulerSerializer(serializers.Serializer):
    # membre_email = serializers.EmailField()
    add_cv = serializers.BooleanField(default=True)
    add_lm = serializers.BooleanField(default=False)


class CandidatureSerializer(serializers.Serializer):
    class Meta:
        model = Candidature
        fields = '__all__'



