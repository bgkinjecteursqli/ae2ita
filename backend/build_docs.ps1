# Chemin vers le répertoire source de la doc
$SourceDir = "documentation/source/"

# Chemin vers le répertoire de build
$BuildDir = "documentation/build"

# Vérifie si sphinx-build est installé
if ((Get-Command "sphinx-build" -ErrorAction SilentlyContinue) -eq $null) {
    Write-Host "sphinx-build n'est pas installé. Veuillez l'installer avant d'exécuter ce script."
    exit
}

# Supprime le contenu du répertoire de build
Remove-Item $BuildDir/* -Recurse -Force

# Génère la doc HTML
& sphinx-build -b html $SourceDir $BuildDir

if ($LASTEXITCODE -ne 0) {
    Write-Host "Erreur lors de la génération de la documentation Sphinx"
    exit $LASTEXITCODE
}

Write-Host "Documentation construire avec succes dans $BuildDir"
