#!/bin/bash

# chmod +x build_docs.sh
# Chemin vers le répertoire source de la documentation
SOURCEDIR=documentation/source/

# Chemin vers le répertoire de build de la documentation
BUILDDIR=documentation/build

# Lancement de la commande sphinx-build
sphinx-build -b html "$SOURCEDIR" "$BUILDDIR"
