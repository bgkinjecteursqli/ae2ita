#!/bin/bash


source /opt/env/ae2ita_env/bin/activate
python -m pip install --upgrade pip
ls
echo "install depencencies....."
pip install -r requirements.txt
echo "migrations......."
python manage.py makemigrations
echo "migrate......."
python manage.py migrate
python manage.py collectstatic
echo "restarting backend "
supervisorctl restart ae2ita_api