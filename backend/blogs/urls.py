from django.urls import include, path
from rest_framework import routers
from .views import ArticleViewSet, TemoignageViewSet, PublicationBureauViewSet, EventViewSet, ExpertViewSet

router = routers.DefaultRouter()
router.register(r'articles', ArticleViewSet)
router.register(r'temoignages', TemoignageViewSet)
router.register(r'evenements', EventViewSet)
router.register(r'publications', PublicationBureauViewSet)
router.register('experts', ExpertViewSet)

router = [
    path('blogs/', include(router.urls)),
]
