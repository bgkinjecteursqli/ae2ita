from rest_framework import serializers

from blogs.models import Article, BlogImage, Temoignage, PublicationBureau, Evenement, Tag, Expert, ExpertComment
from members.models import Membre
from utils.link_parser import link_parser


class BlogImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = BlogImage
        fields = ['image']

    def to_representation(self, instance):
        instance = super(BlogImageSerializer, self).to_representation(instance)
        return instance.get('image')


class AuteurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Membre
        fields = ['slug', 'photo', 'nom']

    def to_representation(self, instance):
        instance = super(AuteurSerializer, self).to_representation(instance)
        photo = instance.get('photo' or 'avatar.jpg')
        return instance


class ArticleSerializer(serializers.ModelSerializer):
    images = BlogImageSerializer(many=True, required=False)

    class Meta:
        model = Article
        fields = '__all__'

    def get_avatar_url(self, instance: Article):
        return instance.auteur.photo.url

    def to_representation(self, instance: Article):
        data = super(ArticleSerializer, self).to_representation(instance)

        # Modifie le champ "auteur"
        auteur = instance.auteur
        data['auteur'] = {
            'id': auteur.id,
            'slug': auteur.slug,
            'nom': auteur.get_nom_complet(),
            'avatar': link_parser(auteur.photo.url) if auteur.photo else 'avatar.jpg'
        }

        return data

    def create(self, validated_data):
        article = Article.objects.create(**validated_data)
        return article


class TemoignageSerializer(serializers.ModelSerializer):
    photo = serializers.ImageField(source='membre.photo', read_only=True)
    nom = serializers.CharField(source='membre.get_nom_complet', read_only=True)
    promotion = serializers.CharField(source='membre.promotion.intitule', read_only=True)

    class Meta:
        model = Temoignage
        fields = '__all__'


class ExpertSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expert
        fields = "__all__"


class ExpertCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = ExpertComment
        fields = "__all__"


class PublicationBureauSerializer(serializers.ModelSerializer):
    class Meta:
        model = PublicationBureau
        fields = '__all__'


class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = '__all__'


class EventSerializer(serializers.ModelSerializer):
    tags = TagSerializer(many=True, required=False)

    class Meta:
        model = Evenement
        fields = '__all__'
