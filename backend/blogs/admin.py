from django.contrib import admin

from formations.admin import BaseAdmin
from .models import Article, Temoignage, BlogImage, PublicationBureau, Tag, Evenement, Expert

admin.site.register(Article, BaseAdmin)
admin.site.register(Temoignage, BaseAdmin)
admin.site.register(BlogImage)
admin.site.register(PublicationBureau, BaseAdmin)


@admin.register(Evenement)
class EvenementAdmin(admin.ModelAdmin):
    pass


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    pass


@admin.register(Expert)
class ExpertAdmin(BaseAdmin):
    list_display = ('id', 'theme', 'nom')
