from django.shortcuts import get_object_or_404
from rest_framework import viewsets, permissions, status
from rest_framework.decorators import action
from rest_framework.mixins import ListModelMixin, RetrieveModelMixin
from rest_framework.permissions import AllowAny
from rest_framework.response import Response

from blogs.models import Article, Temoignage, PublicationBureau, BlogImage, Evenement, Tag, Expert, ExpertComment
from blogs.serializers import ArticleSerializer, TemoignageSerializer, PublicationBureauSerializer, EventSerializer, \
    TagSerializer, ExpertSerializer, ExpertCommentSerializer
from members.models import Membre
from members.permissions import IsAdminOrOwner


class ArticleViewSet(viewsets.ModelViewSet):
    queryset = Article.objects.all()
    serializer_class = ArticleSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    search_fields = ['titre']
    lookup_field = 'slug'

    def create(self, request, *args, **kwargs):
        user = self.request.user
        membre = get_object_or_404(Membre, user=user)
        post_data = request.data
        post_data['auteur'] = membre.pk
        serializer = ArticleSerializer(data=post_data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(data=serializer.data)
        return Response(data=serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def perform_create(self, serializer: ArticleSerializer):
        instance = serializer.save()
        images = serializer.context.get("request").FILES.getlist('images')
        for image_data in images:
            BlogImage.objects.create(article=instance, image=image_data)
        return instance


class TemoignageViewSet(viewsets.ModelViewSet):
    queryset = Temoignage.objects.all()
    serializer_class = TemoignageSerializer
    permission_classes = [AllowAny]

    def get_permissions_classes(self):
        if self.action == 'create' or self.action == 'destroy':
            # Autoriser uniquement les membres authentifiés à créer leur témoignage
            return [permissions.IsAuthenticated]
        else:
            # Autoriser tous les utilisateurs à consulter, mettre à jour et partiellement mettre à jour les témoignages
            return [permissions.AllowAny]

    def check_permissions(self, request):
        self.permission_classes = self.get_permissions_classes()
        super(TemoignageViewSet, self).check_permissions(request)


class PublicationBureauViewSet(viewsets.ModelViewSet):
    queryset = PublicationBureau.objects.all()
    serializer_class = PublicationBureauSerializer
    lookup_field = 'slug'
    permission_classes = [AllowAny]
    filterset_fields = ['titre', "commisson", "ordre"]


class EventViewSet(viewsets.GenericViewSet, ListModelMixin, RetrieveModelMixin):
    queryset = Evenement.objects.all()
    serializer_class = EventSerializer
    filterset_fields = ['titre', 'tags']
    lookup_field = 'slug'


class ExpertViewSet(viewsets.ModelViewSet):
    serializer_class = ExpertSerializer
    permission_classes = [permissions.IsAuthenticatedOrReadOnly]
    queryset = Expert.objects.filter(active=True)
    filterset_fields = ['theme']
    lookup_field = 'uuid'

    @action(detail=True, methods=['GET'], permission_classes=[permissions.AllowAny])
    def comments(self, request, uuid=None):
        expert = self.get_object()
        comments = ExpertComment.objects.filter(expert=expert)
        serializer = ExpertCommentSerializer(comments, many=True)
        return Response(serializer.data)

    @action(detail=True, methods=['POST'], permission_classes=[permissions.AllowAny])
    def create_comment(self, request, uuid=None):
        expert = self.get_object()
        member = None
        try:
            member = Membre.objects.filter(user=request.user)
        except Membre.DoesNotExist:
            return Response(data={'detail': 'profil membre non trouvé'}, status=404)

        serializer = ExpertCommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(expert=expert, member=member)
            return Response(serializer.data, status=201)
        return Response(serializer.errors, status=400)


class TagViewSet(viewsets.GenericViewSet, ListModelMixin):
    queryset = Tag.objects.all()
    serializer_class = TagSerializer
