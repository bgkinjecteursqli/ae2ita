import os
import uuid
from dateutil.utils import today
from django.db.models.signals import post_save

from django.db.transaction import atomic
from django.utils.text import slugify
from django.utils import timezone
from autoslug import AutoSlugField
from django.db import models

from backend.settings import FRONT_URL
from members.models import Membre
from members.tasks import notification_publication_email_task
from members.utils import notification_publication_email
from utils.FileUtil import validate_extension, validate_image_extension
from utils.model_to_js import convert_models_to_js

type_article = ['article', 'evenement']


def article_upload(instance, filename):
    date = today().year
    name_tpl = '{}{}'
    new_name = slugify([instance.titre, date])
    ext = os.path.splitext(filename)[-1]
    return f'covers/{name_tpl.format(new_name, ext)}'


class AbstractModel(models.Model):
    date_creation = models.DateTimeField(auto_now_add=True)
    date_modification = models.DateTimeField(auto_now=True)

    class Meta:
        abstract = True


class Article(models.Model):
    titre = models.CharField(max_length=110)
    description = models.TextField()
    cover = models.ImageField(upload_to=article_upload, null=True, validators=[validate_image_extension])
    slug = AutoSlugField(populate_from='titre', unique_with=['date_creation__year'], )
    auteur = models.ForeignKey(Membre, on_delete=models.CASCADE)
    type = models.CharField(max_length=35, blank=True, default='article')
    tags = models.CharField(max_length=100, blank=True, null=True)
    date_creation = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('-date_creation',)

    @atomic
    def save(self, *args, **kwargs):
        super(Article, self).save(*args, **kwargs)

    def __str__(self):
        return self.titre


class BlogImage(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE, related_name='images', blank=True, null=True)
    image = models.ImageField(upload_to='article_images/', blank=True, null=True)

    def __str__(self):
        return f'image article {self.article.titre} {self.pk}'


class Temoignage(models.Model):
    membre = models.ForeignKey(Membre, on_delete=models.CASCADE)
    contenu = models.TextField()
    date_creation = models.DateTimeField(default=timezone.now)
    active = models.BooleanField(default=True)

    def __str__(self):
        return f"Témoignage de {self.membre.nom} {self.membre.prenoms}"

    class Meta:
        ordering = ('-date_creation',)


class Expert(AbstractModel):
    # membre = models.ForeignKey(Membre, on_delete=models.CASCADE, blank=True, null=True)
    theme = models.CharField(max_length=255)
    nom = models.CharField(max_length=255)
    contenu = models.TextField()
    photo = models.ImageField(upload_to='experts', blank=True, null=True)
    active = models.BooleanField(default=True)
    promotion = models.CharField(max_length=15, default="47")
    uuid = models.UUIDField(blank=True, default=uuid.uuid4, editable=False)

    def __str__(self):
        return f"Expert  {self.theme} - {self.nom}"

    class Meta:
        ordering = ('-date_creation',)

    def save(self, *args, **kwargs):
        if not self.uuid:
            self.uuid = uuid.uuid4()
        return super(Expert, self).save(*args, kwargs)


class ExpertComment(models.Model):
    expert = models.ForeignKey(Expert, on_delete=models.CASCADE, related_name='comments')
    membre = models.ForeignKey(Membre, on_delete=models.CASCADE)
    content = models.TextField()
    date_created = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return f"Comment by {self.user.username} on {self.expert.theme}"


def publication_upload(instance, filename):
    name_tpl = '{}{}'
    new_name = slugify([instance.titre, instance.ordre])
    ext = os.path.splitext(filename)[-1]
    return f'publications/{name_tpl.format(new_name, ext)}'


class PublicationBureau(models.Model):
    slug = AutoSlugField(populate_from='titre', unique_with=['date_creation__year'])
    titre = models.CharField(max_length=125)
    description = models.TextField(blank=True)
    contenu = models.TextField(blank=True)
    commisson = models.CharField(max_length=50, blank=True, default='Bureau Exécutif')
    ordre = models.CharField(max_length=50, blank=True, default='CR 0005/BE/07-2023')
    fichier = models.FileField(upload_to=publication_upload, validators=[validate_extension])
    signature = models.CharField(max_length=100, blank=True, default='')
    auteur_id = models.IntegerField(default=0)
    date_creation = models.DateTimeField(auto_now_add=True)
    date_modification = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ('-date_creation',)

    def __str__(self):
        return self.titre


class Evenement(AbstractModel):
    titre = models.CharField(max_length=255)
    lieu = models.CharField(max_length=255)
    type = models.CharField(max_length=255)
    format = models.CharField(max_length=255)
    debut = models.DateTimeField()
    fin = models.DateTimeField()
    tags = models.ManyToManyField('Tag', related_name='evenements')
    slug = AutoSlugField(populate_from='get_slug')

    def get_slug(self):
        return str(self.titre) + ' - ' + str(self.date_creation.year)

    def __str__(self):
        return str(self.titre) + ' ' + str(self.date_creation.year)

    class Meta:
        ordering = ('debut',)


class Tag(models.Model):
    titre = models.CharField(max_length=255)
    couleur = models.CharField(max_length=10, blank=True, default="#A25463")

    def __str__(self):
        return self.titre


def sent_notification_bureau(sender, instance: PublicationBureau, created, **kwargs):
    # print('signal connected')
    if created:
        lien = FRONT_URL + f"/actualites/bureaux/{instance.slug}"
        try:
            notification_publication_email_task.delay(instance.titre, instance.date_creation, lien, instance.description)
        except Exception as e:
            notification_publication_email(instance.titre, instance.date_creation, lien, instance.description)

        # print(f"Le modèle {instance.titre} a été créé !")


post_save.connect(sent_notification_bureau, sender=PublicationBureau)

# conversion model
model_list = [Article, Temoignage, PublicationBureau, Evenement, Tag]

convert_models_to_js(model_list)
