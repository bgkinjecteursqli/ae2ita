import os

import requests

slug = "binomage-p45-46-2021-2022-yaoussoukro-2023"
url = f"http://127.0.0.1:8000/api/api/albums/{slug}/add_images/"
folder_path = "/home/bgkinjecteursqli/Téléchargements/globe_background_modern_dynamic_bursting_3d_sketch_6841311"
image_path = "chemin/vers/votre/image.jpg"
img_list = []
imgaes = []


def get_images_from_folder(folder_path):
    images = []
    temp_file = []
    for filename in os.listdir(folder_path):
        if filename.endswith('.jpg') or filename.endswith('.png') or filename.endswith('.jpeg') or filename.endswith(
                '.webp'):
            image_path = os.path.join(folder_path, filename)
            images.append(image_path)
            with open(image_path, "rb") as image_file:
                temp_file.append(image_file)
    return images, temp_file


img_data, temp_file = get_images_from_folder(folder_path)
print('images lenght:', len(temp_file))
files = {"images": temp_file }
data = {'album':1}
headers = {}
headers["Content-Type"] = "multipart/form-data"
if __name__ == "__main__":
    response = requests.post(url, data=data, files=files, headers=headers)

    # Vérifier la réponse
    if response.status_code == 200:
        print("Image téléversée avec succès.")
    else:
        print(f"Erreur lors du téléversement de l'image. Code de statut : {response.status_code}")
        print(response.text)
