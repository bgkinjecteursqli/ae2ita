import io, hashlib, os


def generate_hash(filepath):
    sha = hashlib.sha3_256(b"")
    file = io.FileIO(filepath)
    hash_obj = file.readall()
    sha.update(hash_obj)
    return sha.hexdigest()


if __name__ == '__main__':
    old = "65f7f73be1f86e6039665cf6e30111f07b25e905c92ed828393b3a323f46bcb7"
    file = r"/home/bgkinjecteursqli/Bureau/prepa/_MG_0022.jpg"
    sha = generate_hash(file)
    print(sha)
    if sha == old:
        print("is same")
    else:
        print("no same")