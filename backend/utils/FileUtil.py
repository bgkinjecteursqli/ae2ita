import os
from django.core.exceptions import ValidationError


def validate_extension(value):
    valid_extensions = ['.pdf', '.docx', '.doc', '.xlsx', '.xls', '.ptt', '.pptx']
    ext = os.path.splitext(value.name)[-1]
    if ext.lower() not in valid_extensions:
        raise ValidationError(f"L'extension '{ext}' n'est pas autorisée.")


def validate_image_extension(value):
    valid_extensions = ['.jpg', '.png', '.webp', '.jpeg']
    ext = os.path.splitext(value.name)[-1]
    if ext.lower() not in valid_extensions:
        raise ValidationError(f"L'extension '{ext}' n'est pas autorisée.")


def validate_media_extension(value):
    valid_extensions = ['.mp4', '.avi', '.3gp']
    ext = os.path.splitext(value.name)[-1]
    if ext.lower() not in valid_extensions:
        raise ValidationError(f"L'extension '{ext}' n'est pas autorisée.")


def format_duration(duration_in_seconds):
    hours = int(duration_in_seconds // 3600)
    minutes = int((duration_in_seconds % 3600) // 60)
    seconds = int(duration_in_seconds % 60)

    formatted_duration = ""
    if hours > 0:
        formatted_duration += f"{hours} h "
    if minutes > 0:
        formatted_duration += f"{minutes} min "
    if seconds > 0:
        formatted_duration += f"{seconds} sec"

    return formatted_duration

