import re


class StringUtil:
    @staticmethod
    def clean_title(title):
        # Supprimer les caractères spéciaux et les espaces
        cleaned_title = re.sub(r'[^a-zA-Z0-9]', '_', title)

        # Remplacer les espaces par des underscores
        cleaned_title = cleaned_title.replace(' ', '_')

        # Retourner le titre nettoyé
        return cleaned_title

    @staticmethod
    def format_file_size(size):
        """
        Formate la taille d'un fichier en octets, Ko, Mo ou Go.
        """
        if size < 1024:
            return f"{size} octets"
        elif size < 1024 * 1024:
            return f"{round(size / 1024, 2)} Ko"
        elif size < 1024 * 1024 * 1024:
            return f"{round(size / (1024 * 1024), 2)} Mo"
        else:
            return f"{round(size / (1024 * 1024 * 1024), 2)} Go"
