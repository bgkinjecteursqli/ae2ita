def link_parser(link: str) -> str:
    from backend.settings import CDN_LiNK
    if link.startswith('http') or link.startswith('https'):
        return link
    return CDN_LiNK + link
