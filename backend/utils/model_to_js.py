import os

from backend.settings import BASE_DIR, DEBUG


def convert_models_to_js(model_list):
    DEBUG = False
    if DEBUG:
        # print('.......models conversion started......')
        for model in model_list:
            app_name, model_name= str(model._meta).split('.')
            # print("model:", model_name)
            fields = [field.name for field in model._meta.fields]

            js_model = f"export default {{\n"
            for field in fields:
                js_model += f"  {field}: null,\n"
            js_model += "}"
            path = os.path.join(BASE_DIR, 'jsons')
            current_directory = os.path.dirname(os.path.abspath(__file__))
            base_path = '/'.join(current_directory.split('/')[:-2])
            sub_folder_name = "frontend/models"
            front_path = os.path.join(base_path, sub_folder_name)
            # froncleart_path =r'C:\Users\BorisBob\Documents\GitHub\aktaion\ae2ita\frontend\models'
            with open(f"{path}/{model_name}.model.js", "w") as file:
                file.write(js_model)
            with open(f"{front_path}/{model_name}.model.js", "w") as file:
                file.write(js_model)



