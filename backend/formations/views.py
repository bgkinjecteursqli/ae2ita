from datetime import date

from django.shortcuts import get_object_or_404
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, status, generics, mixins
from rest_framework.decorators import action
from rest_framework.response import Response

from backend.serialiers import MultipleSerializerMixin
from members.models import Membre
from .mixins import LienRessourceCreateMixin
from .models import Formation, Formateur, FormationVideo, SupportCours, LienRessource, InscriptionFormation
from .serializers import FormationListSerializer, FormationDetailSerializer, FormateurSerializer, VideoSerializer, \
    SupportCoursSerializer, LienRessourceSerializer, InscriptionFormationSerializer


class FormationViewSet(MultipleSerializerMixin, viewsets.ModelViewSet):
    queryset = Formation.objects.all()
    serializer_class = FormationListSerializer
    detail_serializer_class = FormationDetailSerializer
    lookup_field = 'slug'

    def perform_create(self, serializer):
        return serializer.save()

    def create(self, request, *args, **kwargs):
        formateurs_data = request.data.pop('formateurs', [])
        videos_data = request.data.pop('playlist_videos', [])
        supports_data = request.data.pop('supports_cours', [])
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        instance = self.perform_create(serializer)

        if formateurs_data:
            formateurs_serializer = FormateurSerializer(data=formateurs_data, many=True)
            formateurs_serializer.is_valid(raise_exception=True)
            formateurs_serializer.save(formation=instance)
        if videos_data:
            videos_serializer = VideoSerializer(data=videos_data, many=True)
            videos_serializer.is_valid(raise_exception=True)
            videos_serializer.save(formation=instance)

        if supports_data:
            supports_serializer = SupportCoursSerializer(data=supports_data, many=True)
            supports_serializer.is_valid(raise_exception=True)
            supports_serializer.save(formation=instance)

        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def update(self, request, *args, **kwargs):
        formateurs_data = request.data.pop('formateurs', [])
        videos_data = request.data.pop('playlist_videos', [])
        supports_data = request.data.pop('supports_cours', [])
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        formateurs_serializer = FormateurSerializer(data=formateurs_data, many=True)
        formateurs_serializer.is_valid(raise_exception=True)
        formateurs_serializer.save(formation=self.get_object())

        videos_serializer = VideoSerializer(instance.playlist_videos.all(), data=videos_data, many=True)
        videos_serializer.is_valid(raise_exception=True)
        videos_serializer.save()

        supports_serializer = SupportCoursSerializer(instance.supports_cours.all(), data=supports_data, many=True)
        supports_serializer.is_valid(raise_exception=True)
        supports_serializer.save()

        return Response(serializer.data)

    @swagger_auto_schema(
        method='post',
        operation_summary='Inscription à la formation',
        responses={201: 'Created', 400: 'Bad Request'},
    )
    @action(detail=True, methods=['post'])
    def inscription(self, request, slug=None, *args, **kwargs):
        email = request.data.get('email')
        formation_slug = request.data.get('formation_slug')

        today = date.today()
        try:
            membre = Membre.objects.get(email1__iexact=email)
            formation = get_object_or_404(Formation, slug=slug)

            if formation.date_limit_incription is not None and today > formation.date_limit_incription:
                return Response({'message': 'La date limite d\'inscription est dépassée.'},
                                status=status.HTTP_400_BAD_REQUEST)

            inscrit = InscriptionFormation(
                formation=formation,
                nom=membre.nom,
                prenoms=membre.prenoms,
                email=membre.email1,
                photo=membre.photo.path,
                formation_slug=formation_slug,
                titre=formation.titre
            )

            serializer = InscriptionFormationSerializer(data=inscrit)
            serializer.is_valid(raise_exception=True)
            self.perform_create(serializer)
            headers = self.get_success_headers(serializer.data)
            return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

        except Membre.DoesNotExist:
            # Gérer le cas où le membre n'existe pas
            return Response({'message': 'Le membre n\'existe pas.'}, status=status.HTTP_400_BAD_REQUEST)

        except Formation.DoesNotExist:
            return Response({'message': 'La formation n\'existe pas.'}, status=status.HTTP_400_BAD_REQUEST)

    @swagger_auto_schema(
        operation_description="Liste des personnes inscrites à la formation",
        responses={200: InscriptionFormationSerializer(many=True)}
    )
    @action(detail=True, methods=['get'])
    def inscrits(self, request, slug=None):
        formation = self.get_object()
        inscrits = InscriptionFormation.objects.filter(formation=formation)
        serializer = InscriptionFormationSerializer(inscrits, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        method='get',
        operation_description="Liste des vidéos de la formation",
        responses={200: VideoSerializer}
    )
    @action(detail=True, methods=['get'])
    def videos(self, request, slug=None):
        formation = self.get_object()
        videos = formation.videos.all()
        serializer = VideoSerializer(videos, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        method='get',
        operation_description="Liste des liens  de la formation",
        responses={200: LienRessourceSerializer}
    )
    @action(detail=True, methods=['get'])
    def liens(self, request, pk=None):
        formation = self.get_object()
        liens = formation.liens_ressources.all()
        serializer = LienRessourceSerializer(liens, many=True)
        return Response(serializer.data)

    @swagger_auto_schema(
        method='get',
        operation_description="Liste des supports de cours  de la formation",
        responses={200: LienRessourceSerializer}
    )
    @action(detail=True, methods=['get'])
    def supports(self, request, pk=None):
        formation = self.get_object()
        supports = formation.supports_cours.all()
        serializer = SupportCoursSerializer(supports, many=True)
        return Response(serializer.data)


class FormateurViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.DestroyModelMixin):
    queryset = Formateur.objects.all()
    serializer_class = FormateurSerializer


class VideoViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.DestroyModelMixin):
    queryset = FormationVideo.objects.all()
    serializer_class = VideoSerializer


class SupportCoursViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.DestroyModelMixin):
    queryset = SupportCours.objects.all()
    serializer_class = SupportCoursSerializer


class LienRessourceViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin, mixins.DestroyModelMixin):
    queryset = LienRessource.objects.all()
    serializer_class = LienRessourceSerializer
