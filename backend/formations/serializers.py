from rest_framework import serializers, status

from .models import Formation, Formateur, FormationVideo, SupportCours, LienRessource, InscriptionFormation, \
    FormationImage


class FormateurSerializer(serializers.ModelSerializer):
    class Meta:
        model = Formateur
        fields = '__all__'


class VideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormationVideo
        fields = '__all__'


class ImagesSerializer(serializers.ModelSerializer):
    class Meta:
        model = FormationImage
        fields = '__all__'


class SupportCoursSerializer(serializers.ModelSerializer):
    class Meta:
        model = SupportCours
        fields = '__all__'


class LienRessourceSerializer(serializers.ModelSerializer):
    class Meta:
        model = LienRessource
        fields = '__all__'


class InscriptionFormationSerializer(serializers.ModelSerializer):
    class Meta:
        model = InscriptionFormation
        fields = '__all__'


class FormationListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Formation
        fields = '__all__'


class FormationDetailSerializer(serializers.ModelSerializer):
    formateurs = FormateurSerializer(many=True)
    playlist_videos = VideoSerializer(many=True)
    supports_cours = SupportCoursSerializer(many=True)
    liens_ressources = LienRessourceSerializer(many=True)
    inscriptions = InscriptionFormationSerializer(many=True)
    formation_images = ImagesSerializer(many=True)

    class Meta:
        model = Formation
        fields = '__all__'

