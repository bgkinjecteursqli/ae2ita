from ckeditor.widgets import CKEditorWidget
from django.contrib import admin
from django.db import models

# Register your models here.
from .models import (Formateur, Formation, FormationVideo,
                     InscriptionFormation, LienRessource, SupportCours, FormationImage)


class BaseAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.TextField: {'widget': CKEditorWidget},
    }

admin.site.register(Formateur)
admin.site.register(Formation, BaseAdmin)
admin.site.register(FormationVideo)
admin.site.register(InscriptionFormation)
admin.site.register(LienRessource)
admin.site.register(SupportCours)
admin.site.register(FormationImage)