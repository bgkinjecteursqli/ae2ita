import json
from typing import List, Dict

from rest_framework import mixins, status
from rest_framework.response import Response

from formations.models import Formation, LienRessource
from formations.serializers import LienRessourceSerializer


class LienRessourceCreateMixin(mixins.CreateModelMixin):
    def create(self, request, *args, **kwargs):
        formation_slug = request.data.get('formation_slug', None)
        # Vérifier si le slug de la formation est fourni
        if formation_slug:
            # Récupérer la formation correspondante en utilisant le slug
            try:
                formation = Formation.objects.get(slug=formation_slug)
            except Formation.DoesNotExist:
                return Response({"message": "la formations n'existe pas"}, status=status.HTTP_400_BAD_REQUEST)

            liens = request.data.get('liens', '') #format lien [{'titre','lien']]
            json_lien = json.loads(liens) if liens else []
            liens = [LienRessource(formation=formation, **dict(lien)) for lien in liens]

            serializer = LienRessourceSerializer(data=liens, many=True).is_valid(raise_exception=True)
            # self.perform_create(serializer)
            return serializer

        return super().create(request, *args, **kwargs)
