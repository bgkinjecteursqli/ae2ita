import json
import os
from autoslug import AutoSlugField
from ckeditor.fields import RichTextField
from django.db import models
from django.utils import timezone
from django.utils.text import slugify
from django.utils.crypto import get_random_string

from members.models import Membre
from utils.StringUtil import StringUtil
from utils.model_to_js import convert_models_to_js
from moviepy.video.io.VideoFileClip import VideoFileClip
import moviepy.editor as mp


class Formateur(models.Model):
    # formation = models.ForeignKey(Formation, on_delete=models.CASCADE, related_name='formateurs')
    nom = models.CharField(max_length=100)
    photo = models.ImageField(upload_to='formations/formateurs', blank=True, null=True, default='')
    fonction = models.CharField(max_length=125)
    description = models.TextField(blank=True)

    def __str__(self):
        return self.nom


class Formation(models.Model):
    class Meta:
        ordering = ('-date_creation',)

    titre = models.CharField(max_length=100)
    cover = models.ImageField(upload_to='formations/images/', blank=True, null=True)
    objectif = models.TextField()
    theme = models.CharField(max_length=100)
    description = RichTextField()
    date_creation = models.DateTimeField(auto_now_add=True)
    slug = AutoSlugField(populate_from='get_slug', unique=True)
    texte_cours = models.TextField(blank=True, default=None)
    lieu = models.CharField(max_length=100)
    date_formation = models.DateField()
    date_limit_inscription = models.DateField(blank=True, null=True, default=None)
    duree = models.CharField(max_length=25, blank=True, default="1 jour")
    formateurs = models.ManyToManyField(Formateur, null=True)

    # liste_images = models.ManyToManyField('FormationImage', blank=True, null=True, related_name="images")
    # liste_formateurs = models.ManyToManyField('Formateur', blank=True, null=True, related_name="formateurs")
    # liste_videos = models.ManyToManyField('FormationVideo', blank=True, null=True, related_name="playlist_videos")
    # liste_supports = models.ManyToManyField('SupportCours', blank=True, null=True, related_name="supports_cours")
    # liste_liens = models.ManyToManyField('LienRessource', blank=True, null=True, related_name="liens_ressources")

    def get_slug(self):
        return f'%s-%s' % (self.titre, self.date_creation.year)

    def __str__(self):
        return self.titre


def video_thumbnail(video_path: str, filename: str):
    # Créer un nom pour la miniature
    thumbnail_name = os.path.basename(video_path)
    thumbnail_name = thumbnail_name.replace('.', '_') + '.jpg'

    # Chemin complet pour enregistrer la miniature
    thumbnail_path = os.path.join('formations', 'thumbnails', thumbnail_name)

    # Créer la miniature en utilisant moviepy
    clip = VideoFileClip(video_path)
    thumbnail = clip.get_frame(2)  # Prendre l'image à 2 secondes dans la vidéo
    thumbnail.save_frame(thumbnail_path)
    return thumbnail_path


def get_video_thumbnail(instance, output_path=None):
    output_path = instance.video.path.replace('.', '_') + '.jpg'
    p = os.path.join("formations", 'videos', os.path.basename(instance.video.path)).replace('.', '_') + '.jpg'
    p.replace('\\', "/")
    clip = mp.VideoFileClip(instance.video.path)
    frame = clip.get_frame(2)  # Obtenir la première image de la vidéo
    file_out = output_path
    mp.ImageClip(frame).save_frame(file_out)
    return p.replace('\\', "/")


def video_filename(instance, filename):
    formation_titre = StringUtil.clean_title(instance.formation.titre)

    video_titre = StringUtil.clean_title(instance.titre)

    extension = filename.split('.')[-1]

    nouveau_nom = f"{formation_titre}_{video_titre}_{instance.pk}"
    vn = slugify([nouveau_nom])
    return f"formations/videos/{vn}.{extension}"


def get_video_duration(video_path):
    clip = VideoFileClip(video_path)
    return clip.duration


class FormationVideo(models.Model):
    formation = models.ForeignKey(Formation, on_delete=models.CASCADE, related_name='playlist_videos')
    titre = models.CharField(max_length=100)
    youtube_id = models.CharField(max_length=11, blank=True, default=None)
    video = models.FileField(upload_to=video_filename, blank=True, default=None)
    thumbnail = models.ImageField(upload_to=get_video_thumbnail, blank=True, null=True)
    duration = models.FloatField(blank=True, null=True)

    def __str__(self):
        return self.titre

    def save(self, *args, **kwargs):
        super(FormationVideo, self).save(*args, **kwargs)
        # Enregistrer la miniature
        # self.thumbnail = video_thumbnail(self.video.path)
        video_path = self.video.path
        if not self.thumbnail:
            self.thumbnail = get_video_thumbnail(self)
        if not self.duration:
            self.duration = get_video_duration(video_path)

        super(FormationVideo, self).save(*args, **kwargs)


def image_filename(instance, filename):
    formation_titre = StringUtil.clean_title(instance.formation.titre)

    extension = filename.split('.')[-1]

    nouveau_nom = f"{formation_titre}_image_{instance.pk}"
    nv = slugify([nouveau_nom])
    return f"formations/images/{nv}.{extension}"


class FormationImage(models.Model):
    formation = models.ForeignKey(Formation, on_delete=models.CASCADE, related_name='formation_images')
    image = models.FileField(upload_to=image_filename, blank=True, default=None)

    def __str__(self):
        return str(self.image)


class SupportCours(models.Model):
    formation = models.ForeignKey(Formation, on_delete=models.CASCADE, related_name='supports_cours')
    titre = models.CharField(max_length=100)
    fichier = models.FileField(upload_to='formations/supports/')

    def __str__(self):
        return self.titre


class LienRessource(models.Model):
    formation = models.ForeignKey(Formation, on_delete=models.CASCADE, related_name='liens_ressources')
    titre = models.CharField(max_length=100)
    url = models.URLField(blank=True, null=True)
    lien = models.CharField(max_length=100, blank=True, null=True)

    def __str__(self):
        return self.titre

    def set_liens_ressources(self, value):
        self.liens = json.dumps(value)

    def get_liens_ressources(self):
        return json.loads(self.liens) if self.liens else []


class InscriptionFormation(models.Model):
    formation = models.ForeignKey(Formation, on_delete=models.CASCADE, related_name='inscriptions')
    nom = models.CharField(max_length=25, blank=True)
    prenoms = models.CharField(max_length=125, blank=True)
    photo = models.CharField(max_length=125, blank=True)
    email = models.EmailField()
    code = models.CharField(max_length=10, unique=True, blank=True)
    titre = models.CharField(max_length=100, blank=True, null=True)
    date_inscription = models.DateTimeField(default=timezone.now)
    present = models.BooleanField(default=False)
    formation_slug = models.SlugField(blank=True)

    def save(self, *args, **kwargs):
        if not self.code:
            self.code = get_random_string(6)
        super().save(*args, **kwargs)

    def __str__(self):
        return 'inscrit formation: {} {}'.format(self.membre.nom, self.membre.prenoms)


model_list = [Formation, Formateur, InscriptionFormation, SupportCours, LienRessource, FormationVideo]
convert_models_to_js(model_list)
