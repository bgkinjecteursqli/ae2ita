from django.urls import include, path
from rest_framework.routers import DefaultRouter, SimpleRouter
from .views import FormationViewSet, FormateurViewSet, SupportCoursViewSet, LienRessourceViewSet, VideoViewSet

# Créez un routeur par défaut
router = SimpleRouter()
router2 = SimpleRouter()

# Enregistrez les viewsets avec le routeur
router.register(r'formations', FormationViewSet, basename='formations')
router2.register(r'formateurs', FormateurViewSet, basename='formateurs')
router2.register(r'supports', SupportCoursViewSet, basename='supports')
router2.register(r'liens', LienRessourceViewSet, basename='liens')
router2.register(r'videos', VideoViewSet, basename='videos')


router = [
    # Autres routes de votre application
    path('', include(router.urls)),
    path('formations/', include(router2.urls)),
]
