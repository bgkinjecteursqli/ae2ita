from bs4 import BeautifulSoup

code = """
<div class="_8nE1Y">
  <div role="gridcell" aria-colindex="2" class="y_sn4">
    <div class="_21S-L">
      <div class="Mk0Bp _30scZ">
        <span dir="auto" title="~ Amoin ☺️" aria-label="Peut-être Amoin ☺️" style="min-height: 0px;" class="ggj6brxn gfz4du6o r7fjleex g0rxnol2 lhj4utae le5p0ye3 l7jjieqr _11JPr">~ Amoin <img crossorigin="anonymous" src="data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7" alt="☺️" draggable="false" style="background-position: -60px 0px;" class="b3 emoji wa _11JPr" tabindex="-1"></span>
      </div>
    </div>
    <div class="Dvjym">
      <div class="p357zi0d gndfcl4n icj6mcig om7fnf5u lnjlmjd6 i3it5ig8 mc6o24uu lak21jic gz7w46tb gofg5ll1 p7waza29 oteuebma mzoqfcbu d1poss59 gyj32ejw cmcp1to6 eg0col54 gh2eiktu ed316hig oyhj0yr2 bbryylkj tus15p9f cayimkie fe61fa5g qj4wrk6p cphhpnv8 tfm3omh7">Admin du groupe</div>
    </div>
  </div>
  <div class="_2KKXC">
    <div class="vQ0w7"></div>
    <div role="gridcell" aria-colindex="1" class="Dvjym">
      <span class="_2h0YP"><span dir="auto" aria-label="" style="min-height: 0px;" class="_11JPr">+225 58 45 86 62</span></span><span></span><span></span>
    </div>
  </div>
</div>
"""

soup = BeautifulSoup(code, "html.parser")

nom = soup.find("span", class_="Mk0Bp _30scZ").text.strip()
contact = soup.find("span", class_="p357zi0d").text.strip()

print("Nom:", nom)
print("Contact:", contact)
