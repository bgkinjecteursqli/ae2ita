export default {
  id: null,
  titre: null,
  description: null,
  cover: null,
  slug: null,
  auteur: null,
  type: null,
  tags: null,
  date_creation: null,
}