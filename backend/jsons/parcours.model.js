export default {
  id: null,
  membre: null,
  intitule: null,
  niveau: null,
  specialite: null,
  annee_scolaire: null,
  ecole: null,
  date_creation: null,
  slug: null,
}