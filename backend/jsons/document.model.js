export default {
  id: null,
  dossier: null,
  titre: null,
  fichier: null,
  extension: null,
  taille: null,
  date_creation: null,
  auteur: null,
  slug: null,
}