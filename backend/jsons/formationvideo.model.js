export default {
  id: null,
  formation: null,
  titre: null,
  youtube_id: null,
  video: null,
  thumbnail: null,
  duration: null,
}