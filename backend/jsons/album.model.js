export default {
  id: null,
  titre: null,
  lieu: null,
  date_creation: null,
  auteur: null,
  description: null,
  slug: null,
  cover: null,
  vues: null,
  label: null,
}