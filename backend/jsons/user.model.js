export default {
  id: null,
  password: null,
  last_login: null,
  is_superuser: null,
  username: null,
  first_name: null,
  last_name: null,
  email: null,
  is_staff: null,
  is_active: null,
  date_joined: null,
}