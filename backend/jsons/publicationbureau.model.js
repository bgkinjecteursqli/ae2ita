export default {
  id: null,
  slug: null,
  titre: null,
  description: null,
  contenu: null,
  commisson: null,
  ordre: null,
  fichier: null,
  signature: null,
  auteur_id: null,
  date_creation: null,
  date_modification: null,
}