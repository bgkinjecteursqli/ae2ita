export default {
  id: null,
  album: null,
  titre: null,
  fichier: null,
  taille: null,
  thumbnail: null,
  duration: null,
}