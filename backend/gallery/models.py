import os.path

from autoslug import AutoSlugField
from django.db import models, transaction

from utils.FileUtil import validate_media_extension, validate_image_extension, format_duration
from utils.StringUtil import StringUtil
from utils.hash_utils import generate_hash
from utils.model_to_js import convert_models_to_js
from moviepy.video.io.VideoFileClip import VideoFileClip
import moviepy.editor as mp


class Album(models.Model):
    class Meta:
        db_table = "tb_album"
        ordering = ('-date_creation',)

    titre = models.CharField(max_length=100)
    lieu = models.CharField(max_length=100)
    date_creation = models.DateField()
    auteur = models.CharField(max_length=100, blank=True, default='')
    description = models.TextField(blank=True, default="")
    slug = AutoSlugField(populate_from="get_slug", unique_with=['lieu'])
    cover = models.FileField(upload_to="galerie/thumbnail/", blank=True, null=True,
                             validators=[validate_image_extension])
    vues = models.IntegerField(default=0)
    label = models.CharField(max_length=15, blank=True, default="Akwaba")

    def get_slug(self):
        return f"{self.titre} {self.lieu} {self.date_creation.year}"

    def get_images(self):
        return self.album_images.all()

    def get_videos(self):
        return self.album_videos.all()

    def __str__(self):
        return self.titre

    @transaction.atomic
    def save(self, *args, **kwargs):
        super(Album, self).save(*args, **kwargs)


def video_filename(instance, filename):
    titre = instance.album.titre
    ext = instance.fichier.path.split('.')[-1]
    video_filename = StringUtil.clean_title(f'{titre}_video_{instance.pk}')
    return f"galerie/videos/{video_filename}.{ext}"


def get_video_thumbnail(instance, output_path=None):
    # output_path = instance.fichier.path.replace('.', '_') + '.jpg'
    p = os.path.join("galerie", 'thumbnail', os.path.basename(instance.fichier.path)).replace('.', '_') + '.jpg'
    clip = mp.VideoFileClip(instance.fichier.path)
    frame = clip.get_frame(2)
    file_out = p
    mp.ImageClip(frame).save_frame(os.path.join("media", file_out))
    return p.replace('\\', "/")


def get_video_duration(video_path):
    clip = VideoFileClip(video_path)
    return clip.duration


class AlbumVideo(models.Model):
    class Meta:
        db_table = "tb_album_video"


    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='album_videos')
    titre = models.CharField(blank=True, null=True, max_length=155)
    fichier = models.FileField(upload_to=video_filename, validators=[validate_media_extension])
    taille = models.CharField(blank=True, null=True, max_length=20)
    thumbnail = models.FileField(upload_to=get_video_thumbnail, blank=True, null=True,
                                 validators=[validate_image_extension])
    duration = models.CharField(blank=True, null=True, max_length=25)
    hash = models.CharField(blank=True, null=True, max_length=255, unique=True)

    @transaction.atomic
    def save(self, *args, **kwargs):
        super(AlbumVideo, self).save(*args, **kwargs)
        if not self.titre:
            self.titre = f'{self.album.titre}_video_{self.pk}'
        if not self.taille:
            self.taille = StringUtil.format_file_size(self.fichier.size)
        if not self.duration:
            self.duration = format_duration(get_video_duration(self.fichier.path))
        if not self.thumbnail:
            self.thumbnail = get_video_thumbnail(self)
        if not self.hash:
            self.hash = generate_hash(self.fichier.path)
        super(AlbumVideo, self).save(*args, **kwargs)

    def __str__(self):
        return 'AlbumVideo( ' + str(self.titre) + ')'

    def get_slug(self):
        return f'{self.album.titre} image {self.album.date_creation.year} {self.pk}'


class AlbumImage(models.Model):

    class Meta:
        db_table = "tb_album_image"

    album = models.ForeignKey(Album, on_delete=models.CASCADE, related_name='album_images')
    titre = models.CharField(blank=True, null=True, max_length=155)
    fichier = models.ImageField(upload_to='galerie/images/', validators=[validate_image_extension])
    taille = models.CharField(blank=True, null=True, max_length=10, default="")
    hash = models.CharField(blank=True, null=True, max_length=255, unique=True)


    @transaction.atomic
    def save(self, *args, **kwargs):
        if not self.titre:
            self.titre = self.fichier.name
        if not self.taille:
            self.taille = StringUtil.format_file_size(self.fichier.size)
        super(AlbumImage, self).save(*args, **kwargs)

    def __str__(self):
        return 'AlbumImage( ' + str(self.titre) + ')'

    def get_slug(self):
        return f'{self.album.titre} video {self.album.date_creation.year} {self.pk}'

    def set_hash(self, hash=None):
        base = 'galerie/images/'
        name = self.fichier.path.split("/")[-1]
        if not self.hash:
            if hash:
                self.hash = hash
            else:
                self.hash = generate_hash(os.path.join(base, name))
        self.save()


model_list = [Album, AlbumImage, AlbumVideo]
convert_models_to_js(model_list)
