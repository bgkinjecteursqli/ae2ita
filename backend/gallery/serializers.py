from rest_framework import serializers

from backend.settings import CDN_LiNK

from .models import Album, AlbumImage, AlbumVideo


class AlbumImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlbumImage
        fields = '__all__'

    def to_representation(self, instance: AlbumImage):
        representation = super().to_representation(instance)
        if not 'http' in representation['fichier']:
            representation['fichier'] = CDN_LiNK + representation['fichier']
        return representation


class AlbumVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlbumVideo
        fields = '__all__'

    def to_representation(self, instance: AlbumImage):
        representation = super().to_representation(instance)
        if not 'http' in representation['fichier']:
            representation['fichier'] = CDN_LiNK + representation['fichier']
        if not 'http' in representation['thumbnail']:
            representation['thumbnail'] = CDN_LiNK + representation['thumbnail']
        return representation


class AlbumListSerializer(serializers.ModelSerializer):
    images_count = serializers.SerializerMethodField()
    videos_count = serializers.SerializerMethodField()

    class Meta:
        model = Album
        fields = '__all__'

    def get_images_count(self, instance: Album):
        return instance.get_images().count()

    def get_videos_count(self, instance: Album):
        return instance.get_videos().count()

    def create(self, validated_data):
        album = Album.objects.create(**validated_data)

        images_data = self.context['request'].FILES.getlist('images')
        for image in images_data:
            AlbumImage.objects.create(album=album, fichier=image)

        videos_data = self.context['request'].FILES.getlist('videos')
        for video in videos_data:
            AlbumVideo.objects.create(album=album, fichier=video)

        return album




class AlbumDetailSerializer(serializers.ModelSerializer):
    album_images = AlbumImageSerializer(many=True, read_only=True)
    album_videos = AlbumVideoSerializer(many=True, read_only=True)

    class Meta:
        model = Album
        fields = '__all__'


class GalleryImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlbumImage
        fields = ('album', 'fichier', "titre")


class GalleryVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = AlbumVideo
        fields = ('album', 'fichier', "titre")