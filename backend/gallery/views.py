from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, permissions, status
from rest_framework.decorators import action
from rest_framework.exceptions import ValidationError
from rest_framework.response import Response

from backend.serialiers import MultipleSerializerMixin
from utils.hash_utils import generate_hash
from .models import Album, AlbumImage, AlbumVideo
from .serializers import AlbumListSerializer, AlbumDetailSerializer, AlbumImageSerializer, AlbumVideoSerializer

"""
       Cette fonction effectue une addition.

       Args:
           a (int): Le premier nombre à additionner.
           b (int): Le deuxième nombre à additionner.

       Returns:
           int: La somme des deux nombres.
       """

class AlbumViewSet(MultipleSerializerMixin, viewsets.ModelViewSet):
    # queryset = Album.objects.all()
    queryset = Album.objects.all().prefetch_related('album_images', 'album_videos')
    serializer_class = AlbumListSerializer
    detail_serializer_class = AlbumDetailSerializer
    lookup_field = 'slug'
    permission_classes = [permissions.AllowAny]

    def create(self, request, *args, **kwargs):
        self.permission_classes = self.get_permissions_classes()
        self.check_permissions()
        serializer = AlbumListSerializer(data=request.data, context={'request': request})
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)

    def destroy(self, request, *args, **kwargs):
        self.permission_classes = self.get_permissions_classes()
        self.check_permissions()
        return super(AlbumViewSet, self).destroy(request, *args, **kwargs)

    def retrieve(self, request, *args, **kwargs):
        super(AlbumViewSet, self).retrieve(request, *args, **kwargs)
        obj = self.get_object()
        obj.vues = obj.vues + 1
        obj.save()
        return super(AlbumViewSet, self).retrieve(request, *args, **kwargs)

    def update(self, request, *args, **kwargs):
        self.permission_classes = self.get_permissions_classes()
        self.check_permissions()
        return super(AlbumViewSet, self).update(request, *args, **kwargs)

    @swagger_auto_schema(
        responses={200: AlbumImageSerializer},  # Spécifiez les réponses attendues
        operation_description="liste des images  de l'album spécifié",
    )
    @action(detail=True, methods=['get'])
    def images(self, request, slug=None):
        album = self.get_object()
        images = album.get_images()
        serializer = AlbumImageSerializer(images, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(

        responses={200: AlbumImageSerializer},  # Spécifiez les réponses attendues
        operation_description="liste des vidéos  de l'album spécifié",
    )
    @action(detail=True, methods=['get'])
    def videos(self, request, slug=None):
        album = self.get_object()
        videos = album.get_videos()
        serializer = AlbumVideoSerializer(videos, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)

    @swagger_auto_schema(
        request_body=AlbumImageSerializer,  # Spécifiez le sérialiseur de demande
        responses={200: AlbumImageSerializer},  # Spécifiez les réponses attendues
        operation_description="Ajoutez des images à l'album en spécifiant une liste d'images.",
    )
    @action(detail=True, methods=['post'])
    def add_images(self, request, slug=None):
        album = self.get_object()
        error = []
        images_data = request.FILES.getlist('images')
        for image in images_data:
            data = {"album": album.pk, "fichier": image}
            serializer = AlbumImageSerializer(data=data)
            try:
                serializer.is_valid(raise_exception=True)
                serializer.save()
            except ValidationError as e:
                error.append(serializer.errors)
        if error:
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
        data = AlbumDetailSerializer(album).data
        return Response(data=data, status=status.HTTP_201_CREATED)

    @swagger_auto_schema(
        request_body=AlbumVideoSerializer,
        responses={200: AlbumVideoSerializer},
        operation_description="Ajoutez des vidéos à l'album en spécifiant une liste de vidéos.",
    )
    @action(detail=True, methods=['post'])
    def add_videos(self, request, slug=None):
        album = self.get_object()
        videos_data = request.FILES.getlist('videos')
        for video in videos_data:
            AlbumVideo.objects.create(album=album, fichier=video)
        data = AlbumDetailSerializer(album).data
        return Response(data=data, status=status.HTTP_201_CREATED)

    def get_permissions_classes(self):
        if self.action == 'list' or self.action == 'retrieve':
            # Autoriser l'accès public pour les actions de consultation
            return [permissions.AllowAny()]
        elif self.action in ['create', 'update', 'partial_update']:
            # Autoriser uniquement les utilisateurs authentifiés pour les actions de création et de mise à jour
            return [permissions.IsAuthenticated()]
        elif self.action == 'destroy':
            # Autoriser uniquement les administrateurs pour l'action de suppression
            return [permissions.IsAdminUser()]
        else:
            # Appliquer les permissions par défaut pour les autres actions
            return super().get_permissions()


class ImageViewSet(viewsets.ModelViewSet):
    queryset = AlbumImage.objects.all()
    serializer_class = AlbumImageSerializer


class VideoViewSet(viewsets.ModelViewSet):
    queryset = AlbumVideo.objects.all()
    serializer_class = AlbumVideoSerializer
