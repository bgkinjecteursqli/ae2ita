from django.contrib import admin
from .models import Album, AlbumImage, AlbumVideo

@admin.register(Album)
class AlbumImageAdmin(admin.ModelAdmin):
    display = ('titre', 'taille')

admin.site.register(AlbumImage)
admin.site.register(AlbumVideo)
