from django.urls import include, path
from rest_framework.routers import DefaultRouter

from .views import AlbumViewSet, ImageViewSet, VideoViewSet

router = DefaultRouter()
router.register(r'albums', AlbumViewSet, basename='album')

router = [
    path('', include(router.urls)),
]
