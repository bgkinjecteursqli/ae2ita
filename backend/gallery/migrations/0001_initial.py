# Generated by Django 3.2.20 on 2024-01-06 13:25

import autoslug.fields
from django.db import migrations, models
import django.db.models.deletion
import gallery.models
import utils.FileUtil


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Album',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(max_length=100)),
                ('lieu', models.CharField(max_length=100)),
                ('date_creation', models.DateField()),
                ('auteur', models.CharField(blank=True, default='', max_length=100)),
                ('description', models.TextField(blank=True, default='')),
                ('slug', autoslug.fields.AutoSlugField(editable=False, populate_from='get_slug', unique_with=['lieu'])),
                ('cover', models.FileField(blank=True, null=True, upload_to='galerie/thumbnail/', validators=[utils.FileUtil.validate_image_extension])),
                ('vues', models.IntegerField(default=0)),
                ('label', models.CharField(blank=True, default='Akwaba', max_length=15)),
            ],
            options={
                'db_table': 'tb_album',
                'ordering': ('-date_creation',),
            },
        ),
        migrations.CreateModel(
            name='AlbumVideo',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(blank=True, max_length=155, null=True)),
                ('fichier', models.FileField(upload_to=gallery.models.video_filename, validators=[utils.FileUtil.validate_media_extension])),
                ('taille', models.CharField(blank=True, max_length=20, null=True)),
                ('thumbnail', models.FileField(blank=True, null=True, upload_to=gallery.models.get_video_thumbnail, validators=[utils.FileUtil.validate_image_extension])),
                ('duration', models.CharField(blank=True, max_length=25, null=True)),
                ('hash', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='album_videos', to='gallery.album')),
            ],
            options={
                'db_table': 'tb_album_video',
            },
        ),
        migrations.CreateModel(
            name='AlbumImage',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('titre', models.CharField(blank=True, max_length=155, null=True)),
                ('fichier', models.ImageField(upload_to='galerie/images/', validators=[utils.FileUtil.validate_image_extension])),
                ('taille', models.CharField(blank=True, default='', max_length=10, null=True)),
                ('hash', models.CharField(blank=True, max_length=255, null=True, unique=True)),
                ('album', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='album_images', to='gallery.album')),
            ],
            options={
                'db_table': 'tb_album_image',
            },
        ),
    ]
