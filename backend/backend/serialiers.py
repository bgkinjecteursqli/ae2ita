from auths import serializers, models


class MultipleSerializerMixin:
    detail_serializer_class = None

    # pagination_class = StandardResultsSetPagination

    def get_serializer_class(self):
        if self.action == "retrieve" and self.detail_serializer_class is not None:
            return self.detail_serializer_class
        return super().get_serializer_class()

class LinkSerialierMixi:
    def to_representation(self, instance):
        data = super().to_representation(instance)
        avatar = data.get("avatar" or None)

        return data
