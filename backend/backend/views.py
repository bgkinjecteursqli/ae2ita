from django.middleware.csrf import get_token
from django.http import JsonResponse


def get_csrf_token(request):
    # Récupérer le CSRF Token pour la requête donnée
    csrf_token = get_token(request)
    # Renvoyer le CSRF Token sous forme de réponse JSON
    return JsonResponse({'csrfToken': csrf_token})
