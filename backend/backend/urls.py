from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path, include, re_path
from rest_framework.authentication import SessionAuthentication
from rest_framework_simplejwt.authentication import JWTAuthentication

from backend.views import get_csrf_token
from members.urls import router as membre_router
from blogs.urls import router as blog_router
from formations.urls import router as formation_router
from jobs.urls import router as job_router
from auths.urls import urlpatterns
from gallery.urls import router as album_router
from docs.urls import router as doc_router

from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions

schema_view = get_schema_view(
    openapi.Info(
        title="AE2ITA API",
        default_version='v1',
        description="AE2ITA server API !",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(name="Bgk Injector SqLi", email="borisbob91@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=[permissions.AllowAny],
    authentication_classes=[SessionAuthentication, JWTAuthentication]
)

base_url = 'api/'


def index(request):
    from django.shortcuts import redirect, render

    return redirect('schema-swagger-ui', permanent=False)


urlpatterns = [
                  path('', index, name='index'),
                  path('admin/', admin.site.urls),
                  path(base_url, include(membre_router)),
                  path(base_url, include(blog_router)),
                  path(base_url, include(formation_router)),
                  path(base_url, include(job_router)),
                  path(base_url, include(album_router)),
                  path(base_url, include(doc_router.urls)),
                  path(base_url, include(urlpatterns)),
                  path(f'{base_url}csrf-token/', get_csrf_token, name='get-csrf-token'),
                  re_path(r'^docs/swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0),
                          name='schema-json'),
                  re_path(r'^docs/swagger/$', schema_view.with_ui('swagger', cache_timeout=0),
                          name='schema-swagger-ui'),
                  re_path(r'^docs/redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

admin.site.site_header = "AE2ITA Admin"
admin.site.site_title = "AE2ITA Admin Portal"
admin.site.index_title = "Welcome to AE2ITA  Portal"
