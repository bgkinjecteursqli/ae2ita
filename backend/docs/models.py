from autoslug import AutoSlugField
from django.db import models
import os
from django.core.exceptions import ValidationError
from django.utils import timezone

from members.models import Membre
from utils.StringUtil import StringUtil
from utils.model_to_js import convert_models_to_js


def validate_extension(value):
    valid_extensions = ['.pdf', '.docx', '.doc', '.xlsx', '.xls', '.ppt', '.pptx']
    ext = os.path.splitext(value.name)[-1]  # Récupérer l'extension du fichier
    if ext.lower() not in valid_extensions:
        raise ValidationError(f"L'extension '{ext}' n'est pas autorisée.")


def rename_file(instance, filename):
    base_name, extension = os.path.splitext(filename)
    nom_base = StringUtil.clean_title(filename)
    nouveau_nom = f"{nom_base}{extension}"
    return os.path.join('documents/', nouveau_nom)


dossiers = [
    ('ita_1', "ITA 1"),
    ('ita_2', "ITA 2"),
    ('ita_3', "ITA 3"),
    ('bureau', "BUREAU"),
    ('autres', "AUTRES"),
]


class Document(models.Model):
    class Meta:
        ordering = ('-date_creation',)
    dossier = models.CharField(max_length=15, choices=dossiers, default='ita_1')
    titre = models.CharField(max_length=100, blank=True, null=True)
    fichier = models.FileField(upload_to=rename_file, validators=[validate_extension])
    extension = models.CharField(max_length=10, blank=True)
    taille = models.CharField(max_length=125, null=True, blank=True)
    date_creation = models.DateTimeField(default=timezone.now)
    auteur = models.ForeignKey(Membre, on_delete=models.CASCADE)
    slug = AutoSlugField(populate_from='titre', unique_with=['date_creation'])

    def __str__(self):
        return self.titre

    def get_formatted_size(self):
        return StringUtil.format_file_size(self.fichier.size)

    def get_file_ext(self):
        return os.path.splitext(self.fichier.name)[-1].lower()

    def save(self, *args, **kwargs):
        if not self.extension:
            self.extension = self.get_file_ext()
        if not self.taille:
            self.taille = self.get_formatted_size()
        if not self.titre:
            self.titre = self.fichier.name
        super().save(*args, **kwargs)

# conversion model
model_list = [Document]

convert_models_to_js(model_list)
