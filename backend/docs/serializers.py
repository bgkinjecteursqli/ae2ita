from rest_framework import serializers
from .models import Document


class DocumentSerializer(serializers.ModelSerializer):
    auteur = serializers.CharField(source='auteur.nom', read_only=True)
    lien = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Document
        fields = '__all__'
        filter = []

    def get_lien(self, instance:Document):
        return instance.fichier.url


