from rest_framework import viewsets, status, permissions
from rest_framework.decorators import action
from rest_framework.parsers import MultiPartParser, FormParser
from rest_framework.response import Response
from django.shortcuts import get_object_or_404

from members.models import Membre
from .serializers import  DocumentSerializer
from .models import  Document


class DocumentViewSet(viewsets.ModelViewSet):
    queryset = Document.objects.all()
    serializer_class = DocumentSerializer
    # parser_classes = (MultiPartParser, FormParser)
    permission_classes = [permissions.AllowAny]
    lookup_field = 'slug'
    filterset_fields = ['dossier']

    def create(self, request, *args, **kwargs):

        user = self.request.user
        try:
            membre = Membre.objects.get(user=user)
        except Membre.DoesNotExist:
            return Response({'detail': 'vuillez vous connecter pour ajouter un dossier'})

        serializer = self.get_serializer(data=request.data)
        serializer = serializer(auteur=membre)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
