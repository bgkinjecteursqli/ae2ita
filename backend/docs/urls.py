from django.urls import path, include
from rest_framework.routers import DefaultRouter, SimpleRouter

from docs.views import  DocumentViewSet

router = SimpleRouter()
router.register(r'documents', DocumentViewSet, basename='documents')

