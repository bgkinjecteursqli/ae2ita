const fs = require('fs');
const axios = require('axios');

const slug = "binomage-p45-46-2021-2022-yaoussoukro-2023";
const url = `https://api.ae2ita.com/api/albums/${slug}/add_images/`;
const folderPath = "/home/bgkinjecteursqli/Bureau/prepa";
const albumId = 1;

function getImagesFromFolder(folderPath) {
  const images = [];
  const tempFile = [];

  fs.readdirSync(folderPath).forEach(filename => {
    if (filename.endsWith('.jpg') || filename.endsWith('.png') || filename.endsWith('.jpeg') || filename.endsWith('.webp')) {
      const imagePath = `${folderPath}/${filename}`;
      images.push(imagePath);
      const imageFile = fs.readFileSync(imagePath);
      tempFile.push({
        filename,
        content: imageFile,
      });
    }
  });

  return { images, tempFile };
}

const { images, tempFile } = getImagesFromFolder(folderPath);
console.log('Images length:', tempFile.length);

const formData = new FormData();
tempFile.forEach(file => {
  formData.append('images', file.content, { filename: file.filename });
});

formData.append('album', albumId);

axios.post(url, formData, {
  headers: {

  },
})
  .then(response => {
    console.log('Image uploaded successfully.');
  })
  .catch(error => {
    console.error(`Error uploading image. Status code: ${error.response.status}`);
    console.error(error.response.data);
  });
