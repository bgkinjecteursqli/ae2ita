const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');


require('dotenv').config();

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, '../frontend/static/')))
app.use('/media/', express.static(path.join(__dirname, '../backend/media/')))

app.use('/', indexRouter);
// app.listen(process.env.PORT || 3000, () => {})

module.exports = app;
