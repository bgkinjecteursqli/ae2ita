const fs = require('fs');
const axios = require('axios');
const FormData = require('form-data');

const slug = "binomage-p45-46-2021-2022-yaoussoukro-2023";
const url = `https://api.ae2ita.com/api/albums/${slug}/add_images/`;
const folderPath = "/home/bgkinjecteursqli/Bureau/attente";
const albumId = 1;

function getImagesFromFolder(folderPath) {
  const images = [];
  const formData = new FormData();

  fs.readdirSync(folderPath).forEach(filename => {
    if (filename.endsWith('.jpg') || filename.endsWith('.png') || filename.endsWith('.jpeg') || filename.endsWith('.webp')) {
      const imagePath = `${folderPath}/${filename}`;
      images.push(imagePath);

      // Utilisation de form-data pour simuler FormData côté serveur
      formData.append('images', fs.createReadStream(imagePath), { filename });
    }
  });

  formData.append('album', albumId);

  return { images, formData };
}

const { images, formData } = getImagesFromFolder(folderPath);
console.log('Images length:', images.length);

axios.post(url, formData, {
  headers: {

  },
})
  .then(response => {
    console.log('Image uploaded successfully.');
  })
  .catch(error => {
    console.error(`Error uploading image. Status code: ${error.response.status}`);
    console.error(error.response.data);
  });
